<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\CvStaffController;
use App\Http\Controllers\Staff_CategoryController; 
use App\Http\Controllers\ClientController;
use App\Http\Controllers\PartnerController;  
use App\Http\Controllers\FileController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', [HomeController::class, 'index'])->name('home');

Route::group(['middleware' => ['auth']], function() {
    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
    Route::resource('products', ProductController::class);

    Route::get('/changePassword',[UserController::class, 'changePasswordView'])->name('changePassword');
    
    Route::post('changePasswordUpdate',[UserController::class, 'changePasswordUpdate'])->name('changePasswordUpdate');

    Route::any('projectList', [ProjectController::class, 'index'])->name('project');

    Route::get('project/create',[ProjectController::class, 'create'])->name('addProject');

    Route::post('insert',[ProjectController::class, 'insert'])->name('insertProjectinfo');

    Route::get('project/show/{id}',[ProjectController::class, 'show'])->name('showProject');

    Route::get('/project/{project}/edit', [ProjectController::class, 'edit'])->name('editProject');

    Route::post('/project/{project}/update', [ProjectController::class, 'update'])->name('updateProject');

    Route::get('/project/{project}/delete', [ProjectController::class, 'delete'])->name('deleteProject');

    Route::get('/project/{project}/assignStaff/edit', [ProjectController::class, 'editStaff'])->name('editProjectStaff');

    Route::get('/project/{project}/projectFile/show', [ProjectController::class, 'projectFile'])->name('projectFile');

    Route::get('project/{projectId}/file/{fileId}', [ProjectController::class, 'ProjectFileDelete'])->name('ProjectFileDelete');


    Route::any('cvList', [CvStaffController::class, 'index'])->name('staffcv');

    Route::get('staffCV/create', [CvStaffController::class, 'create'])->name('addStaffcv'); 

    Route::get('/staffCV/{staffCV}/edit', [CvStaffController::class, 'edit'])->name('editStaffcv');

    Route::post('/staffCV/{staffCV}/update', [CvStaffController::class, 'update'])->name('updateStaffcv');

    Route::get('/staffCV/{staffCV}/delete', [CvStaffController::class, 'destroy'])->name('deleteStaffcv');

    Route::post('staffCV/insert', [CvStaffController::class, 'store'])->name('storeStaffcv');

    Route::get('staffCV/show/{id}',[CvStaffController::class, 'show'])->name('showStaffcv');

    Route::get('/staffCV/{staffCV}/staffCvFile/show', [CvStaffController::class, 'staffCvFile'])->name('staffCvFile');

    Route::any('CV/category', [Staff_CategoryController::class, 'index'])->name('category');

    Route::post('CV/category/insert', [Staff_CategoryController::class, 'store'])->name('storeCategory');

    Route::post('CV/category/{category}/update', [Staff_CategoryController::class, 'update'])->name('updateStaffcvCategory');

    Route::get('CV/category/{category}/edit', [Staff_CategoryController::class, 'edit'])->name('editStaffcvCategory');

    Route::get('CV/category/{category}/delete', [Staff_CategoryController::class, 'destroy'])->name('deleteStaffcvCategory');

        Route::get('cvStaff/{cvStaffId}/file/{fileId}', [CvStaffController::class, 'cvStaffFileDelete'])->name('cvStaffFileDelete');

    Route::any('clientList', [ClientController::class, 'index'])->name('client');

    Route::get('client/create', [ClientController::class, 'create'])->name('addClient');

    Route::get('/client/{client}/delete', [ClientController::class, 'destroy'])->name('deleteClient');

    Route::get('/client/{client}/edit', [ClientController::class, 'edit'])->name('editClient');

    Route::post('/client/insert',[ClientController::class, 'store'])->name('storeClient');

    Route::post('/client/{client}/update', [ClientController::class, 'update'])->name('updateClient');

    Route::get('/client/show/{id}',[ClientController::class, 'show'])->name('showClient');



    Route::any('partnerList', [PartnerController::class, 'index'])->name('partner');

    Route::get('partner/create', [PartnerController::class, 'create'])->name('addPartner');

    Route::get('/partner/{partner}/delete', [PartnerController::class, 'destroy'])->name('deletePartner');

    Route::get('/partner/{partner}/edit', [PartnerController::class, 'edit'])->name('editPartner');

    Route::post('/partner/insert',[PartnerController::class, 'store'])->name('storePartner');

    Route::post('/partner/{partner}/update', [PartnerController::class, 'update'])->name('updatePartner');

    Route::get('/partner/show/{id}',[PartnerController::class, 'show'])->name('showPartner');

    Route::get('partner/{partnerId}/file/{fileId}', [PartnerController::class, 'PartnerFileDelete'])->name('PartnerFileDelete');



    Route::get('/file', [FileController::class, 'index'])->name('viewfile');

    Route::get('/file/upload', [FileController::class, 'create'])->name('createfile');

    Route::post('/file/upload', [FileController::class, 'store'])->name('uploadfile');

    Route::get('/file/show/{id}',[FileController::class, 'show'])->name('showFile');

    Route::get('/file/{file}/edit', [FileController::class, 'edit'])->name('editFile');

    Route::post('/file/{file}/edit', [FileController::class, 'update'])->name('updateFile');

    Route::get('/file/{file}/delete', [FileController::class, 'destroy'])->name('deleteFile');

    Route::get('file/{multifileId}/file/{fileId}', [FileController::class, 'notificationFileDelete'])->name('notificationFileDelete');
  
});

