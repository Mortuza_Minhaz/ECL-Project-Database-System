@extends('layouts.app')

@section('title', 'CV List')

@section('js')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {

            var reader = new FileReader();

            reader.onload = function(e) {
                $('.image-upload-wrap').hide();

                $('.file-upload-image').attr('src', e.target.result);
                $('.file-upload-content').show();

                $('.image-title').html(input.files[0].name);
            };

            reader.readAsDataURL(input.files[0]);

        } else {
            removeUpload();
        }
    }

    function removeUpload() {
        $('.file-upload-input').replaceWith($('.file-upload-input').clone());
        $('.file-upload-content').hide();
        $('.image-upload-wrap').show();
    }
    $('.image-upload-wrap').bind('dragover', function() {
        $('.image-upload-wrap').addClass('image-dropping');
    });
    $('.image-upload-wrap').bind('dragleave', function() {
        $('.image-upload-wrap').removeClass('image-dropping');
    });

    $("#datepicker").datepicker({

    format: "yyyy",

    viewMode: "years",

    minViewMode: "years"

});
</script>


@endsection


@section('css')

<style>
    hr {
        border-top: 1px dashed #8e44ad;

    }

    #ssd {
        resize: none;
    }

    .file-upload {
        background-color: #ffffff;
        width: 600px;
        margin-left: 24%;
        padding: 20px;

    }

    .file-upload-btn {
        width: 100%;
        margin: 0;
        color: #fff;
        background: #1FB264;
        border: none;
        padding: 10px;
        border-radius: 4px;
        border-bottom: 4px solid #15824B;
        transition: all .2s ease;
        outline: none;
        text-transform: uppercase;
        font-weight: 700;
    }

    .file-upload-btn:hover {
        background: #1AA059;
        color: #ffffff;
        transition: all .2s ease;
        cursor: pointer;
    }

    .file-upload-btn:active {
        border: 0;
        transition: all .2s ease;
    }

    .file-upload-content {
        display: none;
        text-align: center;
    }

    .file-upload-input {
        position: absolute;
        margin: 0;
        padding: 0;
        width: 100%;
        height: 100%;
        outline: none;
        opacity: 0;
        cursor: pointer;
    }

    .image-upload-wrap {
        margin-top: 20px;
        border: 4px dashed #1FB264;
        position: relative;
    }

    .image-dropping,
    .image-upload-wrap:hover {
        background-color: #1FB264;
        border: 4px dashed #ffffff;
    }

    .image-title-wrap {
        padding: 0 15px 15px 15px;
        color: #222;
    }

    .drag-text {
        text-align: center;
    }

    .drag-text h3 {
        font-weight: 100;
        text-transform: uppercase;
        color: #15824B;
        padding: 60px 0;
    }

    .file-upload-image {
        max-height: 200px;
        max-width: 200px;
        margin: auto;
        padding: 20px;
    }

    .remove-image {
        width: 200px;
        margin: 0;
        color: #fff;
        background: #cd4535;
        border: none;
        padding: 10px;
        border-radius: 4px;
        border-bottom: 4px solid #b02818;
        transition: all .2s ease;
        outline: none;
        text-transform: uppercase;
        font-weight: 700;
    }

    .remove-image:hover {
        background: #c13b2a;
        color: #ffffff;
        transition: all .2s ease;
        cursor: pointer;
    }

    .remove-image:active {
        border: 0;
        transition: all .2s ease;
    }



    [data-role="dynamic-fields"]>.form-inline+.form-inline {
        margin-top: 0.5em;
    }

    [data-role="dynamic-fields"]>.form-inline [data-role="add"] {
        display: none;
    }

    [data-role="dynamic-fields"]>.form-inline:last-child [data-role="add"] {
        display: inline-block;
    }

    [data-role="dynamic-fields"]>.form-inline:last-child [data-role="remove"] {
        display: none;
    }
</style>

@endsection


@section('content')

<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('pages.include.beginPageHeader')


    <!-- END PAGE HEADER-->

    <div class="row">

        <div class="col-md-12">
            <div class="tabbable-line boxless tabbable-reversed">

                <div class="tab-content">
                    <div class="tab-pane active" id="tab_0">

                        {{-- <div class="portlet box blue-hoki">--}}
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-gift"></i>CV Form
                                </div>

                            </div>
                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->

                                <form action="{{route('updateStaffcv',$cvData->id)}}" method="post" class="form-horizontal" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-body">

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Category Name</label>
                                            <div class="col-md-6">
                                                <select id="single" name="category_id" class="form-control select2">
                                                    <option>Select Category</option>

                                                    @foreach($staff_categories as $item)

                                                    @php
                                                    if ($cvData->category_id == $item->id)
                                                    {
                                                    $selected = 'selected';
                                                    }
                                                    else
                                                    {
                                                    $selected = '';
                                                    }
                                                    echo '<option value="' . $item->id . '"' . $selected . '>' . ' ' .
                                                        $item->category_name . '</option>';

                                                    @endphp

                                                    @endforeach


                                                </select>
                                                {!! $errors->first('brand_id', '<small class="text-danger">:message</small>') !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Name of the Consultant</label>
                                            <div class="col-md-6">
                                                <input name="consultant_name" value="{{$cvData->consultant_name}}" type="text" class="form-control" placeholder="Enter Consultant Name">


                                                {!! $errors->first('product_name', '<small class="text-danger">:message</small>') !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">RFP Identification No.</label>
                                            <div class="col-md-6">

                                                <textarea class="form-control" name="rfp_no" id="ssd" rows="3" placeholder="Enter RFP Identification No">{{$cvData->rfp_no}}</textarea>

                                                {!! $errors->first('product_name', '<small class="text-danger">:message</small>') !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Name of the Client</label>
                                            <div class="col-md-6">

                                                <textarea class="form-control" name="client_name" id="ssd" rows="3" placeholder="Enter Name of the Client">{{$cvData->client_name}}</textarea>

                                                {!! $errors->first('product_name', '<small class="text-danger">:message</small>') !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Proposed Position for this
                                                project</label>
                                            <div class="col-md-6">
                                                <input name="position" value="{{$cvData->position}}" type="text" class="form-control" placeholder="Enter Position Held">


                                                {!! $errors->first('product_price', '<small class="text-danger">:message</small>') !!}
                                            </div>
                                        </div>

                                        <hr>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Name of Staff/Professional</label>
                                            <div class="col-md-6">
                                                <input name="staff_name" value="{{$cvData->staff_name}}" type="text" class="form-control" placeholder="Enter Name of Staff/Professional">


                                                {!! $errors->first('product_name', '<small class="text-danger">:message</small>') !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Date of Birth</label>
                                            <div class="col-md-6">
                                                <input type="text" class="date-picker form-control" id="" name="birth_date" value="{{$cvData->birth_date}}" value="<?php echo date('Y-m-d'); ?>" data-date-format="yyyy-mm-dd" placeholder="Birth Date: yyyy-mm-dd">


                                                {!! $errors->first('start_date', '<small class="text-danger">:message</small>') !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Nationality</label>
                                            <div class="col-md-6">
                                                <input name="nationality" value="{{$cvData->nationality}}" type="text" class="form-control" placeholder="Enter Nationality by Birth">




                                                {!! $errors->first('product_name', '<small class="text-danger">:message</small>') !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Membership of Professional
                                                Societies</label>
                                            <div class="col-md-6">

                                                <textarea class="form-control" name="membership" id="ssd" rows="2" placeholder="Enter Membership">{{$cvData->membership}}</textarea>

                                                {!! $errors->first('product_name', '<small class="text-danger">:message</small>') !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Education Qualification</label>

                                            <div class="col-md-6">
                                                <textarea name="qualification" id="summary-ckeditor" rows="3">{{$cvData->qualification}}</textarea>


                                                {!! $errors->first('product_name', '<small class="text-danger">:message</small>') !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                Year of Experience</label>
                                            <div class="col-md-6">
                                                <input type="text" class="date-picker form-control" id="" name="experience" value="{{$cvData->experience}}" value="<?php echo date('Y-m-d'); ?>" data-date-format="yyyy-mm-dd" placeholder="Enter Year of Experience">


                                                {!! $errors->first('start_date', '<small class="text-danger">:message</small>') !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Other Training</label>
                                            <div class="col-md-6">

                                                <textarea class="form-control" name="training" id="ssd" rows="3" placeholder="Other Training.....">{{$cvData->training}}</textarea>

                                                {!! $errors->first('product_name', '<small class="text-danger">:message</small>') !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label" style="padding-top: 30px;">Language
                                                and Degree of Proficiency</label>
                                            <div class="col-md-6">
                                                <div class="row">

                                                    <?php
                                                    $cvDataBng = json_decode($cvData->bengali, true);
                                                    ?>

                                                    <div class="col-md-4">
                                                        <div class="form-group form-md-line-input has-success">
                                                            <div class="input-icon">
                                                                <input type="text" name="bengali[speaking]" class="form-control" placeholder="Speaking" value="{{$cvDataBng['speaking']}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group form-md-line-input has-warning">
                                                            <div class="input-group">
                                                                <input type="text" name="bengali[reading]" class="form-control" placeholder="Reading" value="{{$cvDataBng['reading']}}">


                                                                <span class="input-group-addon">

                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group form-md-line-input has-error">
                                                            <div class="input-group">
                                                                <div class="input-group-control">
                                                                    <input type="text" name="bengali[writing]" class="form-control" placeholder="Writing" value="{{$cvDataBng['writing']}}">

                                                                </div>
                                                                <span class="input-group-btn btn-right">
                                                                    <button href="#" class="btn green-haze " aria-expanded="false">Bengali

                                                                    </button>

                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <?php
                                                    $cvDataEng = json_decode($cvData->english, true);
                                                    ?>
                                                    <div class="col-md-4">
                                                        <div class="form-group form-md-line-input has-success">
                                                            <div class="input-icon">
                                                                <input type="text" name="english[speaking]" class="form-control" placeholder="Speaking" value="{{$cvDataEng['speaking']}}">


                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group form-md-line-input has-warning">
                                                            <div class="input-group">
                                                                <input type="text" name="english[reading]" class="form-control" placeholder="Reading" value="{{$cvDataEng['reading']}}">


                                                                <span class="input-group-addon">

                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group form-md-line-input has-error">
                                                            <div class="input-group">
                                                                <div class="input-group-control">
                                                                    <input type="text" name="english[writing]" class="form-control" placeholder="Writing" value="{{$cvDataEng['writing']}}">

                                                                </div>
                                                                <span class="input-group-btn btn-right">
                                                                    <button class="btn green-haze " aria-expanded="false">English
                                                                    </button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Countries of Work Experience</label>
                                            <div class="col-md-6">
                                                <input name="country_w_experience" value="{{$cvData->country_w_experience}}" type="text" class="form-control" placeholder="Enter Countries of Working Experience">


                                                {!! $errors->first('product_name', '<small class="text-danger">:message</small>') !!}
                                            </div>
                                        </div>

                                        <hr>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Employer-1</label>
                                            <div class="col-md-6">
                                                <input name="employer" value="" type="text" class="form-control" placeholder="">


                                                {!! $errors->first('product_name', '<small class="text-danger">:message</small>') !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Duration</label>
                                            <div class="col-md-6">
                                                <input name="duration" value="" type="text" class="form-control" placeholder="Enter Duration">


                                                {!! $errors->first('product_name', '<small class="text-danger">:message</small>') !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Position</label>
                                            <div class="col-md-6">
                                                <input name="employment_position" value="" type="text" class="form-control" placeholder="Enter Position">


                                                {!! $errors->first('product_name', '<small class="text-danger">:message</small>') !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Duties</label>
                                            <div class="col-md-6">

                                                <textarea class="form-control" name="duties" id="ssd" rows="3" placeholder="Enter Duties....."></textarea>

                                                {!! $errors->first('product_name', '<small class="text-danger">:message</small>') !!}
                                            </div>
                                        </div>

                                        <div class="file-upload">
                                            <div class="image-upload-wrap ">
                                                <input name="cv_file" class="file-upload-input" type='file' onchange="readURL(this);" accept="image/*" />
                                                <div class="drag-text">
                                                    <h3>Drag and drop a file or select to add File</h3>
                                                </div>
                                            </div>
                                            <div class="file-upload-content">
                                                <img class="file-upload-image" src="#" alt="" />
                                                <div class="image-title-wrap">
                                                    <button type="button" onclick="removeUpload()" class="remove-image">Remove <span class="image-title">Uploaded
                                                            Image</span></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-actions top">
                                        <div class="row">
                                            <div class="col-md-offset-4 col-md-8">
                                                <button type="submit" class="btn green">Submit</button>

                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <!-- END FORM-->
                            </div>
                        </div>

                        @php
                        $extention_name = pathinfo($cvData->cv_file, PATHINFO_EXTENSION)
                        @endphp

                        @if($extention_name == 'png' || $extention_name == 'jpg' || $extention_name == 'jpeg')
                        <div>
                            <center>
                                <div class="form-group">
                                    <label class=""></label>
                                    <img src="{{ '/'.$cvData->cv_file}}" height="250px" width="300px">
                                </div>
                            </center>
                        </div>
                        @elseif($extention_name == 'docx' || $extention_name == 'doc' || $extention_name == 'pdf')
                        <div>
                            <center>
                                <div class="form-group">
                                    <label class=""></label>

                                    <iframe src="{{'/'.$cvData->cv_file}}" frameborder="0" style="width:100%;min-height:640px;"></iframe>

                                </div>
                            </center>
                        </div>
                        @else

                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>


@endsection