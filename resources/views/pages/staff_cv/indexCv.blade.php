@extends('layouts.app')

@section('title', 'Staff CV List')

@section('js')

<script>
    $(document).ready(function() {
        $('#example').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'print',
                'pdf',
                'excel'
            ]
        });
    });
</script>

@endsection




@section('css')

<style>
    .portlet.box .dataTables_wrapper .dt-buttons {
        margin-top: 0px;
        margin-bottom: 20px;
    }

    .dataTables_wrapper .dt-buttons {
        float: left;
    }

    div.dataTables_wrapper div.dataTables_paginate {
        /* margin: 0; */
        white-space: nowrap;
        /* text-align: right; */
        float: right !important;
    }

    .input-group-sm>.input-group-btn>select.btn,
    .input-group-sm>select.form-control,
    .input-group-sm>select.input-group-addon,
    select.input-sm {
        height: 31px;
        line-height: 30px;
    }
</style>

@endsection



@section('content')

<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('pages.include.beginPageHeader')


    <!-- END PAGE HEADER-->

    <div class="row">

        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title" style="min-height:21px">
                    <div class="caption" style="font-size: 14px;padding:1px 0 1px;">
                        List of CV
                    </div>
                </div>
                <div class="portlet-body">

                    <div class="row">
                        <div class="col-md-12">

                            <form class="form-horizontal" method="post" id="report_filter" action="{{route('staffcv')}}">

                                @csrf
                                <div class="col-sm-12">
                                    <div style="background-color: grey!important;">

                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Category</label>
                                                <div class="col-md-8">
                                                    <select class="form-control select2" name="category_id" onchange="document.getElementById('report_filter').submit();">
                                                        <option value="">Select Category</option>

                                                        @foreach($staff_categories as $item)

                                                        <option value="{{$item->id}}">{{$item->category_name}}</option>

                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.page-content -->
        </div>

        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">

                    <div class="dt-buttons" style="margin-top: 5px;">
                        @can('cv-create')
                        <a style="color: black; border: none; background-color: #ecf0f1;" class="dt-button buttons-print btn default" tabindex="0" aria-controls="sample_2" href="{{route('addStaffcv')}}"><span> <i class="fa fa-plus"></i>&nbsp; Add CV</span>

                        </a>
                        @endcan

                        @can('create-category')
                        <a style="color: black; border: none; background-color: #ecf0f1;" class="dt-button buttons-print btn default" tabindex="0" aria-controls="sample_2" href="{{route('category')}}"><span> <i class="fa fa-plus"></i>&nbsp; Add Category</span>
                        </a>
                        @endcan

                        @can('cv-list')
                        <a style="color: black; border: none; background-color: #ecf0f1;" class="dt-button buttons-print btn default" tabindex="0" aria-controls="sample_2" href="{{route('staffcv')}}"><span> <i class="fa fa-list"></i>&nbsp; List All</span>

                        </a>
                        @endcan
                    </div>

                </div>

                <div class="portlet-body">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="example">
                        <thead>
                            <tr>
                                <th> Sl No.</th>
                                <th> Name</th>
                                <th> Education Qualification</th>
                                <th> Position Held</th>
                                <th> Year of Experience</th>
                                <th> Category</th>
                                <th> Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($cvList as $item)

                            <tr>
                                <td style="width: 2%;">{{$loop->iteration}}</td>
                                <td>{{$item->staff_name}}</td>

                                <td style="width: 20%;">{!! Str::limit(strip_tags($item->qualification), 500, ' .....') !!}</td>
                                <td>{{$item->position}}</td>
                                <td style="width: 15%;">{{ Carbon\Carbon::parse($item->experience.'-0-0')->diffInYears(now(), false)}} years
                                </td>

                                <td>{{$item->category_name}}
                                </td>

                                 <td>
                                <div class="btn-group">
                                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-left" role="menu">
                                        <li>@can('cv-report')
                                            <a style="border: none;" href="{{route('showStaffcv',$item->id)}}">
                                                <i style="" class="fa fa-eye"></i> Details </a>@endcan
                                        </li>
                                        <li>
                                            @can('cv-edit')
                                            <a style="border: none;" href="{{route('editStaffcv',$item->id)}}"><i class="fa fa-edit"></i> Edit</a>@endcan
                                        </li>
                                        <li>
                                            @can('cv-delete')
                                            <a href="{{route('deleteStaffcv',$item->id)}}" onclick="return confirm('Are You Sure?')"><i class="fa fa-trash"></i> Delete</a>@endcan
                                        </li>

                                    </ul>
                                </div>
                            </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
</div>



@endsection