<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>

<head>
    <title>CV of {{$cvdata->staff_name}}</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        @media print {
            .noprint {
                visibility: hidden;
            }
        }

        .center {
            margin-left: auto;
            margin-right: auto;
        }

        tr.border_bottom td {
            border-bottom: 1px solid black;
        }

        p {
            margin: 2px 0px 10px
        }

         .GFG { 
            text-decoration: none; 
            color: #00aaaa;
            font-size: 17px;

    </style>

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="center" style="width: 1050px;">
                    <tbody>
                        <tr class="border_bottom">
                            <td style="text-align:left">
                                <p style="left:0px">
                                    @can('cv-list')
                                    <a class="GFG" href="{{route('staffcv')}}"><i style="color: #00aaaa;" class="fa fa-arrow-circle-left noprint"></i></a>@endcan
                                    @can('cvfile-download')
                                    <div>@if($staffcv_files->count() > 0)
                                        <form action="{{route('staffCvFile',$cvdata->id)}}" target="_blank" >
                                            <input style="color: #00aaaa;" class="noprint" type="submit" value="Files">
                                            @else

                                            @endif
                                            </form>

                                    </div>
                                    @endcan
                                </p>
                                <p style="left:0px"></p>
                                <p style="left:0px">ECL</p>
                                <p style="left:0px">Technical Proposal</p>

                            </td>

                            <td style="text-align:right">
                                </p>
                                <p style="left:0px">
                                    <div>
                                        @can('cv-edit')
                                        <form action="{{route('editStaffcv',$cvdata->id)}}">
                                            <input style="color: #00aaaa;" class="noprint" type="submit" value="Edit">@endcan
                                            <span>
                                                <form>
                                            <input style="color: #00aaaa;" class="noprint" type="button" value="Print" href="" onClick="window.print()">
                                        </form>
                                       
                                        </span>
                                        </form>
                                    </div>
                                </p>
                                <p style="left:0px">CV of {{$cvdata->staff_name}}</p>
                                <p style="left:0px">{{$cvdata->position}}</p>

                            </td>
                        </tr>

                    </tbody>
                </table>
                <p style="font-size: 20px; font-weight: bold; text-align: center; margin: 20px 0px;">
                    Form 5A8 Curriculum Vitae (CV) for {{$cvdata->position}}
                </p>

                <table class="center" width="648" cellspacing="0" cellpadding="0" border="0" style="width: 1050px; border: 1px solid black; margin-top: 30px;">
                    <tbody>
                        <tr>
                            <td width="36" valign="top">
                                <p>
                                    &nbsp;&nbsp;&nbsp;1.
                                </p>
                            </td>
                            <td colspan="2" width="168" valign="top">
                                <p>
                                    Proposed Position for this project
                                </p>
                            </td>
                            <td width="18" valign="top">
                                <p>
                                    :
                                </p>
                            </td>
                            <td width="426" valign="top">
                                <p>
                                    <strong style="font-size: 17px;">{{$cvdata->position}}</strong>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td width="36" valign="top">
                                <br />
                            </td>
                            <td colspan="2" width="168" valign="top">
                                <br />
                            </td>
                            <td width="18" valign="top">
                                <br />
                            </td>
                            <td width="426" valign="top">
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td width="36" valign="top">
                                <p>
                                    &nbsp;&nbsp;&nbsp;2.
                                </p>
                            </td>
                            <td colspan="2" width="168" valign="top">
                                <p>
                                    Name of Staff
                                </p>
                            </td>
                            <td width="18" valign="top">
                                <p>
                                    :
                                </p>
                            </td>
                            <td width="426" valign="top">
                                <p>
                                    <strong>{{$cvdata->staff_name}}</strong>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td width="36" valign="top">
                                <br />
                            </td>
                            <td colspan="2" width="168" valign="top">
                                <br />
                            </td>
                            <td width="18" valign="top">
                                <br />
                            </td>
                            <td width="426" valign="top">
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td width="36" valign="top">
                                <p>
                                    &nbsp;&nbsp;&nbsp;3.
                                </p>
                            </td>
                            <td colspan="2" width="168" valign="top">
                                <p>
                                    Date of Birth
                                </p>
                            </td>
                            <td width="18" valign="top">
                                <p>
                                    :
                                </p>
                            </td>
                            <td width="426" valign="top">
                                <p>
                                    {{\Carbon\Carbon::createFromFormat('Y-m-d', $cvdata->birth_date)->isoFormat('Do MMMM Y')}}
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td width="36" valign="top">
                                <br />
                            </td>
                            <td colspan="2" width="168" valign="top">
                                <br />
                            </td>
                            <td width="18" valign="top">
                                <br />
                            </td>
                            <td width="426" valign="top">
                                <br />
                            </td>
                        </tr>
                        <tr class="border_bottom">
                            <td width="36" valign="top">
                                <p>
                                    &nbsp;&nbsp;&nbsp;4.
                                </p>
                            </td>
                            <td colspan="2" width="168" valign="top">
                                <p>
                                    Nationality
                                </p>
                            </td>
                            <td width="18" valign="top">
                                <p>
                                    :
                                </p>
                            </td>
                            <td width="426" valign="top">
                                <p>
                                    {{$cvdata->nationality}}
                                </p>
                            </td>
                        </tr>

                        <tr>
                            <td width="36" valign="top">
                                <p>
                                    &nbsp;&nbsp;&nbsp;5.
                                </p>
                            </td>
                            <td colspan="2" width="168" valign="top">
                                <p>
                                    Membership of Professional Societies
                                </p>
                            </td>
                            <td width="18" valign="top">
                                <p>
                                    :
                                </p>
                            </td>
                            <td width="426" valign="top">
                                <p>
                                    {{$cvdata->membership}}
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td width="36" valign="top"></td>
                            <td colspan="2" width="168" valign="top"></td>
                            <td width="18" valign="top"></td>
                            <td width="426" valign="top"></td>
                        </tr>
                        <tr>
                            <td width="36" valign="top">
                                <p>
                                    &nbsp;&nbsp;&nbsp;6.
                                </p>
                            </td>
                            <td colspan="2" width="168" valign="top">
                                <p>
                                    Education
                                </p>
                            </td>
                            <td width="18" valign="top">
                                <p>
                                    :
                                </p>
                            </td>
                            <td width="426" valign="top">
                                <p>
                                    {!! Str::limit(strip_tags($cvdata->qualification), 5000, '') !!}
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td width="36" valign="top">
                                <br />
                            </td>
                            <td colspan="2" width="168" valign="top">
                                <br />
                            </td>
                            <td width="18" valign="top">
                                <br />
                            </td>
                            <td width="426" valign="top">
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td width="36" valign="top">
                                <p>
                                    &nbsp;&nbsp;&nbsp;7.
                                </p>
                            </td>
                            <td colspan="2" width="168" valign="top">
                                <p>
                                    Other Training
                                </p>
                            </td>
                            <td width="18" valign="top">
                                <p>
                                    :
                                </p>
                            </td>
                            <td width="426" valign="top">
                                <p>
                                    {{$cvdata->training}}
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td width="36" valign="top">
                                <br />
                            </td>
                            <td colspan="2" width="168" valign="top">
                                <br />
                            </td>
                            <td width="18" valign="top">
                                <br />
                            </td>
                            <td width="426" valign="top">
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td width="36" valign="top">
                                <p>
                                    &nbsp;&nbsp;&nbsp;8.
                                </p>
                            </td>
                            <td colspan="2" width="168" valign="top">
                                <p>
                                    Language and degree of
                                </p>
                                <p>
                                    Proficiency.
                                </p>
                            </td>
                            <td width="18" valign="top">
                                <p>
                                    :
                                </p>
                            </td>
                            
                            <td width="426" valign="top">

                                <table>
                                    <tbody>
                                        <tr style="height: 31px;">
                                            <td style="height: 31px;" width="103">
                                            </td>
                                            <td style="height: 31px;" width="103">
                                                <p><strong><u>Speaking</u></strong></p>
                                            </td>
                                            <td style="height: 31px;" width="103">
                                                <p><strong><u>Reading</u></strong>&nbsp;</p>
                                            </td>
                                            <td style="height: 31px;" width="103">
                                                <p><strong><u>Writing</u></strong></p>
                                            </td>
                                        </tr>
                                        @foreach($cv_staff_languages as $value)
                                        <tr style="height: 30px;">
                                            <td style="height: 30px;" width="103">
                                                <p>{{$value->language}}&nbsp;&nbsp; :</p>
                                            </td>
                                            <td style="height: 30px;" width="103">
                                                <p>{{$value->speaking}}&nbsp;&nbsp;</p>
                                            </td>
                                            <td style="height: 30px;" width="103">
                                                <p>{{$value->reading}}</p>
                                            </td>
                                            <td style="height: 30px;" width="103">
                                                <p>{{$value->writing}}</p>
                                            </td>
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>

                            </td>
                        </tr>
                        <tr>
                            <td width="36" valign="top">
                                <br />
                            </td>
                            <td colspan="2" width="168" valign="top">
                                <br />
                            </td>
                            <td width="18" valign="top">
                                <br />
                            </td>
                            <td width="426" valign="top">
                                <br />
                            </td>
                        </tr>
                        <tr class="border_bottom">
                            <td width="36" valign="top">
                                <p>
                                    &nbsp;&nbsp;&nbsp;9.
                                </p>
                            </td>
                            <td colspan="2" width="168" valign="top">
                                <p>
                                    Countries of work Experience
                                </p>
                            </td>
                            <td width="18" valign="top">
                                <p>
                                    :
                                </p>
                            </td>
                            <td width="426" valign="top">
                                <p>
                                    {{$cvdata->country_w_experience}}
                                </p>
                            </td>
                        </tr>
                        @php
                        $employment_record = json_decode($cvdata->employment_record, true);

                        $employment_record_size = count($employment_record['employer'], COUNT_RECURSIVE);

                        @endphp
                       <tr>
                            <td width="36" valign="top">
                                <p>&nbsp;&nbsp;&nbsp;10.</p>
                            </td>
                            <td colspan="2" width="168" valign="top">
                                <p>
                                    <strong><u>Employment Record:</u></strong>
                                </p>
                            </td>

                            <td width="18" valign="top"></td>
                            <td width="426" valign="top">
                                <p>

                                </p>
                            </td>
                            @for ($i = 0; $i < $employment_record_size; $i++)
                            </tr>
                            <tr>
                                <td width="36" valign="top"></td>
                                <td colspan="2" width="168" valign="top">
                                    <p>
                                        <strong>Employer-{{$i+1}}</strong>
                                    </p>
                                </td>
                                <td width="18" valign="top">
                                    <p>
                                        :
                                    </p>
                                </td>
                                <td width="426" valign="top">
                                    <p>
                                        <strong>{{$employment_record['employer'][$i]}}</strong>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td width="36" valign="top"></td>
                                <td colspan="2" width="168" valign="top">
                                    <p>
                                        Duration
                                    </p>
                                </td>
                                <td width="18" valign="top">
                                    <p>
                                        :
                                    </p>
                                </td>
                                <td width="426" valign="top">
                                    <p>
                                        {{$employment_record['duration'][$i]}}
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td width="36" valign="top"></td>
                                <td colspan="2" width="168" valign="top">
                                    <p>
                                        Position
                                    </p>
                                </td>
                                <td width="18" valign="top">
                                    <p>
                                        :
                                    </p>
                                </td>
                                <td width="426" valign="top">
                                    <p>
                                        <strong>{{$employment_record['employment_position'][$i]}}</strong>
                                    </p>
                                </td>
                            </tr>
                            <tr class="border_bottom">
                                <td width="36" valign="top"></td>
                                <td colspan="2" width="168" valign="top">
                                    <p>
                                        Duties
                                    </p>
                                </td>
                                <td width="18" valign="top">
                                    <p>
                                        :
                                    </p>
                                </td>
                                <td width="426" valign="top">
                                    <p>
                                        {{$employment_record['duties'][$i]}}
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td width="36" valign="top"></td>
                                <td colspan="2" width="168" valign="top"></td>
                                <td width="18" valign="top"></td>
                                <td width="426" valign="top"></td>
                            </tr>
                            @endfor
                            <tr>
                                <td width="36" valign="top">
                                    <p align="center">
                                        11.
                                    </p>
                                </td>
                                <td colspan="2" width="168" valign="top">
                                    <p>
                                        <strong>
                                            WORK UNDERTAKEN THAT BEST ILLUSTRATES HIS CAPABILITY TO HANDLE THIS ASSIGNMENT.
                                        </strong>
                                    </p>
                                </td>
                                <td width="18" valign="top">
                                    <p>
                                        :
                                    </p>
                                </td>
                                <td width="426" valign="top">

                                    <p>
                                        <strong>
                                            Works undertaken below are best illustrating Capability of “{{$cvdata->staff_name}}” for
                                            the position of “{{$cvdata->position}}” of the following Project:
                                        </strong>
                                    </p>
                                </td>
                            </tr>

                            <tr height="0">
                                <td width="36">
                                    <br />
                                </td>
                                <td width="166">
                                    <br />
                                </td>
                                <td width="1">
                                    <br />
                                </td>
                                <td width="19">
                                    <br />
                                </td>
                                <td width="425">
                                    <br />
                                </td>
                            </tr>
                    </tbody>
                </table>
                <table class="center" width="648" cellspacing="0" cellpadding="0" border="0" style="width: 1050px;  margin-top: 3px;">
                    <tbody>
                        @foreach($project_involve_staff_duties as $value)

                        <tr>
                            <td width="36" valign="top"></td>
                            <td width="167" valign="top">
                                <p align="center">
                                    <strong> </strong>
                                </p>
                            </td>
                            <td colspan="2" width="19" valign="top">
                                <p style="margin-top: 20px !important;">
                                    :
                                </p>
                            </td>
                            <td width="426" valign="top">
                                <p style="margin-top: 20px !important;">
                                    <strong>Duration:</strong>
                                    {{ date('F-Y', strtotime($value->start_date)) }}
                                    to {{ date('F-Y', strtotime($value->end_date)) }}<strong></strong>
                                </p>
                                <p>
                                    <strong>
                                        Client: {{$value->project_client_name}}
                                    </strong>
                                    <strong></strong>
                                </p>
                                <p>
                                    <strong>
                                        Project:
                                        <u>
                                            {{$value->project_name}}. 
                                        </u>
                                    </strong>
                                      {{$value->in_location}}
                                </p>

                                {!! $value->project_description !!}

                                <p>
                                    <strong><u>Position held:</u></strong>
                                    <u> <strong>{{$value->project_involve_staff_assign_position}}</strong></u>
                                    <strong><u></u></strong>
                                </p>
                                <p>
                                    <strong>Duties: </strong>
                                    {{$value->project_involve_staff_duties}}
                                </p>
                            </td>
                        </tr>
                        @endforeach

                        <tr>
                            <td width="36" valign="top"></td>
                            <td width="167" valign="top">
                                <p>
                                    <strong> </strong>
                                </p>
                            </td>
                            <td colspan="2" width="19" valign="top"></td>
                            <td width="426" valign="top"></td>
                        </tr>
                    </tbody>
                    <tr height="0">
                        <td width="36">
                            <br />
                        </td>
                        <td width="166">
                            <br />
                        </td>
                        <td width="1">
                            <br />
                        </td>
                        <td width="19">
                            <br />
                        </td>
                        <td width="425">
                            <br />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
</body>

</html>