@extends('layouts.app')

@section('title', 'Staff CV Edit Form')

@section('js')

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>

    <script>

        $(document).ready(function () {
            var max_fields = 100; //maximum input boxes allowed
            var wrapper3 = $(".input_fields_employment_record"); //Fields wrapper
            var add_employment_record = $(".employment_record"); //Add button ID


            var x = 1; //initlal text box count
            $(add_employment_record).click(function (e) { //on add input button click
                var r = confirm("Press a button!\nEither OK or Cancel.");
                if (r == true) {
                    e.preventDefault();
                    if (x < max_fields) { //max input box allowed
                        x++; //text box increment
                        $(wrapper3).append('<div class="row"> <div class="form-group"> <label class="col-md-3 control-label">Employer- (Company Name)</label> <div class="col-md-6"> <input name="employment_record[employer][]" value="" type="text" class="form-control" placeholder="Enter Name of the Company"> {!! $errors->first('product_name', '<small class="text-danger">:message</small>') !!} </div> </div> <div class="form-group"> <label class="col-md-3 control-label">Duration</label> <div class="col-md-6"> <input name="employment_record[duration][]" value="" type="text" class="form-control" placeholder="Enter Duration"> {!! $errors->first('product_name', '<small class="text-danger">:message</small>') !!} </div> </div> <div class="form-group"> <label class="col-md-3 control-label">Position</label> <div class="col-md-6"> <input name="employment_record[employment_position][]" value="" type="text" class="form-control" placeholder="Enter Position"> {!! $errors->first('product_name', '<small class="text-danger">:message</small>') !!} </div> </div> <div class="form-group"> <label class="col-md-3 control-label">Duties</label> <div class="col-md-6"> <textarea class="form-control" name="employment_record[duties][]" id="ssd" rows="3" placeholder="Enter Duties....."></textarea> {!! $errors->first('product_name', '<small class="text-danger">:message</small>') !!} </div> </div><div style="cursor:pointer;background-color:red;  margin-left: 50%; margin-bottom:10px;" class="remove_field3 btn btn-info"><i class="fa fa-close"></i></div></div></div>');
                    }
                } else {
                    return false;
                }


            });

            $(wrapper3).on("click", ".remove_field3", function (e) { //user click on remove text
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
            })
        });

        function readURL(input) {
            if (input.files && input.files[0]) {

                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.image-upload-wrap').hide();

                    $('.file-upload-image').attr('src', e.target.result);
                    $('.file-upload-content').show();

                    $('.image-title').html(input.files[0].name);
                };

                reader.readAsDataURL(input.files[0]);

            } else {
                removeUpload();
            }
        }

        function removeUpload() {
            $('.file-upload-input').replaceWith($('.file-upload-input').clone());
            $('.file-upload-content').hide();
            $('.image-upload-wrap').show();
        }

        $('.image-upload-wrap').bind('dragover', function () {
            $('.image-upload-wrap').addClass('image-dropping');
        });
        $('.image-upload-wrap').bind('dragleave', function () {
            $('.image-upload-wrap').removeClass('image-dropping');
        });

        $(function () {
            $('#datepicker_1').datepicker({
                changeYear: true,
                showButtonPanel: true,
                dateFormat: 'yy',
                yearRange: "-100:+0",
                onClose: function (dateText, inst) {
                    var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                    $(this).datepicker('setDate', new Date(year, 1));
                }
            });
            $(".date-picker-year").focus(function () {
                $(".ui-datepicker-month").hide();
            });
        });

        $("#datepicker").datepicker({

    format: "yyyy",

    viewMode: "years",

    minViewMode: "years"

});
    </script>


@endsection


@section('css')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" rel="stylesheet"/>

    <style>
        .ui-datepicker-calendar {
   display: none;
}
.ui-datepicker-month {
   display: none;
}
.ui-datepicker-next,.ui-datepicker-prev {
  display:none;
}

        hr {
            border-top: 1px dashed #8e44ad;

        }

        #ssd {
            resize: none;
        }

        .file-upload {
            background-color: #ffffff;
            width: 600px;
            margin-left: 24%;
            padding: 20px;

        }

        .file-upload-btn {
            width: 100%;
            margin: 0;
            color: #fff;
            background: #1FB264;
            border: none;
            padding: 10px;
            border-radius: 4px;
            border-bottom: 4px solid #15824B;
            transition: all .2s ease;
            outline: none;
            text-transform: uppercase;
            font-weight: 700;
        }

        .file-upload-btn:hover {
            background: #1AA059;
            color: #ffffff;
            transition: all .2s ease;
            cursor: pointer;
        }

        .file-upload-btn:active {
            border: 0;
            transition: all .2s ease;
        }

        .file-upload-content {
            display: none;
            text-align: center;
        }

        .file-upload-input {
            position: absolute;
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
            outline: none;
            opacity: 0;
            cursor: pointer;
        }

        .image-upload-wrap {
            margin-top: 20px;
            border: 4px dashed #1FB264;
            position: relative;
        }

        .image-dropping,
        .image-upload-wrap:hover {
            background-color: #1FB264;
            border: 4px dashed #ffffff;
        }

        .image-title-wrap {
            padding: 0 15px 15px 15px;
            color: #222;
        }

        .drag-text {
            text-align: center;
        }

        .drag-text h3 {
            font-weight: 100;
            text-transform: uppercase;
            color: #15824B;
            padding: 60px 0;
        }

        .file-upload-image {
            max-height: 200px;
            max-width: 200px;
            margin: auto;
            padding: 20px;
        }

        .remove-image {
            width: 200px;
            margin: 0;
            color: #fff;
            background: #cd4535;
            border: none;
            padding: 10px;
            border-radius: 4px;
            border-bottom: 4px solid #b02818;
            transition: all .2s ease;
            outline: none;
            text-transform: uppercase;
            font-weight: 700;
        }

        .remove-image:hover {
            background: #c13b2a;
            color: #ffffff;
            transition: all .2s ease;
            cursor: pointer;
        }

        .remove-image:active {
            border: 0;
            transition: all .2s ease;
        }

        [data-role="dynamic-fields"] > .form-inline + .form-inline {
            margin-top: 0.5em;
        }

        [data-role="dynamic-fields"] > .form-inline [data-role="add"] {
            display: none;
        }

        [data-role="dynamic-fields"] > .form-inline:last-child [data-role="add"] {
            display: inline-block;
        }

        [data-role="dynamic-fields"] > .form-inline:last-child [data-role="remove"] {
            display: none;
        }
    </style>

@endsection


@section('content')

    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->


    @include('pages.include.beginPageHeader')


    <!-- END PAGE HEADER-->

        <div class="row">

            <div class="col-md-12">
                <div class="tabbable-line boxless tabbable-reversed">

                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_0">

                            {{-- <div class="portlet box blue-hoki">--}}
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-list"></i> Staff CV Edit Form
                                    </div>
                                    
                                    <div class="pull-right">
                                        @can('cv-report')
                                <a style="background-color: transparent; border: none; margin-top: 5px;" class="btn btn-primary" href="{{route('showStaffcv',$cvData->id)}}"><i class="fa fa-info-circle"></i> Details</a>@endcan
                                @can('cv-list')
                                <a style="background-color: transparent; border: none; margin-top: 5px;" class="btn btn-primary" href="{{route('staffcv')}}"><i class="fa fa-list-alt"></i> Staff CV List</a> @endcan
                                </div>
                               

                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->

                                    <form action="{{route('updateStaffcv',$cvData->id)}}" method="post"
                                          class="form-horizontal" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-body">

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Category Name</label>
                                                <div class="col-md-6">
                                                    <select id="single" name="category_id" class="form-control select2">
                                                        <option>Select Category</option>

                                                        @foreach($staff_categories as $item)

                                                            @php
                                                                if ($cvData->category_id == $item->id)
                                                                {
                                                                $selected = 'selected';
                                                                }
                                                                else
                                                                {
                                                                $selected = '';
                                                                }
                                                                echo '<option value="' . $item->id . '"' . $selected . '>' . ' ' .
                                                                    $item->category_name . '</option>';

                                                            @endphp

                                                        @endforeach


                                                    </select>
                                                    {!! $errors->first('brand_id', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                        

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Proposed Position for this
                                                    project</label>
                                                <div class="col-md-6">
                                                    <input name="position" value="{{$cvData->position}}" type="text"
                                                           class="form-control" placeholder="Enter Position Held">


                                                    {!! $errors->first('product_price', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <hr>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Name of Staff/Professional</label>
                                                <div class="col-md-6">
                                                    <input name="staff_name" value="{{$cvData->staff_name}}" type="text"
                                                           class="form-control"
                                                           placeholder="Enter Name of Staff/Professional">


                                                    {!! $errors->first('product_name', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">
                                                    Date of Birth</label>
                                                <div class="col-md-6">
                                                    <input type="text" class="date-picker form-control" id="datepicker_1"
                                                           name="birth_date" value="{{$cvData->birth_date}}"
                                                           value="<?php echo date('Y-m-d'); ?>"
                                                           data-date-format="yyyy-mm-dd"
                                                           placeholder="Birth Date: yyyy-mm-dd">


                                                    {!! $errors->first('start_date', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Nationality</label>
                                                <div class="col-md-6">
                                                    <input name="nationality" value="{{$cvData->nationality}}"
                                                           type="text" class="form-control"
                                                           placeholder="Enter Nationality by Birth">


                                                    {!! $errors->first('product_name', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Membership of Professional
                                                    Societies</label>
                                                <div class="col-md-6">

                                                    <textarea class="form-control" name="membership" id="ssd" rows="2"
                                                              placeholder="Enter Membership">{{$cvData->membership}}</textarea>

                                                    {!! $errors->first('product_name', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Education Qualification</label>

                                                <div class="col-md-6">
                                                    <textarea name="qualification" id="summary-ckeditor"
                                                              rows="3">{{$cvData->qualification}}</textarea>


                                                    {!! $errors->first('product_name', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">
                                                    Passing Year</label>
                                                <div class="col-md-6">
                                                    <input autocomplete="off" name="experience" type="text"
                                                           value="{{$cvData->experience}}" id="datepicker"/>


                                                    {!! $errors->first('start_date', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Other Training</label>
                                                <div class="col-md-6">

                                                    <textarea class="form-control" name="training" id="ssd" rows="3"
                                                              placeholder="Other Training.....">{{$cvData->training}}</textarea>

                                                    {!! $errors->first('product_name', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>


                                            <div class="form-group">

                                                <label class="col-md-3 control-label">Language and Degree of
                                                    Proficiency</label>


                                                <div class="col-md-9">
                                                    <div class="mt-repeater">
                                                        <div data-repeater-list="group-b">
                                                            @foreach($cv_staff_languages as $value)

                                                                <div data-repeater-item class="row">
                                                                    <div class="col-md-2">

                                                                        <label class="control-label">Language Name</label>
                                                                        <input type="text" placeholder="Language Name"
                                                                               name="language"
                                                                               value="{{$value->language}}" class="form-control"/></div>
                                                                    <div class="col-md-2">
                                                                        <label class="control-label">Speaking</label>
                                                                        <input type="text"
                                                                               placeholder="Speaking skill"
                                                                               name="speaking"
                                                                               value="{{$value->speaking}}" class="form-control"/></div>
                                                                    <div class="col-md-2">
                                                                        <label class="control-label">Reading</label>
                                                                        <input type="text"
                                                                               placeholder="Reading Skill"
                                                                               name="reading"
                                                                               value="{{$value->reading}}"
                                                                               class="form-control"/></div>
                                                                    <div class="col-md-2">
                                                                        <label class="control-label">Writing</label>
                                                                        <input type="text"
                                                                               placeholder="writing Skill"
                                                                               name="writing"
                                                                               value="{{$value->writing}}"
                                                                               class="form-control"/></div>


                                                                    <div class="col-md-1">
                                                                        <label class="control-label">&nbsp;</label>
                                                                        <a style="margin-top: 28px;" href="javascript:;"
                                                                           data-repeater-delete class="btn btn-danger">
                                                                            <i class="fa fa-close"></i>
                                                                        </a>
                                                                    </div>

                                                                </div>
                                                            @endforeach

                                                        </div>
                                                        <br>
                                                        <a href="javascript:;" data-repeater-create
                                                           class="btn btn-info mt-repeater-add">
                                                            <i class="fa fa-plus"></i> Add More Language</a>
                                                        <br>
                                                        <br>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Countries of Work
                                                    Experience</label>
                                                <div class="col-md-6">
                                                    <input name="country_w_experience"
                                                           value="{{$cvData->country_w_experience}}" type="text"
                                                           class="form-control"
                                                           placeholder="Enter Countries of Working Experience">


                                                    {!! $errors->first('product_name', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <hr>

                                            <div class="input_fields_employment_record">
                                           
                                                <div class="row">
                                                <center>
                                                    <button style="background-color:green; margin-bottom: 15px;"
                                                            class="employment_record btn btn-info active">Add More
                                                        Employment Record
                                                    </button>
                                                </center>

                                            </div>


                                            @php
                                                $employment_record = json_decode($cvData->employment_record, true);

                                                $employment_record_size = count($employment_record['employer'], COUNT_RECURSIVE);

                                            @endphp

                                            @for ($i = 0; $i < $employment_record_size; $i++)

                                                <div class="row">

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Employer- (Company Name)</label>
                                                        <div class="col-md-6">
                                                            <input name="employment_record[employer][]"
                                                                   value="{{$employment_record['employer'][$i]}}"
                                                                   type="text" class="form-control"
                                                                   placeholder="Enter Name of The Company">


                                                            {!! $errors->first('product_name', '<small class="text-danger">:message</small>') !!}
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Duration</label>
                                                        <div class="col-md-6">
                                                            <input name="employment_record[duration][]"
                                                                   value="{{$employment_record['duration'][$i]}}"
                                                                   type="text" class="form-control"
                                                                   placeholder="Enter Duration">


                                                            {!! $errors->first('product_name', '<small class="text-danger">:message</small>') !!}
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Position</label>
                                                        <div class="col-md-6">
                                                            <input name="employment_record[employment_position][]"
                                                                   value="{{$employment_record['employment_position'][$i]}}"
                                                                   type="text"
                                                                   class="form-control" placeholder="Enter Position">


                                                            {!! $errors->first('product_name', '<small class="text-danger">:message</small>') !!}
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Duties</label>
                                                        <div class="col-md-6">

                                                    <textarea class="form-control" name="employment_record[duties][]"
                                                              id="ssd" rows="3"
                                                              placeholder="Enter Duties.....">{{$employment_record['duties'][$i]}}</textarea>

                                                            {!! $errors->first('product_name', '<small class="text-danger">:message</small>') !!}
                                                        </div>
                                                    </div>

                                                    <div style="cursor:pointer;background-color:red;  margin-left: 50%; margin-bottom:10px;"
                                                         class="remove_field3 btn btn-info"><i class="fa fa-close"></i>
                                                    </div>

                                                </div>

                                            @endfor

                                        </div>
                                            <hr>

                                            <label class="control-label col-md-3"></label>
                                            <div class="form-group">
                                                <div class="col-md-6">
                                                    <label for="">Select Singlr or Multiple File</label>

                                                    <input type="file" multiple name="cv_file[]">

                                                </div>
                                            </div>

                                            <hr>
                                            <label class="control-label col-md-3"></label>
                                            <div class="form-group">
                                                <div class="col-md-6">

                                                    <ul>

                                                        @foreach($staff_File  as $item)
                                                            @php
                                                                $path_parts = pathinfo($item->path);
                                                            @endphp


                                                            <li>
                                                                {{substr($path_parts['basename'], 11) }} &nbsp;
                                                                <a target="_blank"
                                                                   onclick="return confirm('Are You Sure To Delete This Category?')"
                                                                   href="{{route('cvStaffFileDelete',['cvStaffId'=>$item->cv_staff_id,'fileId'=>$item->id])}}"
                                                                   title="Delete Faile">

                                                                    <i style="color: red" class="fa fa-trash "
                                                                       aria-hidden="true">Delete</i>

                                                                </a>
                                                                <hr>
                                                            </li>

                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-actions top">
                                            <div class="row">
                                                <div class="col-md-offset-4 col-md-8">
                                                    <button type="submit" class="btn green">Submit</button>

                                                </div>
                                            </div>
                                        </div>


                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>


@endsection