@extends('layouts.app')

@section('title', 'Project List')


@section('js')

<script>

</script>


@endsection

@section('css')
<style>

</style>


@endsection



@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('pages.include.beginPageHeader')


    <!-- END PAGE HEADER-->

    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs"></i>Responsive Flip Scroll Tables </div>
            <div class="tools">
                <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
                <a href="javascript:;" class="remove" data-original-title="" title=""></a>
            </div>
        </div>
        <div class="portlet-body flip-scroll">

            <table class="table table-bordered">
                <tr>
                    <td colspan="1" style="width:20%"><b>Name of the Consultant</b></td>
                    <td colspan="2">{{$cvdata->consultant_name}}</td>
                </tr>
                <tr>
                    <td colspan="1" class="ssd" style="width:20%"><b>RFP Identification No</b></td>
                    <td colspan="2">{{$cvdata->rfp_no}}</td>
                </tr>
                <tr>
                    <td colspan="1" style="width:20%"><b>Name of the Client</b></td>
                    <td colspan="2">{{$cvdata->client_name}}</td>
                </tr>
            </table>

            <table class="table table-bordered table-condensed flip-content">
             
                <tr>
                    <td colspan="3">1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Proposed Position for this project &nbsp; &nbsp; : &nbsp;&nbsp; <b>{{$cvdata->position}}</b><br><br>2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Name of Staff &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  : &nbsp;&nbsp; <b>{{$cvdata->staff_name}}</b><br><br>3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date of Birth &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  : &nbsp;&nbsp; <b></b>{{\Carbon\Carbon::createFromFormat('Y-m-d', $cvdata->birth_date)->isoFormat('Do MMMM Y')}}<br><br>4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nationality &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  : &nbsp;&nbsp; <b>{{$cvdata->nationality}}</b><br><br></td>

                </tr>
                <tr>
                    <td colspan="3">5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Membership of Professional &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : &nbsp;&nbsp; <b>{{$cvdata->membership}}</b><br><br>6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Education &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  : &nbsp;&nbsp;{!! Str::limit($cvdata->qualification, 5000, '') !!}<br><br>7.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Other Training&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  : &nbsp;&nbsp; <b></b><br><br>8.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Language & Degree of Proficiency  &nbsp;&nbsp;: &nbsp;&nbsp; <b>..444.</b><br><br>9.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Countries of working experience &nbsp;&nbsp;&nbsp; : &nbsp;&nbsp; <b>{{$cvdata->country_w_experience}}</b></td>

                </tr>
                <tr>
                    <td colspan="3"><b>WORK UNDERTAKEN THAT BEST<br>ILLUSTRATE HIS CAPABILITY TO <br>HANDLE THIS ASSIGNMENT.</b></td>
                </tr>

                 <tr>
                    <td colspan="3">Detailed description</td>
                </tr>

            </table>

            
        </div>
    </div>



@endsection