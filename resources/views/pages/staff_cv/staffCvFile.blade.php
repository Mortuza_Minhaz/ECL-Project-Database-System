@extends('layouts.app')

@section('title', 'Staff CV File List')


@section('js')

<script>
    $(document).ready(function() {
        $('#example').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'print',
                'pdf'
            ]
        });
    });
</script>


@endsection


@section('css')

<style>
    #GFG { 
            text-decoration: none; 
            color: black;
        }
    .portlet.box .dataTables_wrapper .dt-buttons {
        margin-top: 0px;
        margin-bottom: 20px;
    }

    .dataTables_wrapper .dt-buttons {
        float: left;
    }

    div.dataTables_wrapper div.dataTables_paginate {
        /* margin: 0; */
        white-space: nowrap;
        /* text-align: right; */
        float: right !important;
    }

    .input-group-sm>.input-group-btn>select.btn,
    .input-group-sm>select.form-control,
    .input-group-sm>select.input-group-addon,
    select.input-sm {
        height: 31px;
        line-height: 30px;
    }
</style>

@endsection


@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('pages.include.beginPageHeader')


    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="dt-buttons" style="margin-top: 5px;">
                        @can('cv-report')
                        <a style="color: black; border: none; background-color: #ecf0f1;" class="dt-button buttons-print btn default" tabindex="0" aria-controls="sample_2" href="{{route('showStaffcv', $cvData->id)}}"><span> <i class="fa fa-info-circle"></i>&nbsp; Details</span>
                        </a>@endcan
                        @can('cv-list')
                        <a style="color: black; border: none; background-color: #ecf0f1;" class="dt-button buttons-print btn default" tabindex="0" aria-controls="sample_2" href="{{route('staffcv')}}"><span> <i class="fa fa-list"></i>&nbsp; Staff CV List</span>
                        </a>@endcan
                    </div>

                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_3" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="all" style="text-align: center;"> File List</th>                                
                            </tr>
                        </thead>
                        <tbody>

                            <tr>
                                <td>
                                     @foreach($staffcv_files as $item)
                                            @php

                                            $path_parts = pathinfo($item->path);
                                            $extention_name = pathinfo($item->path, PATHINFO_EXTENSION)
                                            @endphp

                                             <br><br>
                                    @if($extention_name == 'png' || $extention_name == 'jpg' || $extention_name == 'jpeg')
                                    <a id = "GFG" style="font-size: 20px; width: 30%;" href="{{ ($item->path) }}" download="{{ substr($path_parts['basename'],11) }}">
                                        <i style="color: #15aabf;" class="far fa-file-image fa-lg"></i> {{substr($path_parts['basename'], 11) }}</a> &nbsp;

                                    <br>
                                     @elseif($extention_name == 'docx' || $extention_name == 'doc')
                                     <a id = "GFG" style="font-size: 20px; width: 30%;" href="{{ ($item->path) }}" download="{{ substr($path_parts['basename'],11) }}">
                                        <i style="color: #2980b9;" class="fas fa-file-word fa-lg" aria-hidden="true"></i> {{substr($path_parts['basename'], 11) }}</a> &nbsp;
                                    <br>
                                    @elseif($extention_name == 'pdf')
                                    <a id = "GFG" style="font-size: 20px; width: 30%;" href="{{ ($item->path) }}" download="{{ substr($path_parts['basename'],11) }}">
                                        <i style="color: red" class="fa fa-file-pdf-o fa-lg" aria-hidden="true"></i> {{substr($path_parts['basename'], 11) }}</a> &nbsp;
                                    <br>
                                    @elseif($extention_name == 'zip' || $extention_name == 'rar')
                                    <a id = "GFG" style="font-size: 20px; width: 30%;" href="{{ ($item->path) }}" download="{{ substr($path_parts['basename'],11) }}">
                                        <i style="color: #e67e22;" class="fa fa-file-zip-o fa-lg" aria-hidden="true"></i> {{substr($path_parts['basename'], 11) }}</a> &nbsp;
                                    <br>
                                    @elseif($extention_name == 'mp4' || $extention_name == 'WEBM' || $extention_name == 'flv' || $extention_name == 'AVI' || $extention_name == 'WMV' || $extention_name == 'MPEG')
                                    <a id = "GFG" style="font-size: 20px; width: 30%;" href="{{ ($item->path) }}" download="{{ substr($path_parts['basename'],11) }}">
                                        <i style="color: #15aabf;" class="far fa-file-video fa-lg"></i> {{substr($path_parts['basename'], 11) }}</a> &nbsp;
                                    <br>
                                    @else
                                    <a id = "GFG" style="font-size: 20px; width: 30%;" href="{{ ($item->path) }}" download="{{ substr($path_parts['basename'],11) }}">
                                        <i style="color: #4c6ef5;" class="fas fa-file-alt fa-lg"></i> {{substr($path_parts['basename'], 11) }}</a> &nbsp;
                                    <br>
                                            @endif
                                            @endforeach
                                </td>

                              

                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

</div>
<!-- END CONTENT BODY -->
@endsection

