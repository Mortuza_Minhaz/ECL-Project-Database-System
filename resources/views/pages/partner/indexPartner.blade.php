@extends('layouts.app')

@section('title', 'Partner List')


@section('js')

    <script>
        $(document).ready(function() {
        $('#example').DataTable({
            dom: 'B<"top"lf>rt<"bottom"ip><"clear">',
            buttons: [
                'print',
                'pdf'
            ],
            "language": {
                "lengthMenu": " &nbsp;_MENU_"
            },
            "lengthMenu": [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, "All"]
            ],
            "lengthChange": true,
            columnDefs: [{
                targets: 2,
                searchable: true,
                visible: false,
            }],
            lengthMenu: [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ]

        });

    });
    </script>


@endsection



@section('css')

    <style>
        .fa-file-pdf {
            color: red;
            background-color: white;
        }

        .portlet.box .dataTables_wrapper .dt-buttons {
            margin-top: 0px;
            margin-bottom: 20px;
        }

        .dataTables_wrapper .dt-buttons {
            float: left;
        }

        div.dataTables_wrapper div.dataTables_paginate {
            /* margin: 0; */
            white-space: nowrap;
            /* text-align: right; */
            float: right !important;
        }

        .input-group-sm > .input-group-btn > select.btn,
        .input-group-sm > select.form-control,
        .input-group-sm > select.input-group-addon,
        select.input-sm {
            height: 31px;
            line-height: 30px;
        }
    </style>

@endsection


@section('content')
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->


    @include('pages.include.beginPageHeader')


    <!-- END PAGE HEADER-->

        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        @can('create-partner')
                        <div class="dt-buttons" style="margin-top: 5px;">
                            <a class="dt-button buttons-print btn default" tabindex="0" aria-controls="sample_2"
                               href="{{route('addPartner')}}"><span> <i class="fa fa-plus"></i>&nbsp; Add Partner</span>
                            </a>
                        </div>
                        @endcan

                    </div>

                    <div class="portlet-body">
                        <div class="table-responsive">   
                        <table class="table table-striped table-bordered table-hover" id="example">
                            <thead>
                            <tr>
                                <th style="text-align: center;"> Name of the Company</th>
                                <th style="text-align: center;"> Home Country</th>
                                <th style="text-align: center;">specialization Sector</th>

                                <th style="text-align: center;"> Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($partnerList as $item)
                                <tr>

                                    <td>{{$item->partner_company_name}}</td>
                                    <td style="width: 20%;">{{$item->partner_home_country}}</td>

                                    <td>

                                        @foreach ($item->partner_specialization as $value)
                                            Sector:{{$value->sector}} -- Tags :{{$value->tags}} <br>
                                        @endforeach


                                    </td>

                            <td>
                                <div class="btn-group">
                                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-left" role="menu">
                                        <li>@can('partner-report')
                                            <a style="border: none;" href="{{route('showPartner',$item->id)}}">
                                                <i style="" class="fa fa-eye"></i> Details </a>@endcan
                                        </li>
                                        <li>
                                            @can('edit-partner')
                                            <a style="border: none;" href="{{route('editPartner',$item->id)}}"><i class="fa fa-edit"></i> Edit</a>@endcan
                                        </li>
                                        <li>
                                            @can('delete-partner')
                                            <a href="{{route('deletePartner',$item->id)}}" onclick="return confirm('Are You Sure?')"><i class="fa fa-trash"></i> Delete</a>@endcan
                                        </li>

                                    </ul>
                                </div>
                            </td>

                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>

    </div>
    <!-- END CONTENT BODY -->
@endsection