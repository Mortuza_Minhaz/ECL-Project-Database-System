@extends('layouts.app')

@section('title', 'Partner Edit Form')


@section('js')

    <script>
        $(document).ready(function () {
            var max_fields = 100; //maximum input boxes allowed
            var wrapper = $(".input_fields_bidding"); //Fields wrapper
            var add_bidding = $(".add_field_bidding"); //Add button ID
            var x = 1; //initlal text box count
            $(add_bidding).click(function (e) {
                var r = confirm("Press OK button to add more\n Press Cancel to ignore.");
                if (r == true) {
                    e.preventDefault();
                    if (x < max_fields) { //max input box allowed
                        x++; //text box increment
                        $(wrapper).append('<div class="row"> <label class="control-label col-md-3"></label> <div class="form-group"> <div class="col-md-6"> <label>Project Name</label><textarea class="form-control" name="jv_bidding[name][]" id="ssd" rows="3" placeholder="Enter Project Name"></textarea> </div> </div> <label class="control-label col-md-3"></label> <div class="form-group"> <div class="col-md-6"> <label>Country of Project</label> <input name="jv_bidding[country][]" value="" type="text" class="form-control" placeholder="Enter Country of Project"></div> </div> <label class="control-label col-md-3"></label> <div class="form-group"> <div class="col-md-6"> <label>Procuring Authority</label> <textarea class="form-control" name="jv_bidding[procuring][]" id="ssd" rows="3" placeholder="Enter Procuring Authority"></textarea> </div> </div> <div style="cursor:pointer;background-color:red;  margin-left: 50%; margin-bottom:10px;" class="remove_field btn btn-info"><i class="fa fa-close"></i></div></div>'); //add input box
                    }
                } else {
                    return false;
                }
            });
            $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
            })
        });


        $(document).ready(function () {
            var max_fields = 100; //maximum input boxes allowed
            var wrapper2 = $(".input_fields_projects"); //Fields wrapper
            var add_projects = $(".add_field_projects"); //Add button ID
            var x = 1; //initlal text box count
            $(add_projects).click(function (e) {
                var r = confirm("Press OK button to add more\n Press Cancel to ignore.");
                if (r == true) {
                    e.preventDefault();
                    if (x < max_fields) { //max input box allowed
                        x++; //text box increment
                        $(wrapper2).append('<div class="row"> <label class="control-label col-md-3"></label> <div class="form-group"> <div class="col-md-6"> <label>Project Name</label> <textarea class="form-control" name="jv_project[name][]" id="ssd" rows="3" placeholder="Enter Project Name"></textarea> </div> </div> <label class="control-label col-md-3"></label> <div class="form-group"> <div class="col-md-6"> <label>Country of Project</label> <input name="jv_project[country][]" value="" type="text" class="form-control" placeholder="Enter Country of Project">  </div> </div> <label class="control-label col-md-3"></label> <div class="form-group"> <div class="col-md-6"> <label>Procuring Authority</label> <textarea class="form-control" name="jv_project[procuring][]" id="ssd" rows="3" placeholder="Enter Procuring Authority"></textarea> </div> </div> <div style="cursor:pointer;background-color:red;  margin-left: 50%; margin-bottom:10px;" class="remove_field2 btn btn-info"><i class="fa fa-close"></i></div></div>'); //add input box
                    }
                } else {
                    return false;
                }
            });
            $(wrapper2).on("click", ".remove_field2", function (e) { //user click on remove text
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
            })
        });


        $(document).ready(function () {
            var max_fields = 100; //maximum input boxes allowed
            var wrapper3 = $(".input_fields_contact"); //Fields wrapper
            var add_contact_person = $(".add_field_contact"); //Add button ID
            var x = 1; //initlal text box count
            $(add_contact_person).click(function (e) {
                var r = confirm("Press OK button to add more\n Press Cancel to ignore.");
                if (r == true) {
                    e.preventDefault();
                    if (x < max_fields) { //max input box allowed
                        x++; //text box increment
                        $(wrapper3).append('<div class="row"> <div class="form-group"> <label class="col-md-3 control-label">Name</label> <div class="col-md-6"> <input name="contact_person[name][]" value="" type="text" class="form-control" placeholder="Enter Name"> </div> </div> <div class="form-group"> <label class="col-md-3 control-label">Phone</label> <div class="col-md-6"> <input name="contact_person[phone][]" value="" type="text" class="form-control" placeholder="Enter Phone Number"> </div> </div> <div class="form-group"> <label class="col-md-3 control-label">Email</label> <div class="col-md-6"> <input name="contact_person[email][]" value="" type="text" class="form-control" placeholder="Enter Email ID"> </div> </div> <div style="cursor:pointer;background-color:red;  margin-left: 50%; margin-bottom:10px;" class="remove_field3 btn btn-info"><i class="fa fa-close"></i></div></div>');
                    }
                } else {
                    return false;
                }
            });
            $(wrapper3).on("click", ".remove_field3", function (e) { //user click on remove text
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
            })
        });


        $(document).ready(function () {
            var max_fields = 100;
            var wrapper = $(".input_fields_phone");
            var x = 1;
            $(".add_phone").click(function (e) {
                e.preventDefault();
                if (x < max_fields) {
                    x++;
                    $("#phone").append('<div class="phone"><div id="phone"> <input name="partner_phone[]" value="" type="text" class="form-control" style="margin-top:10px;" placeholder="Enter Phone number"></div> <div style="cursor:pointer;background-color:red; margin-top: 10px;  margin-left: 50%;" class="remove_field btn btn-info"><i class="fa fa-close"></i></div></div>');
                }
            });

            $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
            })
        });

        $(document).ready(function () {
            var max_fields = 100;
            var wrapper2 = $(".input_fields_mail");
            var x = 1;
            $(".add_mail").click(function (e) {
                e.preventDefault();
                if (x < max_fields) {
                    x++;
                    $("#mail").append('<div class="mail"><div id="mail"> <input name="partner_mail[]" value="" type="text" class="form-control" style="margin-top:10px;" placeholder="Enter Email ID"></div> <div style="cursor:pointer;background-color:red; margin-top: 10px;  margin-left: 50%;" class="remove_field2 btn btn-info"><i class="fa fa-close"></i></div></div>');
                }
            });

            $(wrapper2).on("click", ".remove_field2", function (e) { //user click on remove text
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
            })
        });

        $(document).ready(function () {
            var max_fields = 100;
            var wrapper3 = $(".input_fields_address");
            var x = 1;
            $(".add_address").click(function (e) {
                e.preventDefault();
                if (x < max_fields) {
                    x++;
                    $("#address").append('<div style="margin-top: 20px;"  class="address"><div id="address"> <textarea class="form-control" name="partner_address[]" id="ssd" rows="3" placeholder="Enter Partner Address"></textarea></div> <div style="cursor:pointer;background-color:red; margin-top: 10px;  margin-left: 50%;" class="remove_field3 btn btn-info"><i class="fa fa-close"></i></div></div>');
                }
            });

            $(wrapper3).on("click", ".remove_field3", function (e) { //user click on remove text
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
            })
        });
    </script>


@endsection


@section('css')

    <style>
        #ssd {
            resize: none;
        }

        hr {
            border-top: 1px solid black;
        }

        .separator {
            display: flex;
            align-items: center;
            text-align: center;
            padding-bottom: 20px;
            margin-top: 25px;
        }

        .separator::before,
        .separator::after {
            content: '';
            flex: 1;
            border-bottom: 1px solid #000;
        }

        .separator::before {
            margin-right: .25em;
        }

        .separator::after {
            margin-left: .25em;
        }

        [data-role="dynamic-fields"] > .form-inline + .form-inline {
            margin-top: 0.5em;
        }

        [data-role="dynamic-fields"] > .form-inline [data-role="add"] {
            display: none;
        }

        [data-role="dynamic-fields"] > .form-inline:last-child [data-role="add"] {
            display: inline-block;
        }

        [data-role="dynamic-fields"] > .form-inline:last-child [data-role="remove"] {
            display: none;
        }
    </style>

@endsection


@section('content')
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->


    @include('pages.include.beginPageHeader')


    <!-- END PAGE HEADER-->

        <div class="row">

            <div class="col-md-12">
                <div class="tabbable-line boxless tabbable-reversed">


                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_0">

                            {{-- <div class="portlet box blue-hoki">--}}
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-list"></i>Partner Edit Form
                                    </div>
                                    <div class="pull-right">
                                @can('partner-report')
                                <a style="background-color: transparent; border: none; margin-top: 5px;" class="btn btn-primary" href="{{route('showPartner',$partner->id)}}"><i class="fa fa-info-circle"></i> Partner Details</a>@endcan
                                @can('partner-list')
                                <a style="background-color: transparent; border: none; margin-top: 5px;" class="btn btn-primary" href="{{route('partner')}}"><i class="fa fa-list-alt"></i> Partner List</a>@endcan
                                </div>
                                           

                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->

                                    <form action="{{route('updatePartner',$partner->id)}}" method="post"
                                          class="form-horizontal" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-body">

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Name of the Company</label>
                                                <div class="col-md-6">
                                                    <input name="partner_company_name"
                                                           value="{{$partner->partner_company_name}}" type="text"
                                                           class="form-control" placeholder="Enter ">


                                                    {!! $errors->first('partner_company_name', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Sector of Specialization</label>
                                                <div class="col-md-9">
                                                    <div class="mt-repeater">
                                                        <div data-repeater-list="group-b">
                                                            @foreach ($partner->partner_specialization as $value)

                                                                <div data-repeater-item class="row">
                                                                    <div class="col-md-5">
                                                                        <label class="control-label">Name of
                                                                            Sector</label>
                                                                        <input type="text" placeholder="Enter Sector"
                                                                               name="sector_of_specialization[sector]"
                                                                               value="{{$value->sector}}"
                                                                               class="form-control"/></div>
                                                                    <div class="col-md-3">
                                                                        <label class="control-label">Tags</label>
                                                                        <input type="text" placeholder="Enter Tags"
                                                                               name="sector_of_specialization[tags]"
                                                                               value="{{$value->tags}}"
                                                                               class="form-control"/></div>
                                                                    <div class="col-md-2">
                                                                        <label class="control-label">&nbsp;</label>
                                                                        <a style="margin-top: 28px;" href="javascript:;"
                                                                           data-repeater-delete class="btn btn-danger">
                                                                            <i class="fa fa-close"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>

                                                                {{--  echo($value->sector);--}}
                                                            @endforeach


                                                        </div>
                                                        <br>
                                                        <a href="javascript:;" data-repeater-create
                                                           class="btn btn-info mt-repeater-add">
                                                            <i class="fa fa-plus"></i> Add More Tags by Sector</a>
                                                        <br>
                                                        <br>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Home Country</label>
                                                <div class="col-md-6">
                                                    <input name="partner_home_country"
                                                           value="{{$partner->partner_home_country}}" type="text"
                                                           class="form-control" placeholder="Enter Home Country">


                                                    {!! $errors->first('company_home_country', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="input_fields_address">
                                                <div class="address">

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Address</label>
                                                        <div class="col-md-6" id="address">

                                                            <input class="form-control" readonly
                                                                   placeholder="plz click [+] button to add more address">

                                                            {!! $errors->first('partner_address', '<small class="text-danger">:message</small>') !!}
                                                        </div>
                                                        <div class="col-md-3">
                                                            <button style="background-color: green; color: white;"
                                                                    id="b1" class="btn add_address" type="button">+
                                                            </button>
                                                        </div>


                                                    </div>
                                                    <?php
                                                    $partner_address_array = explode("*", $partner->partner_address); ?>

                                                    @for ($i = 0; $i < count($partner_address_array); $i++)
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label"></label>
                                                            <div class="col-md-6" style="margin-top: 20px;"
                                                                 class="address">
                                                                <div id="address"><textarea class="form-control"
                                                                                            name="partner_address[]"
                                                                                            id="ssd" rows="3"
                                                                                            placeholder="Enter Partner Address">{{$partner_address_array[$i]}}</textarea>
                                                                </div>
                                                                <div style="cursor:pointer;background-color:red; margin-top: 10px;  margin-left: 50%;"
                                                                     class="remove_field3 btn btn-info"><i
                                                                            class="fa fa-close"></i></div>
                                                            </div>

                                                        </div>
                                                    @endfor


                                                </div>
                                            </div>

                                            <div class="input_fields_phone">
                                                <div class="phone">

                                                    <div class="form-group">

                                                        <label class="col-md-3 control-label">Phone</label>
                                                        <div class="col-md-6" id="phone">

                                                            <input readonly type="text"
                                                                   class="form-control"
                                                                   placeholder="plz click [+] button to add more Phone Number">


                                                            {!! $errors->first('partner_phone', '<small class="text-danger">:message</small>') !!}
                                                        </div>
                                                        <div class="col-md-3">
                                                            <button style="background-color: green; color: white;"
                                                                    id="b1" class="btn add_phone" type="button">+
                                                            </button>
                                                        </div>
                                                    </div>

                                                    @php $partner_phone = explode(',', $partner->partner_phone);

                                                    @endphp
                                                    @foreach ($partner_phone as $key => $value)

                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label"></label>
                                                            <div class="phone">
                                                                <div class="col-md-6" id="phone"><input
                                                                            name="partner_phone[]"
                                                                            value="{{$value}}"
                                                                            type="text"
                                                                            class="form-control"
                                                                            style="margin-top:10px;"
                                                                            placeholder="Enter Phone number"></div>
                                                                <div style="cursor:pointer;background-color:red; margin-top: 10px;  margin-left: 50%;"
                                                                     class="remove_field btn btn-info"><i
                                                                            class="fa fa-close"></i></div>
                                                            </div>
                                                        </div>

                                                    @endforeach

                                                </div>
                                            </div>

                                            <div class="input_fields_mail">
                                                <div class="mail">

                                                    <div class="form-group">

                                                        <label class="col-md-3 control-label">Email</label>
                                                        <div class="col-md-6" id="mail">

                                                            <input readonly value="" type="text"
                                                                   class="form-control"
                                                                   placeholder="plz click [+] button to add more Email ID">


                                                            {!! $errors->first('partner_mail', '<small class="text-danger">:message</small>') !!}
                                                        </div>
                                                        <div class="col-md-3">
                                                            <button style="background-color: green; color: white;"
                                                                    id="b1" class="btn add_mail" type="button">+
                                                            </button>
                                                        </div>
                                                    </div>

                                                    <?php
                                                    $partner_mail_array = explode(",", $partner->partner_mail);



                                                    ?>
                                                    @for ($i = 0; $i < count($partner_mail_array); $i++)
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label"></label>
                                                            <div class="mail">
                                                                <div class="col-md-6" id="mail"><input
                                                                            name="partner_mail[]"
                                                                            value="{{$partner_mail_array[$i]}}"
                                                                            type="text"
                                                                            class="form-control"
                                                                            style="margin-top:10px;"
                                                                            placeholder="Enter Email ID"></div>
                                                                <div style="cursor:pointer;background-color:red; margin-top: 10px;  margin-left: 50%;"
                                                                     class="remove_field2 btn btn-info"><i
                                                                            class="fa fa-close"></i></div>
                                                            </div>
                                                        </div>
                                                    @endfor
                                                </div>
                                            </div>

                                            <div class="separator">Contact Person</div>

                                            <div class="input_fields_contact">
                                                <div class="row">
                                                    <center>
                                                        <button style="background-color:green; margin-bottom: 15px;"
                                                                class="add_field_contact btn btn-info active">Add More Contact Person
                                                        </button>
                                                    </center>

                                                </div>
                                                @php
                                                    $contact_person = json_decode($partner->contact_person, true);

                                                      $contact_person_size = count($contact_person['name'], COUNT_RECURSIVE);
                                                @endphp
                                                @for ($i = 0; $i < $contact_person_size; $i++)

                                                    <div class="row">
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Name</label>
                                                            <div class="col-md-6">
                                                                <input name="contact_person[name][]"
                                                                       value="{{$contact_person['name'][$i]}}"
                                                                       type="text" class="form-control"
                                                                       placeholder="Enter Name ">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Phone</label>
                                                            <div class="col-md-6"><input name="contact_person[phone][]"
                                                                                         value="{{$contact_person['phone'][$i]}}"
                                                                                         type="text"
                                                                                         class="form-control"
                                                                                         placeholder="Enter Phone Number">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Email</label>
                                                            <div class="col-md-6"><input name="contact_person[email][]"
                                                                                         value="{{$contact_person['email'][$i]}}"
                                                                                         type="text"
                                                                                         class="form-control"
                                                                                         placeholder="Enter Email ID"></div>
                                                        </div>
                                                        <div style="cursor:pointer;background-color:red;  margin-left: 50%; margin-bottom:10px;"
                                                             class="remove_field3 btn btn-info"><i
                                                                    class="fa fa-close"></i></div>
                                                    </div>
                                                @endfor
                                            </div>

                                            <div class="separator">Joint Venture in bidding</div>

                                            <div class="input_fields_bidding">
                                                <div class="row">

                                                    <center>
                                                        <button style="background-color:green;"
                                                                class="add_field_bidding btn btn-info active">Add More JV in Bidding
                                                        </button>
                                                    </center>

                                                </div>

                                                @php
                                                    $jv_bidding = json_decode($partner->jv_bidding, true);

                                                      $jv_bidding_size = count($jv_bidding['name'], COUNT_RECURSIVE);
                                                @endphp
                                                @for ($i = 0; $i < $jv_bidding_size; $i++)
                                                    <div class="row"><label class="control-label col-md-3"></label>
                                                        <div class="form-group">
                                                            <div class="col-md-6"><label>Project Name</label> <textarea
                                                                        class="form-control" name="jv_bidding[name][]"
                                                                        id="ssd" rows="3"
                                                                        placeholder="Enter Project Name">{{$jv_bidding['name'][$i]}}</textarea>
                                                            </div>
                                                        </div>
                                                        <label class="control-label col-md-3"></label>
                                                        <div class="form-group">
                                                            <div class="col-md-6"><label>Country of Project</label>
                                                                <input name="jv_bidding[country][]"
                                                                       value="{{$jv_bidding['country'][$i]}}"
                                                                       type="text"
                                                                       class="form-control" placeholder="Enter Country of Project"></div>
                                                        </div>
                                                        <label class="control-label col-md-3"></label>
                                                        <div class="form-group">
                                                            <div class="col-md-6"><label>Procuring Authority</label>
                                                                <textarea class="form-control"
                                                                          name="jv_bidding[procuring][]" id="ssd"
                                                                          rows="3"
                                                                          placeholder="Enter Procuring Authority">{{$jv_bidding['procuring'][$i]}}</textarea>
                                                            </div>
                                                        </div>
                                                        <div style="cursor:pointer;background-color:red;  margin-left: 50%; margin-bottom:10px;"
                                                             class="remove_field btn btn-info"><i
                                                                    class="fa fa-close"></i></div>
                                                    </div>

                                                @endfor


                                            </div>

                                            <div class="separator" style="">Joint Venture in projects</div>

                                            <div class="input_fields_projects">
                                                <div class="row">

                                                    <center>
                                                        <button style="background-color:green;"
                                                                class="add_field_projects btn btn-info active">Add More Joint Venture in projects
                                                        </button>
                                                    </center>

                                                </div>
                                                @php
                                                    $procuring = json_decode($partner->jv_project, true);

                                                      $procuring_size = count($procuring['name'], COUNT_RECURSIVE);
                                                @endphp
                                                @for ($i = 0; $i < $procuring_size; $i++)

                                                    <div class="row"><label class="control-label col-md-3"></label>
                                                        <div class="form-group">
                                                            <div class="col-md-6"><label>Project Name</label> <textarea
                                                                        class="form-control" name="jv_project[name][]"
                                                                        id="ssd" rows="3"
                                                                        placeholder="Enter Project Name">{{$procuring['name'][$i]}}</textarea>
                                                            </div>
                                                        </div>
                                                        <label class="control-label col-md-3"></label>
                                                        <div class="form-group">
                                                            <div class="col-md-6"><label>Country of Project</label>
                                                                <input name="jv_project[country][]"
                                                                       value="{{$procuring['country'][$i]}}" type="text"
                                                                       class="form-control" placeholder="Enter Country of Project"></div>
                                                        </div>
                                                        <label class="control-label col-md-3"></label>
                                                        <div class="form-group">
                                                            <div class="col-md-6"><label>Procuring Authority</label>
                                                                <textarea class="form-control"
                                                                          name="jv_project[procuring][]" id="ssd"
                                                                          rows="3"
                                                                          placeholder="Enter Procuring Authority">{{$procuring['procuring'][$i]}}</textarea>
                                                            </div>
                                                        </div>
                                                        <div style="cursor:pointer;background-color:red;  margin-left: 50%; margin-bottom:10px;"
                                                             class="remove_field2 btn btn-info"><i
                                                                    class="fa fa-close"></i></div>
                                                    </div>
                                                @endfor

                                            </div>

                                            <hr>

                                            <label class="control-label col-md-3"></label>
                                            <div class="form-group">
                                                <div class="col-md-6">
                                                    <label for="">Select single or Multiple File</label>

                                                    <input type="file" multiple name="partner_files[]">

                                                </div>
                                            </div>

                                            <hr>
                                            <label class="control-label col-md-3"></label>
                                            <div class="form-group">
                                                <div class="col-md-6">

                                                    <ul>

                                                        @foreach($partner_multiple_files  as $item)
                                                            @php
                                                                $path_parts = pathinfo($item->file_path);
                                                            @endphp


                                                            <li>
                                                                {{substr($path_parts['basename'], 11) }} &nbsp;
                                                                <a target="_blank"
                                                                   onclick="return confirm('Are You Sure To Delete This Category?')"
                                                                   href="{{route('PartnerFileDelete',['partnerId'=>$item->partner_id,'fileId'=>$item->id])}}"
                                                                   title="Delete Faile">

                                                                    <i style="color: red" class="fa fa-trash "
                                                                       aria-hidden="true">Delete</i>

                                                                </a>
                                                                <hr>
                                                            </li>

                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="form-actions top">
                                            <div class="row">
                                                <div class="col-md-offset-4 col-md-8">
                                                    <button type="submit" class="btn green">Submit</button>

                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>

@endsection