<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>

<head>
    <title>Partner Details</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        .center {
            margin-left: auto;
            margin-right: auto;
        }

        tr.border_bottom td {
            border-bottom: 1px solid black !important;
        }
        table,
        tr,
        td {
            border: 1px solid black;
            border-collapse: collapse;
            padding: 2px
        }

        p {
            margin: 5px 7px 7px 7px;
        }

        @media print {
            .noprint {
                visibility: hidden;
            }
        }

         .GFG { 
            text-decoration: none; 
            color: blue;
            font-size: 17px;
    </style>

</head>

<body>

<div class="container">
    <div class="row">
        <div class="col-md-12">

            <table class="center" style="width: 1050px; border:0">
                    <tbody>
                        <tr class="border_bottom noprint" style="border:0; ">
                            <td style="text-align:left; border:0">
                                <p style="left:0px">
                                    @can('partner-list')
                                    <div>

                                            <form action="{{route('partner')}}">
                                            <i style="color: #00aaaa;" class="fa fa-arrow-circle-left noprint"></i> <input style="color: #00aaaa;" class="noprint" type="submit" value="Partner List">
                                        
                                            
                                        </form>



                                    </div>
                                    @endcan
                                </p>
                                <p style="left:0px"></p>
                                <p style="left:0px"></p>
                                <p style="left:0px"></p>

                            </td>

                            <td style="text-align:right; border:0;" class="border_bottom">
                                </p>
                                <p style="left:0px">
                                    <div>
                                        @can('edit-partner')
                                         <form action="{{route('editPartner',$partnertData->id)}}">
                                            <input style="color: #00aaaa;" class="noprint" type="submit" value="Edit">@endcan
                                            <span>
                                                <form>
                                            <input style="color: #00aaaa;" class="noprint" type="button" value="Print" href="" onClick="window.print()">
                                        </form>
                                       
                                        </span>
                                        </form>
                                    </div>
                                </p>
                                <p style="left:0px"> </p>
                                <p style="left:0px"></p>

                            </td>
                        </tr>

                    </tbody>
                </table>

            <p style="text-align: center; margin: 15px 15px;"><strong>Partner Information</strong></p>

            <table class="center table" cellspacing="0" cellpadding="0" border="1" style="width: 1050px;">
                <tbody>
                <tr>
                    <td width="319">
                        <p>Name of the company</p>

                    </td>
                    <td width="319">
                        <p>{{$partnertData->partner_company_name}}</p>
                    </td>
                </tr>

                <tr>
                    <td width="319">
                        <p>Sector of Specialization</p>

                    </td>
                    
                    <td width="319">
                        @foreach($specializationsInfo as $item)

                            <p>{{$item->sector}} - {{$item->tags}}</p>
                            <br>
                        @endforeach
                    </td>
                    
                </tr>


                <tr>
                    <td width="319">
                        <p>Home Country</p>
                    </td>
                    <td width="319">
                        <p>{{$partnertData->partner_home_country}}</p>
                    </td>
                </tr>
                @php $partner_address = explode('*', $partnertData->partner_address);

                @endphp


                 @foreach ($partner_address as $key => $value)
                    <tr>
                        @if( $key==0)
                        <td width="319">
                            <center>
                                <p style="color: #00aaaa">Address </p>
                            </center>

                        </td>
                        @else
                        <td width="319">
                            <center>
                                <p style="color: #00aaaa">Address({{ $key+1 }}) </p>
                            </center>

                        </td>
                        @endif
                        <td width="319">

                            <p>{{ $value }} </p>

                        </td>
                    </tr>
                @endforeach


                @php $partner_phone = explode(',', $partnertData->partner_phone);

                @endphp

                @foreach ($partner_phone as $key => $value)
                    <tr>
                        @if($key == 0)
                        <td width="319">
                            <center>
                                <p style="color: #00aaaa"><a>Phone</a></p>
                            </center>

                        </td>
                        @else
                        <td width="319">
                            <center>
                                <p style="color: #00aaaa"><a>Phone</a>({{ $key+1 }}) </p>
                            </center>

                        </td>
                        @endif
                        <td width="319">

                            <p>{{ $value }} </p>

                        </td>
                    </tr>
                @endforeach


                @php $partner_mail = explode(',', $partnertData->partner_mail);

                @endphp

                @foreach ($partner_mail as $key => $value)
                    <tr>
                        @if($key == 0)
                        <td width="319">
                            <center>
                                <p style="color: #00aaaa">Email </p>
                            </center>

                        </td>
                        @else
                        <td width="319">
                            <center>
                                <p style="color: #00aaaa">Email({{ $key+1 }}) </p>
                            </center>

                        </td>
                        @endif
                        <td width="319">

                            <p>{{ $value }}</p>

                        </td>
                    </tr>
                    
                @endforeach



                @php $contact_person = json_decode($partnertData->contact_person, true);
                        $contact_person_size = count($contact_person['name'], COUNT_RECURSIVE);
                @endphp


                @for ($i = 0; $i < $contact_person_size; $i++)
                    <tr>
                        @if($i == 0)
                        <td width="319">                           
                            <center>
                                <p style="color: #00aaaa">Contact Person</p>
                            </center>

                        </td>
                        @else
                        <td width="319">                           
                            <center>
                                <p style="color: #00aaaa">Contact Person ({{$i+1}}) </p>
                            </center>

                        </td>
                        @endif
                        <td width="319">

                            <p>Name: {{$contact_person['name'][$i]}} </p>
                            <p>Phone: {{$contact_person['phone'][$i]}} </p>
                            <p>Email: {{$contact_person['email'][$i]}} </p>
                        </td>
                    </tr>
                @endfor



                @php $jv_bidding = json_decode($partnertData->jv_bidding, true);
                            $jv_bidding_size = count($jv_bidding['name'], COUNT_RECURSIVE);
                @endphp

                 @for ($i = 0; $i < $jv_bidding_size; $i++)
                    <tr>
                        @if($i == 0)
                        <td width="319">
                            <center>
                                <p style="color: burlywood">Joint Venture in Bidding </p>
                            </center>

                        </td>
                        @else
                        <td width="319">
                            <center>
                                <p style="color: burlywood">Joint Venture in Bidding ({{$i+1}}) </p>
                            </center>

                        </td>
                        @endif
                        <td width="319">
                            <p>Project Name: {{$jv_bidding['name'][$i]}} </p>
                            <p>Country of Project: {{$jv_bidding['country'][$i]}} </p>
                            <p>Procuring Authority: {{$jv_bidding['procuring'][$i]}} </p>
                        </td>
                    </tr>
                @endfor

               
                @php $jv_project = json_decode($partnertData->jv_project, true);
                                $jv_project_size = count($jv_project['name'], COUNT_RECURSIVE);
                @endphp

                @for ($i = 0; $i < $jv_project_size; $i++)
                    <tr>
                        @if($i == 0)
                        <td width="319">
                            <center>
                                <p style="color: mediumseagreen">Joint Venture in Project</p>
                            </center>

                        </td>
                        @else
                        <td width="319">
                            <center>
                                <p style="color: mediumseagreen">Joint Venture in Project ({{$i+1}}) </p>
                            </center>

                        </td>
                        @endif
                        <td width="319">

                            <p>Project Name: {{$jv_project['name'][$i]}}</p>
                            <p>Country of Project: {{$jv_project['country'][$i]}}</p>
                            <p>Procuring Authority: {{$jv_project['procuring'][$i]}}</p>
                        </td>
                    </tr>
                @endfor


                @if($partner_multiple_files->count() > 0)
                <tr class="noprint">
                    <td width="319">
                        <center>
                            <p style="color: mediumseagreen"><i class="fa fa-file"></i> <a>Document</a></p>
                        </center>

                    </td>
                    <td width="319">
                        @foreach($partner_multiple_files as $item)
                            @php

                                $path_parts = pathinfo($item->file_path);

                            @endphp

                            <a href="{{ ($item->file_path) }}" download="{{ substr($path_parts['basename'],11) }}">
                                <button type="button" class="btn btn-primary"><i class="fa fa-file-o"
                                                                                 aria-hidden="true"> {{substr($path_parts['basename'], 11) }}</i>
                                </button>
                            </a>
                            <br>

                        @endforeach

                    </td>
                </tr>
                @else

                @endif
                </tbody>
            </table>

            @foreach($projectsInfo as $key=>$item)
            @if($key==0)
                <p style="text-align: center; margin: 15px 15px;"><strong>Project Information Associated with {{$partnertData->partner_company_name}}</strong></p>
            @else
            <p style="text-align: center; margin: 15px 15px;"><strong>Project Information Associated with {{$partnertData->partner_company_name}} ({{$key+1}}) </strong></p>
            @endif
                <table class="center table" cellspacing="0" cellpadding="0" border="1" style="width: 1050px;">
                    <tbody>

                    <tr>
                        <td width="319">
                            <p>Name of the Project</p>
                        </td>
                        <td width="319">
                            <p>{{$item->project_name}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td width="319">
                            <p>Site location</p>
                        </td>
                        <td width="319">
                            <p>{{$item->in_location}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td width="319">
                            <p>Description of assignment</p>
                        </td>
                        <td width="319">
                            <p>{!! $item->project_description !!}</p>
                        </td>
                    </tr>
                    <tr>
                        <td width="319">
                            <p>Duration</p>
                        </td>
                        <td width="319">
                            <p>{{ date('F-Y', strtotime($item->start_date)) }}
                                to {{ date('F-Y', strtotime($item->end_date)) }}</p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            @endforeach

        </div>
    </div>
</div>


<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
</body>

</html>