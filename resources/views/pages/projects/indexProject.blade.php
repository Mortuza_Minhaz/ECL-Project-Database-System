@extends('layouts.app')

@section('title', 'Project List')


@section('js')

<script>
    $(document).ready(function() {
        $('#example').DataTable({
            dom: 'B<"top"lf>rt<"bottom"ip><"clear">',
            buttons: [
                'print',
                'pdf'
            ],
            "language": {
                "lengthMenu": " &nbsp;_MENU_"
            },
            "lengthMenu": [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, "All"]
            ],
            "lengthChange": true,
            columnDefs: [{
                targets: 7,
                searchable: true,
                visible: false
            }],
            lengthMenu: [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ]

        });

    });
</script>


@endsection


@section('css')

<style>
    .portlet.box .dataTables_wrapper .dt-buttons {
        margin-top: 0px;
        margin-bottom: 20px;
    }

    .dataTables_wrapper .dt-buttons {
        float: left;
    }

    div.dataTables_wrapper div.dataTables_paginate {
        /* margin: 0; */
        white-space: nowrap;
        /* text-align: right; */
        float: right !important;
    }

    .input-group-sm>.input-group-btn>select.btn,
    .input-group-sm>select.form-control,
    .input-group-sm>select.input-group-addon,
    select.input-sm {
        height: 31px;
        line-height: 30px;
    }
</style>

@endsection


@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('pages.include.beginPageHeader')

    <!-- END PAGE HEADER-->

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">

                    <div class="dt-buttons" style="margin-top: 5px;">
                        @can('create-project')
                        <a style="color: black; border: none; background-color: #ecf0f1;" class="dt-button buttons-print btn default" tabindex="0" aria-controls="sample_2" href="{{route('addProject')}}"><span> <i class="fa fa-plus"></i>&nbsp; Add Project</span>

                        </a>
                        @endcan
                    </div>
                </div>

                <div class="portlet-body">
                   <div class="table-responsive">   
                    <table class="table table-striped table-bordered table-hover" id="example">
                        <thead>
                            <tr>
                                <th style="text-align: center;"> Sl No.</th>
                                <th style="text-align: center;"> Project Name & Location</th>

                                <th style="text-align: center;"> Project Description</th>
                                <th style="text-align: center;"> Project Cost</th>
                                <th style="text-align: center;"> Start Date</th>
                                <th style="text-align: center;"> Completion Date</th>
                                <th style="text-align: center;"> Services Rendered by the firm</th>
                                <th style="text-align: center;"> Tag</th>
                                <th> Remarks</th>
<!--                                 <th style="text-align: center;"> Action</th> -->
                                <th style="text-align: center;"> Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($datewiseProjectList as $item)
                            <tr>
                                <td style="width: 1%;">{{$loop->iteration}}</td>
                                <td style="width: 15%;">{!! Str::limit($item->project_name, 100, ' .....') !!}. {!!$item->in_location!!}...</td>

                                <td style="width: 20%;">{!! Str::limit(strip_tags($item->project_description), 120, ' .....') !!}</td>
                                <td>{{$item->project_cost}}</td>

                                <td style="width: 10%;">{{ date('F-Y', strtotime($item->start_date)) }}</td>

                                <td style="width: 6%;">{{ date('F-Y', strtotime($item->end_date)) }}</td>

                                <td>{!! Str::limit($item->service_render, 100, ' .....') !!}</td>
                                <td>{{$item->project_tag}}</td>
                                <td style="width: 5%;">{{$item->remarks}}</td>

                                <td>
                                                        <div class="btn-group">
                                                            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                                                <i class="fa fa-angle-down"></i>
                                                            </button>
                                                            <ul class="dropdown-menu pull-left" role="menu">
                                                                <li>@can('project-report')
                                                                    <a style="border: none;" href="{{route('showProject',$item->id)}}">
                                                                        <i style="" class="fa fa-eye"></i> Details </a>@endcan
                                                                </li>
                                                                <li>
                                                                    @can('edit-project')
                                    <a style="border: none;" href="{{route('editProject',$item->id)}}"><i class="fa fa-edit"></i> Edit</a>@endcan
                                                                </li>
                                                                <li>
                                                                    @can('delete-project')
                                    <a href="{{route('deleteProject',$item->id)}}" onclick="return confirm('Are You Sure?')"><i class="fa fa-trash"></i> Delete</a>@endcan
                                                                </li>
                                                                
                                                            </ul>
                                                        </div>
                                                    </td>

                            </tr>
                            @endforeach


                        </tbody>
                    </table>
                </div>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

</div>
<!-- END CONTENT BODY -->
@endsection