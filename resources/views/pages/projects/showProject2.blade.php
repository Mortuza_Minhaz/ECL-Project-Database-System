@extends('layouts.app')

@section('title', 'Project List')


@section('js')

<script>

</script>


@endsection

@section('css')

<style>
	.tool {
  position: relative;
  display: inline-block;
  
}

.tool .tooltext {
  visibility: hidden;
  width: 120px;
  background-color: black;
  color: #fff;
  text-align: center;
  border-radius: 6px;
  padding: 5px 0;
  
  /* Position the tooltip */
  position: absolute;
  z-index: 1;
  bottom: 100%;
  left: 50%;
  margin-left: -60px;
}

.tool:hover .tooltext {
  visibility: visible;
}


</style>


@endsection



@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('pages.include.beginPageHeader')


    <!-- END PAGE HEADER-->

    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs"></i>Responsive Flip Scroll Tables </div>
            <div class="tools">
                <!-- <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a> -->
                <a href="{{route('editProject',$pdata->id)}}" class="tool" data-original-title="" title=""><i style="color:white;" class="fa fa-edit fa-sm"></i><span class="tooltext">Edit</span> </a>
                <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
               <!--  <a href="javascript:;" class="remove" data-original-title="" title=""></a> -->
            </div>
        </div>
        <div class="portlet-body flip-scroll">
            <table class="table table-bordered table-condensed flip-content">
                <tr>
                    <td colspan="2"><b>Assignment Name:</b> {{$pdata->project_name}}</td>
                    <td colspan="1"><b>Country:</b> {{$pdata->country}}</td>
                </tr>
                <tr>
                    <td colspan="2"><b>Assignment Location Within Country:</b> {{$pdata->in_location}}</td>
                    <td colspan="1">Duration of Assignment(months):</td>
                </tr>
                <tr>

                    <td colspan="2"><b>Name of client</b> {{$pdata->client_name}}</td>
                    <td rowspan="3">professional staff provided by our Organization:<br>No of staff: <br><br>No of Staff-Months: {{$pdata->staff_months}}</td>

                </tr>
                <tr>
                    <td><b>Start Date</b>:<br>{{ date('F-Y', strtotime($pdata->start_date)) }}</td>
                    <td><b>Completion Date</b>:<br>{{ date('F-Y', strtotime($pdata->end_date)) }}</td>

                </tr>
                <tr>
                    <td>jan</td>
                    <td>Feb</td>
                </tr>
                <tr>
                    <td colspan="2">Name of joint venture Consultant, if any: {{$pdata->jv_consultant}}</td>
                    <td colspan="1">No of Staff-Months of Professional Staff provided by Joint Venture Consultants: {{$pdata->jv_staff_months}}</td>
                </tr>
                <tr>
                    <td colspan="3"><b>Assigned Professional Staff:</b> </td>

                </tr>
                <tr>
                    <td colspan="3"><b>Detailed Narrative Description of Project:</b> {!! Str::limit($pdata->project_description, 5000, ' .....') !!}<br> <br> (<b>The Project Cost is Taka: {{$pdata->project_cost}} </b>)</td>

                </tr>
                <tr>
                    <td colspan="3"><b>Detailed Description of Actual Services Provided by our staff:</b> {{$pdata->service_render}}</td>
                </tr>

            </table>

            <table class="table table-bordered">
                <tr>
                    <td colspan="1" style="width:20%">Firm;s Name</td>
                    <td colspan="2"><b>{{$pdata->firm_name}}</b></td>
                </tr>
                <tr>
                    <td colspan="1" style="width:20%">Assignment</td>
                    <td colspan="2">Country</td>
                </tr>
            </table>
        </div>
    </div>



@endsection