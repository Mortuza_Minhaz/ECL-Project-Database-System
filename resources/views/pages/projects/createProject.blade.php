@extends('layouts.app')

@section('title', 'Project Form')


@section('js')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>


        $(document).ready(function () {
            var max_fields = 100; //maximum input boxes allowed
            var wrapper = $(".input_fields_wrap"); //Fields wrapper
            var add_button = $(".add_field_button"); //Add button ID
            var x = 1; //initlal text box count
            $(add_button).click(function (e) { //on add input button click
                e.preventDefault();
                if (x < max_fields) { //max input box allowed
                    x++; //text box increment
                    $(wrapper).append('<div class="row"> <label class="control-label col-md-3"></label> <div class="form-group"> <div class="col-md-6"> <label for="">Staff Name</label> <select id="single" name="staff_id[]" class="form-control select2"> <option>Select Staff </option> @foreach($staff as $item) <option value="{{$item->id}}">{{$item->staff_name}} --- {{$item->position}}</option> @endforeach </select> </div> </div> <label class="control-label col-md-3"></label> <div class="form-group"> <div class="col-md-6"> <label for="">Position</label> <input name="assign_position[]" value="" type="text" class="form-control" placeholder="Input Position for this Project"> </div> </div> <label class="control-label col-md-3"></label> <div class="form-group"> <div class="col-md-6"> <label for="">Duties</label> <textarea class="form-control" name="duties[]" id="ssd" rows="3" placeholder="Enter Duty of the Staff" ></textarea> </div> </div> <div style="cursor:pointer;background-color:red;  margin-left: 50%;" class="remove_field btn btn-info"><i class="fa fa-close"></i></div></div>'); //add input box
                }
            });
            $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
            })
        });


        $(document).ready(function () {
            var max_fields = 100;
            var wrapper = $(".input_fields_wrapp");
            var x = 1;
            $(".add-more").click(function (e) {
                e.preventDefault();
                if (x < max_fields) {
                    x++;
                    $("#field").append('<div class="fild"> <input name="project_tag[]" value="" type="text" class="form-control" style="margin-top:10px;" placeholder="Project Tag"> <div style="cursor:pointer;background-color:red;  margin-left: 50%;" class="remove_field btn btn-info"><i class="fa fa-close"></i></div></div>');
                }
            });

            $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
            })
        });


        $(document).ready(function () {
            var max_fields = 100; //maximum input boxes allowed
            var wrapper3 = $(".input_fields_director"); //Fields wrapper
            var add_director = $(".project_director"); //Add button ID


            var x = 1; //initlal text box count
            $(add_director).click(function (e) { //on add input button click
                var r = confirm("Press a button!\nEither OK or Cancel.");
                if (r == true) {
                    e.preventDefault();
                    if (x < max_fields) { //max input box allowed
                        x++; //text box increment
                        $(wrapper3).append('<div class="row">' +
                            ' <p style="text-align: center; font-size: 25px;">Project Director</p>' +
                            ' <div class="form-group"> <label class="col-md-3 control-label">Designation</label>' +
                            ' <div class="col-md-6">' +
                            ' <input name="project_director[designation][]" value="" type="text" class="form-control" placeholder="Enter Designation"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!} </div> </div> <div class="form-group"> <label class="col-md-3 control-label">Name</label> <div class="col-md-6"> <input name="project_director[name][]" value="" type="text" class="form-control" placeholder="Enter Name"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!} </div> </div> <div class="form-group"> <label class="col-md-3 control-label">Phone</label> <div class="col-md-6"> <input name="project_director[phone][]" value="" type="text" class="form-control" placeholder="Enter Phone Number"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!} </div> </div> <div class="form-group"> <label class="col-md-3 control-label">Email</label> <div class="col-md-6"> <input name="project_director[email][]" value="" type="text" class="form-control" placeholder="Enter Email"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!} </div> </div> <div style="cursor:pointer;background-color:red;  margin-left: 50%; margin-bottom:10px;" class="remove_field3 btn btn-info"><i class="fa fa-close"></i></div></div>');
                    }
                } else {
                    return false;
                }


            });

            $(wrapper3).on("click", ".remove_field3", function (e) { //user click on remove text
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
            })
        });

        $(document)
            .ready(function () {
                var max_fields = 100; //maximum input boxes allowed
                var wrapper2 = $(".input_fields_department"); //Fields wrapper
                var add_department = $(".head_of_the_department"); //Add button ID
                var x = 1; //initlal text box count
                $(add_department).click(function (e) { //on add input button click

                    var r = confirm("Press a button!\nEither OK or Cancel.");
                    if (r == true) {
                        e.preventDefault();
                        if (x < max_fields) { //max input box allowed
                            x++; //text box increment
                            $(wrapper2).append('<div class="row"> <p style="text-align: center; font-size: 25px;">Head of the Department</p> <div class="form-group"> <label class="col-md-3 control-label">Designation</label> <div class="col-md-6"> <input name="head_of_the_department[designation][]" value="" type="text" class="form-control" placeholder="Enter Designation"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!} </div> </div> <div class="form-group"> <label class="col-md-3 control-label">Name</label> <div class="col-md-6"> <input name="head_of_the_department[name][]" value="" type="text" class="form-control" placeholder="Enter Name"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!} </div> </div> <div class="form-group"> <label class="col-md-3 control-label">Phone</label> <div class="col-md-6"> <input name="head_of_the_department[phone][]" value="" type="text" class="form-control" placeholder="Enter Phone Number"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!} </div> </div> <div class="form-group"> <label class="col-md-3 control-label">Email</label> <div class="col-md-6"> <input name="head_of_the_department[email][]" value="" type="text" class="form-control" placeholder="Enter Email"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!} </div> </div> <div style="cursor:pointer;background-color:red;  margin-left: 50%; margin-bottom:10px;" class="remove_field2 btn btn-info"><i class="fa fa-close"></i></div></div>');
                        }
                    } else {
                        return false;
                    }
                });

                $(wrapper2).on("click", ".remove_field2", function (e) { //user click on remove text
                    e.preventDefault();
                    $(this).parent('div').remove();
                    x--;
                })
            });


        $(document).ready(function () {
            var max_fields = 100; //maximum input boxes allowed
            var wrapper4 = $(".input_fields_lobby"); //Fields wrapper
            var add_button_lobby = $(".add_field_lobby"); //Add button ID
            var x = 1; //initlal text box count
            $(add_button_lobby).click(function (e) { //on add input button click
                e.preventDefault();

                var r = confirm("Press a button!\nEither OK or Cancel.");
                if (r == true) {
                    if (x < max_fields) { //max input box allowed
                        x++; //text box increment
                        $(wrapper4).append('<div class="row"> <p style="text-align: center; font-size: 25px;">Lobby</p> <div class="form-group"> <label class="col-md-3 control-label">Designation</label> <div class="col-md-6"> <input name="lobby[designation][]" value="" type="text" class="form-control" placeholder="Enter Designation"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!} </div> </div> <div class="form-group"> <label class="col-md-3 control-label">Name</label> <div class="col-md-6"> <input name="lobby[name][]" value="" type="text" class="form-control" placeholder="Enter Name"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!} </div> </div> <div class="form-group"> <label class="col-md-3 control-label">Phone</label> <div class="col-md-6"> <input name="lobby[phone][]" value="" type="text" class="form-control" placeholder="Enter Phone Number"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!} </div> </div> <div class="form-group"> <label class="col-md-3 control-label">Email</label> <div class="col-md-6"> <input name="lobby[email][]" value="" type="text" class="form-control" placeholder="Enter Email"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!} </div> </div> <div style="cursor:pointer;background-color:red;  margin-left: 50%;" class="remove_field4 btn btn-info"><i class="fa fa-close"></i></div></div>'); //add input box
                    }

                } else {
                    return false;
                }


            });
            $(wrapper4).on("click", ".remove_field4", function (e) { //user click on remove text
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
            })
        });


        $(document).ready(function () {
            var max_fields = 100; //maximum input boxes allowed
            var wrapper5 = $(".input_fields_media"); //Fields wrapper
            var add_media = $(".add_field_media"); //Add button ID
            var x = 1; //initlal text box count
            $(add_media).click(function (e) { //on add input button click
                e.preventDefault();
                var r = confirm("Press a button!\nEither OK or Cancel.");
                if (r == true) {
                    if (x < max_fields) { //max input box allowed
                        x++; //text box increment
                        $(wrapper5).append('<div class="row"> <p style="text-align: center; font-size: 25px;">Media</p> <div class="form-group"> <label class="col-md-3 control-label">Name</label> <div class="col-md-6"> <input name="media[name][]" value="" type="text" class="form-control" placeholder="Enter Name"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!} </div> </div> <div class="form-group"> <label class="col-md-3 control-label">Phone</label> <div class="col-md-6"> <input name="media[phone][]" value="" type="text" class="form-control" placeholder="Enter Phone Number"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!} </div> </div> <div class="form-group"> <label class="col-md-3 control-label">Email</label> <div class="col-md-6"> <input name="media[email][]" value="" type="text" class="form-control" placeholder="Enter Email"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!} </div> </div> <div style="cursor:pointer;background-color:red;  margin-left: 50%;" class="remove_field5 btn btn-info"><i class="fa fa-close"></i></div></div>'); //add input box
                    }
                } else {
                    return false;
                }
            });
            $(wrapper5).on("click", ".remove_field5", function (e) { //user click on remove text
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
            })
        });


        function readURL(input) {
            if (input.files && input.files[0]) {

                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.image-upload-wrap').hide();

                    $('.file-upload-image').attr('src', e.target.result);
                    $('.file-upload-content').show();

                    $('.image-title').html(input.files[0].name);
                };

                reader.readAsDataURL(input.files[0]);

            } else {
                removeUpload();
            }
        }

        function removeUpload() {
            $('.file-upload-input').replaceWith($('.file-upload-input').clone());
            $('.file-upload-content').hide();
            $('.image-upload-wrap').show();
        }

        $('.image-upload-wrap').bind('dragover', function () {
            $('.image-upload-wrap').addClass('image-dropping');
        });
        $('.image-upload-wrap').bind('dragleave', function () {
            $('.image-upload-wrap').removeClass('image-dropping');
        });

        $(function () {
            // Remove button click
            $(document).on(
                'click',
                '[data-role="dynamic-fields"] > .form-inline [data-role="remove"]',
                function (e) {
                    e.preventDefault();
                    $(this).closest('.form-inline').remove();
                }
            );
            // Add button click
            $(document).on(
                'click',
                '[data-role="dynamic-fields"] > .form-inline [data-role="add"]',
                function (e) {
                    e.preventDefault();
                    var container = $(this).closest('[data-role="dynamic-fields"]');
                    new_field_group = container.children().filter('.form-inline:first-child').clone();
                    new_field_group.find('input').each(function () {
                        $(this).val('name="assign_staffs[]"');
                    });
                    container.append(new_field_group);

                }
            );
        });

        $(function () {
            $('.date-picker_1').datepicker(
                {
                    dateFormat: "mm/yy",
                    changeMonth: true,
                    changeYear: true,
                    yearRange: "-100:+100",
                    showButtonPanel: true,
                    onClose: function (dateText, inst) {


                        function isDonePressed() {
                            return ($('#ui-datepicker-div').html().indexOf('ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all ui-state-hover') > -1);
                        }

                        if (isDonePressed()) {
                            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                            $(this).datepicker('setDate', new Date(year, month, 1)).trigger('change');

                            $('.date-picker').focusout()//Added to remove focus from datepicker input box on selecting date
                        }
                    },
                    beforeShow: function (input, inst) {

                        inst.dpDiv.addClass('month_year_datepicker')

                        if ((datestr = $(this).val()).length > 0) {
                            year = datestr.substring(datestr.length - 4, datestr.length);
                            month = datestr.substring(0, 2);
                            $(this).datepicker('option', 'defaultDate', new Date(year, month - 1, 1));
                            $(this).datepicker('setDate', new Date(year, month - 1, 1));
                            $(".ui-datepicker-calendar").hide();
                        }
                    }
                })
        });

        

    </script>

@endsection


@section('css')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <style>

        .ui-datepicker-calendar {
            display: none;
        }

        hr {
            border-top: 1px solid black;
        }

        .separator {
            display: flex;
            align-items: center;
            text-align: center;
            padding-bottom: 20px;

        }

        .separator::before,
        .separator::after {
            content: '';
            flex: 1;
            border-bottom: 1px solid #000;

        }

        .separator::before {
            margin-right: .25em;

        }

        .separator::after {
            margin-left: .25em;
        }

        #ssd {
            resize: none;

        }

        .file-upload {
            background-color: #ffffff;
            width: 600px;
            margin: 0 auto;
            padding: 20px;
        }

        .file-upload-btn {
            width: 100%;
            margin: 0;
            color: #fff;
            background: #1FB264;
            border: none;
            padding: 10px;
            border-radius: 4px;
            border-bottom: 4px solid #15824B;
            transition: all .2s ease;
            outline: none;
            text-transform: uppercase;
            font-weight: 700;
        }

        .file-upload-btn:hover {
            background: #1AA059;
            color: #ffffff;
            transition: all .2s ease;
            cursor: pointer;
        }

        .file-upload-btn:active {
            border: 0;
            transition: all .2s ease;
        }

        .file-upload-content {
            display: none;
            text-align: center;
        }

        .file-upload-input {
            position: absolute;
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
            outline: none;
            opacity: 0;
            cursor: pointer;
        }

        .image-upload-wrap {
            margin-top: 20px;
            border: 4px dashed #1FB264;
            position: relative;
        }

        .image-dropping,
        .image-upload-wrap:hover {
            background-color: #1FB264;
            border: 4px dashed #ffffff;
        }

        .image-title-wrap {
            padding: 0 15px 15px 15px;
            color: #222;
        }

        .drag-text {
            text-align: center;
        }

        .drag-text h3 {
            font-weight: 100;
            text-transform: uppercase;
            color: #15824B;
            padding: 60px 0;
        }

        .file-upload-image {
            max-height: 200px;
            max-width: 200px;
            margin: auto;
            padding: 20px;
        }

        .remove-image {
            width: 200px;
            margin: 0;
            color: #fff;
            background: #cd4535;
            border: none;
            padding: 10px;
            border-radius: 4px;
            border-bottom: 4px solid #b02818;
            transition: all .2s ease;
            outline: none;
            text-transform: uppercase;
            font-weight: 700;
        }

        .remove-image:hover {
            background: #c13b2a;
            color: #ffffff;
            transition: all .2s ease;
            cursor: pointer;
        }

        .remove-image:active {
            border: 0;
            transition: all .2s ease;
        }

        [data-role="dynamic-fields"] > .form-inline + .form-inline {
            margin-top: 0.5em;
        }

        [data-role="dynamic-fields"] > .form-inline [data-role="add"] {
            display: none;
        }

        [data-role="dynamic-fields"] > .form-inline:last-child [data-role="add"] {
            display: inline-block;
        }

        [data-role="dynamic-fields"] > .form-inline:last-child [data-role="remove"] {
            display: none;
        }

    </style>

@endsection


@section('content')
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->


    @include('pages.include.beginPageHeader')


    <!-- END PAGE HEADER-->


        <div class="row">

            <div class="col-md-12">
                <div class="tabbable-line boxless tabbable-reversed">

                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_0">

                            {{-- <div class="portlet box blue-hoki">--}}
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-list"></i>Project Form
                                    </div>
                                    <div class="pull-right">
                                <a style="background-color: transparent; border: none; margin-top: 5px;" class="btn btn-primary" href="{{route('project')}}"><i class="fa fa-list-alt"></i> Project List</a>
                                </div>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->

                                    <form action="{{ route('insertProjectinfo')}}" method="post" class="form-horizontal"
                                          enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Assignment/Project Name<span class="required" aria-required="true"> * </span></label>
                                                <div class="col-md-6">

                                                    <textarea class="form-control" name="project_name" id="ssd" rows="3"
                                                              placeholder="Enter Assignment/Project Name"></textarea>

                                                    {!! $errors->first('project_name', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>



                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Country</label>
                                                <div class="col-md-6">
                                                    <select id="country" name="country" class="form-control">
                                                        <option value="Afghanistan">Afghanistan</option>
                                                        <option value="Åland Islands">Åland Islands</option>
                                                        <option value="Albania">Albania</option>
                                                        <option value="Algeria">Algeria</option>
                                                        <option value="American Samoa">American Samoa</option>
                                                        <option value="Andorra">Andorra</option>
                                                        <option value="Angola">Angola</option>
                                                        <option value="Anguilla">Anguilla</option>
                                                        <option value="Antarctica">Antarctica</option>
                                                        <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                                        <option value="Argentina">Argentina</option>
                                                        <option value="Armenia">Armenia</option>
                                                        <option value="Aruba">Aruba</option>
                                                        <option value="Australia">Australia</option>
                                                        <option value="Austria">Austria</option>
                                                        <option value="Azerbaijan">Azerbaijan</option>
                                                        <option value="Bahamas">Bahamas</option>
                                                        <option value="Bahrain">Bahrain</option>
                                                        <option value="Bangladesh" selected="selected">Bangladesh
                                                        </option>
                                                        <option value="Barbados">Barbados</option>
                                                        <option value="Belarus">Belarus</option>
                                                        <option value="Belgium">Belgium</option>
                                                        <option value="Belize">Belize</option>
                                                        <option value="Benin">Benin</option>
                                                        <option value="Bermuda">Bermuda</option>
                                                        <option value="Bhutan">Bhutan</option>
                                                        <option value="Bolivia">Bolivia</option>
                                                        <option value="Bosnia and Herzegovina">Bosnia and Herzegovina
                                                        </option>
                                                        <option value="Botswana">Botswana</option>
                                                        <option value="Bouvet Island">Bouvet Island</option>
                                                        <option value="Brazil">Brazil</option>
                                                        <option value="British Indian Ocean Territory">British Indian
                                                            Ocean Territory
                                                        </option>
                                                        <option value="Brunei Darussalam">Brunei Darussalam</option>
                                                        <option value="Bulgaria">Bulgaria</option>
                                                        <option value="Burkina Faso">Burkina Faso</option>
                                                        <option value="Burundi">Burundi</option>
                                                        <option value="Cambodia">Cambodia</option>
                                                        <option value="Cameroon">Cameroon</option>
                                                        <option value="Canada">Canada</option>
                                                        <option value="Cape Verde">Cape Verde</option>
                                                        <option value="Cayman Islands">Cayman Islands</option>
                                                        <option value="Central African Republic">Central African
                                                            Republic
                                                        </option>
                                                        <option value="Chad">Chad</option>
                                                        <option value="Chile">Chile</option>
                                                        <option value="China">China</option>
                                                        <option value="Christmas Island">Christmas Island</option>
                                                        <option value="Cocos (Keeling) Islands">Cocos (Keeling)
                                                            Islands
                                                        </option>
                                                        <option value="Colombia">Colombia</option>
                                                        <option value="Comoros">Comoros</option>
                                                        <option value="Congo">Congo</option>
                                                        <option value="Congo, The Democratic Republic of The">Congo, The
                                                            Democratic Republic of The
                                                        </option>
                                                        <option value="Cook Islands">Cook Islands</option>
                                                        <option value="Costa Rica">Costa Rica</option>
                                                        <option value="Cote D'ivoire">Cote D'ivoire</option>
                                                        <option value="Croatia">Croatia</option>
                                                        <option value="Cuba">Cuba</option>
                                                        <option value="Cyprus">Cyprus</option>
                                                        <option value="Czech Republic">Czech Republic</option>
                                                        <option value="Denmark">Denmark</option>
                                                        <option value="Djibouti">Djibouti</option>
                                                        <option value="Dominica">Dominica</option>
                                                        <option value="Dominican Republic">Dominican Republic</option>
                                                        <option value="Ecuador">Ecuador</option>
                                                        <option value="Egypt">Egypt</option>
                                                        <option value="El Salvador">El Salvador</option>
                                                        <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                        <option value="Eritrea">Eritrea</option>
                                                        <option value="Estonia">Estonia</option>
                                                        <option value="Ethiopia">Ethiopia</option>
                                                        <option value="Falkland Islands (Malvinas)">Falkland Islands
                                                            (Malvinas)
                                                        </option>
                                                        <option value="Faroe Islands">Faroe Islands</option>
                                                        <option value="Fiji">Fiji</option>
                                                        <option value="Finland">Finland</option>
                                                        <option value="France">France</option>
                                                        <option value="French Guiana">French Guiana</option>
                                                        <option value="French Polynesia">French Polynesia</option>
                                                        <option value="French Southern Territories">French Southern
                                                            Territories
                                                        </option>
                                                        <option value="Gabon">Gabon</option>
                                                        <option value="Gambia">Gambia</option>
                                                        <option value="Georgia">Georgia</option>
                                                        <option value="Germany">Germany</option>
                                                        <option value="Ghana">Ghana</option>
                                                        <option value="Gibraltar">Gibraltar</option>
                                                        <option value="Greece">Greece</option>
                                                        <option value="Greenland">Greenland</option>
                                                        <option value="Grenada">Grenada</option>
                                                        <option value="Guadeloupe">Guadeloupe</option>
                                                        <option value="Guam">Guam</option>
                                                        <option value="Guatemala">Guatemala</option>
                                                        <option value="Guernsey">Guernsey</option>
                                                        <option value="Guinea">Guinea</option>
                                                        <option value="Guinea-bissau">Guinea-bissau</option>
                                                        <option value="Guyana">Guyana</option>
                                                        <option value="Haiti">Haiti</option>
                                                        <option value="Heard Island and Mcdonald Islands">Heard Island
                                                            and Mcdonald Islands
                                                        </option>
                                                        <option value="Holy See (Vatican City State)">Holy See (Vatican
                                                            City State)
                                                        </option>
                                                        <option value="Honduras">Honduras</option>
                                                        <option value="Hong Kong">Hong Kong</option>
                                                        <option value="Hungary">Hungary</option>
                                                        <option value="Iceland">Iceland</option>
                                                        <option value="India">India</option>
                                                        <option value="Indonesia">Indonesia</option>
                                                        <option value="Iran, Islamic Republic of">Iran, Islamic Republic
                                                            of
                                                        </option>
                                                        <option value="Iraq">Iraq</option>
                                                        <option value="Ireland">Ireland</option>
                                                        <option value="Isle of Man">Isle of Man</option>
                                                        <option value="Israel">Israel</option>
                                                        <option value="Italy">Italy</option>
                                                        <option value="Jamaica">Jamaica</option>
                                                        <option value="Japan">Japan</option>
                                                        <option value="Jersey">Jersey</option>
                                                        <option value="Jordan">Jordan</option>
                                                        <option value="Kazakhstan">Kazakhstan</option>
                                                        <option value="Kenya">Kenya</option>
                                                        <option value="Kiribati">Kiribati</option>
                                                        <option value="Korea, Democratic People's Republic of">Korea,
                                                            Democratic People's Republic of
                                                        </option>
                                                        <option value="Korea, Republic of">Korea, Republic of</option>
                                                        <option value="Kuwait">Kuwait</option>
                                                        <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                        <option value="Lao People's Democratic Republic">Lao People's
                                                            Democratic Republic
                                                        </option>
                                                        <option value="Latvia">Latvia</option>
                                                        <option value="Lebanon">Lebanon</option>
                                                        <option value="Lesotho">Lesotho</option>
                                                        <option value="Liberia">Liberia</option>
                                                        <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya
                                                        </option>
                                                        <option value="Liechtenstein">Liechtenstein</option>
                                                        <option value="Lithuania">Lithuania</option>
                                                        <option value="Luxembourg">Luxembourg</option>
                                                        <option value="Macao">Macao</option>
                                                        <option value="Macedonia, The Former Yugoslav Republic of">
                                                            Macedonia, The Former Yugoslav Republic of
                                                        </option>
                                                        <option value="Madagascar">Madagascar</option>
                                                        <option value="Malawi">Malawi</option>
                                                        <option value="Malaysia">Malaysia</option>
                                                        <option value="Maldives">Maldives</option>
                                                        <option value="Mali">Mali</option>
                                                        <option value="Malta">Malta</option>
                                                        <option value="Marshall Islands">Marshall Islands</option>
                                                        <option value="Martinique">Martinique</option>
                                                        <option value="Mauritania">Mauritania</option>
                                                        <option value="Mauritius">Mauritius</option>
                                                        <option value="Mayotte">Mayotte</option>
                                                        <option value="Mexico">Mexico</option>
                                                        <option value="Micronesia, Federated States of">Micronesia,
                                                            Federated States of
                                                        </option>
                                                        <option value="Moldova, Republic of">Moldova, Republic of
                                                        </option>
                                                        <option value="Monaco">Monaco</option>
                                                        <option value="Mongolia">Mongolia</option>
                                                        <option value="Montenegro">Montenegro</option>
                                                        <option value="Montserrat">Montserrat</option>
                                                        <option value="Morocco">Morocco</option>
                                                        <option value="Mozambique">Mozambique</option>
                                                        <option value="Myanmar">Myanmar</option>
                                                        <option value="Namibia">Namibia</option>
                                                        <option value="Nauru">Nauru</option>
                                                        <option value="Nepal">Nepal</option>
                                                        <option value="Netherlands">Netherlands</option>
                                                        <option value="Netherlands Antilles">Netherlands Antilles
                                                        </option>
                                                        <option value="New Caledonia">New Caledonia</option>
                                                        <option value="New Zealand">New Zealand</option>
                                                        <option value="Nicaragua">Nicaragua</option>
                                                        <option value="Niger">Niger</option>
                                                        <option value="Nigeria">Nigeria</option>
                                                        <option value="Niue">Niue</option>
                                                        <option value="Norfolk Island">Norfolk Island</option>
                                                        <option value="Northern Mariana Islands">Northern Mariana
                                                            Islands
                                                        </option>
                                                        <option value="Norway">Norway</option>
                                                        <option value="Oman">Oman</option>
                                                        <option value="Pakistan">Pakistan</option>
                                                        <option value="Palau">Palau</option>
                                                        <option value="Palestinian Territory, Occupied">Palestinian
                                                            Territory, Occupied
                                                        </option>
                                                        <option value="Panama">Panama</option>
                                                        <option value="Papua New Guinea">Papua New Guinea</option>
                                                        <option value="Paraguay">Paraguay</option>
                                                        <option value="Peru">Peru</option>
                                                        <option value="Philippines">Philippines</option>
                                                        <option value="Pitcairn">Pitcairn</option>
                                                        <option value="Poland">Poland</option>
                                                        <option value="Portugal">Portugal</option>
                                                        <option value="Puerto Rico">Puerto Rico</option>
                                                        <option value="Qatar">Qatar</option>
                                                        <option value="Reunion">Reunion</option>
                                                        <option value="Romania">Romania</option>
                                                        <option value="Russian Federation">Russian Federation</option>
                                                        <option value="Rwanda">Rwanda</option>
                                                        <option value="Saint Helena">Saint Helena</option>
                                                        <option value="Saint Kitts and Nevis">Saint Kitts and Nevis
                                                        </option>
                                                        <option value="Saint Lucia">Saint Lucia</option>
                                                        <option value="Saint Pierre and Miquelon">Saint Pierre and
                                                            Miquelon
                                                        </option>
                                                        <option value="Saint Vincent and The Grenadines">Saint Vincent
                                                            and The Grenadines
                                                        </option>
                                                        <option value="Samoa">Samoa</option>
                                                        <option value="San Marino">San Marino</option>
                                                        <option value="Sao Tome and Principe">Sao Tome and Principe
                                                        </option>
                                                        <option value="Saudi Arabia">Saudi Arabia</option>
                                                        <option value="Senegal">Senegal</option>
                                                        <option value="Serbia">Serbia</option>
                                                        <option value="Seychelles">Seychelles</option>
                                                        <option value="Sierra Leone">Sierra Leone</option>
                                                        <option value="Singapore">Singapore</option>
                                                        <option value="Slovakia">Slovakia</option>
                                                        <option value="Slovenia">Slovenia</option>
                                                        <option value="Solomon Islands">Solomon Islands</option>
                                                        <option value="Somalia">Somalia</option>
                                                        <option value="South Africa">South Africa</option>
                                                        <option value="South Georgia and The South Sandwich Islands">
                                                            South Georgia and The South Sandwich Islands
                                                        </option>
                                                        <option value="Spain">Spain</option>
                                                        <option value="Sri Lanka">Sri Lanka</option>
                                                        <option value="Sudan">Sudan</option>
                                                        <option value="Suriname">Suriname</option>
                                                        <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen
                                                        </option>
                                                        <option value="Swaziland">Swaziland</option>
                                                        <option value="Sweden">Sweden</option>
                                                        <option value="Switzerland">Switzerland</option>
                                                        <option value="Syrian Arab Republic">Syrian Arab Republic
                                                        </option>
                                                        <option value="Taiwan, Province of China">Taiwan, Province of
                                                            China
                                                        </option>
                                                        <option value="Tajikistan">Tajikistan</option>
                                                        <option value="Tanzania, United Republic of">Tanzania, United
                                                            Republic of
                                                        </option>
                                                        <option value="Thailand">Thailand</option>
                                                        <option value="Timor-leste">Timor-leste</option>
                                                        <option value="Togo">Togo</option>
                                                        <option value="Tokelau">Tokelau</option>
                                                        <option value="Tonga">Tonga</option>
                                                        <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                                        <option value="Tunisia">Tunisia</option>
                                                        <option value="Turkey">Turkey</option>
                                                        <option value="Turkmenistan">Turkmenistan</option>
                                                        <option value="Turks and Caicos Islands">Turks and Caicos
                                                            Islands
                                                        </option>
                                                        <option value="Tuvalu">Tuvalu</option>
                                                        <option value="Uganda">Uganda</option>
                                                        <option value="Ukraine">Ukraine</option>
                                                        <option value="United Arab Emirates">United Arab Emirates
                                                        </option>
                                                        <option value="United Kingdom">United Kingdom</option>
                                                        <option value="United States">United States</option>
                                                        <option value="United States Minor Outlying Islands">United
                                                            States Minor Outlying Islands
                                                        </option>
                                                        <option value="Uruguay">Uruguay</option>
                                                        <option value="Uzbekistan">Uzbekistan</option>
                                                        <option value="Vanuatu">Vanuatu</option>
                                                        <option value="Venezuela">Venezuela</option>
                                                        <option value="Viet Nam">Viet Nam</option>
                                                        <option value="Virgin Islands, British">Virgin Islands,
                                                            British
                                                        </option>
                                                        <option value="Virgin Islands, U.S.">Virgin Islands, U.S.
                                                        </option>
                                                        <option value="Wallis and Futuna">Wallis and Futuna</option>
                                                        <option value="Western Sahara">Western Sahara</option>
                                                        <option value="Yemen">Yemen</option>
                                                        <option value="Zambia">Zambia</option>
                                                        <option value="Zimbabwe">Zimbabwe</option>
                                                    </select>
                                                    {!! $errors->first('country', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Location within Country</label>
                                                <div class="col-md-6">
                                                    <input name="in_location" value="" type="text" class="form-control"
                                                           placeholder="Assignment Location within Country">


                                                    {!! $errors->first('in_location', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="separator">Client Information</div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Client Name</label>
                                                <div class="col-md-6">
                                                    <select id="single" name="clients_id" class="form-control select2">
                                                        <option value=" ">Select Clients</option>

                                                        @foreach($clients as $item)
                                                            <option value="{{$item->id}}">{{$item->client_name}}</option>

                                                        @endforeach

                                                    </select>
                                                    {!! $errors->first('client_name', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Consulting Fee</label>
                                                <div class="col-md-6">
                                                    <input name="consulting_fee" value="" type="text"
                                                           class="form-control"
                                                           placeholder="Input Consulting Fee">


                                                    {!! $errors->first('consulting_fee', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Lobbing Expenses</label>
                                                <div class="col-md-6">
                                                    <input name="lobbing_expenses" value="" type="text"
                                                           class="form-control"
                                                           placeholder="Input Lobbing Expenses">


                                                    {!! $errors->first('lobbing_expenses', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="input_fields_director">
                                                <div class="row">
                                                    <p style="text-align: center; font-size: 25px;">Project Director</p>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Designation</label>
                                                        <div class="col-md-6">
                                                            <input name="project_director[designation][]" value="" name
                                                                   type="text" class="form-control"
                                                                   placeholder="Enter Designation">


                                                            {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Name</label>
                                                        <div class="col-md-6">
                                                            <input name="project_director[name][]" value="" type="text"
                                                                   class="form-control" placeholder="Enter Name">


                                                            {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Phone</label>
                                                        <div class="col-md-6">
                                                            <input name="project_director[phone][]" value="" type="text"
                                                                   class="form-control" placeholder="Enter phone Number">


                                                            {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Email</label>
                                                        <div class="col-md-6">
                                                            <input name="project_director[email][]" value="" type="text"
                                                                   class="form-control" placeholder="Enter Email">


                                                            {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                                        </div>
                                                    </div>

                                                    <center>
                                                        <button style="background-color:green; margin-bottom: 15px;"
                                                                class="project_director btn btn-info active">Add More Project Director
                                                        </button>
                                                    </center>

                                                </div>
                                            </div>

                                            <div class="input_fields_department">
                                                <div class="row">
                                                    <p style="text-align: center; font-size: 25px;">Head of the
                                                        Department</p>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Designation</label>
                                                        <div class="col-md-6">
                                                            <input name="head_of_the_department[designation][]" value=""
                                                                   type="text" class="form-control"
                                                                   placeholder="Enter Designation">


                                                            {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Name</label>
                                                        <div class="col-md-6">
                                                            <input name="head_of_the_department[name][]" value=""
                                                                   type="text" class="form-control"
                                                                   placeholder="Enter Name">

                                                            {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Phone</label>
                                                        <div class="col-md-6">
                                                            <input name="head_of_the_department[phone][]" value=""
                                                                   type="text" class="form-control"
                                                                   placeholder="Enter Phone Number">

                                                            {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Email</label>
                                                        <div class="col-md-6">
                                                            <input name="head_of_the_department[email][]" value=""
                                                                   type="text" class="form-control"
                                                                   placeholder="Enter Email">


                                                            {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                                        </div>
                                                    </div>

                                                    <center>
                                                        <button style="background-color:green; margin-bottom: 15px;"
                                                                class="head_of_the_department btn btn-info active">Add
                                                            More Head of the Department
                                                        </button>
                                                    </center>

                                                </div>
                                            </div>

                                            <div class="input_fields_lobby">
                                                <div class="row">
                                                    <p style="text-align: center; font-size: 25px;">Lobby</p>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Designation</label>
                                                        <div class="col-md-6">
                                                            <input name="lobby[designation][]" value="" type="text"
                                                                   class="form-control"
                                                                   placeholder="Enter Designation">


                                                            {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Name</label>
                                                        <div class="col-md-6">
                                                            <input name="lobby[name][]" value="" type="text"
                                                                   class="form-control"
                                                                   placeholder="Enter Name">


                                                            {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Phone</label>
                                                        <div class="col-md-6">
                                                            <input name="lobby[phone][]" value="" type="text"
                                                                   class="form-control"
                                                                   placeholder="Enter Phone Number">


                                                            {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Email</label>
                                                        <div class="col-md-6">
                                                            <input name="lobby[email][]" value="" type="text"
                                                                   class="form-control"
                                                                   placeholder="Enter Email">


                                                            {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                                        </div>
                                                    </div>

                                                    <center>
                                                        <button style="background-color:green; margin-bottom: 15px;"
                                                                class="add_field_lobby btn btn-info active">Add More Lobby
                                                        </button>
                                                    </center>

                                                </div>
                                            </div>

                                            <div class="input_fields_media">
                                                <div class="row">
                                                    <p style="text-align: center; font-size: 25px;">Media</p>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Name</label>
                                                        <div class="col-md-6">
                                                            <input name="media[name][]" value="" type="text"
                                                                   class="form-control"
                                                                   placeholder="Enter Name">


                                                            {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Phone</label>
                                                        <div class="col-md-6">
                                                            <input name="media[phone][]" value="" type="text"
                                                                   class="form-control"
                                                                   placeholder="Enter Phone Number">


                                                            {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Email</label>
                                                        <div class="col-md-6">
                                                            <input name="media[email][]" value="" type="text"
                                                                   class="form-control"
                                                                   placeholder="Enter Email">


                                                            {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                                        </div>
                                                    </div>

                                                    <center>
                                                        <button style="background-color:green; margin-bottom: 15px;"
                                                                class="add_field_media btn btn-info active">Add More Media
                                                        </button>
                                                    </center>

                                                </div>
                                            </div>

                                            <hr>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">No of Staff-Months</label>
                                                <div class="col-md-6">
                                                    <input name="staff_months" value="" type="text" class="form-control"
                                                           placeholder="No of Staff-Months">


                                                    {!! $errors->first('staff_months', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Name of Partner,
                                                    if any</label>
                                                <div class="col-md-6">

                                                    <div class="input-group select2-bootstrap-append">
                                                        <select name="jv_consultant[]" id="multi-append"
                                                                class="form-control select2" multiple>
                                                            <option></option>
                                                            @foreach($partners as $item)
                                                                <option value="{{$item->id}}">{{$item->partner_company_name}}</option>

                                                            @endforeach
                                                        </select>

                                                        <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button"
                                                            data-select2-open="multi-append">
                                                        <i class="fas fa-arrow-down"></i>
                                                    </button>
                                                </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Lead Partner</label>
                                                <div class="col-md-6">
                                                    <select id="single" name="jv_lead_consultant"
                                                            class="form-control select2">
                                                        <option value=" ">Select Lead Partner</option>

                                                        @foreach($partners as $item)
                                                            <option value="{{$item->id}}">{{$item->partner_company_name}}</option>

                                                        @endforeach

                                                    </select>
                                                    {!! $errors->first('jv_lead_consultant', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">No of Staff-Months of Joint
                                                    Venture Consultant</label>
                                                <div class="col-md-6">
                                                    <input name="jv_staff_months" value="" type="text"
                                                           class="form-control"
                                                           placeholder="No of Staff-Months of Professional Staff Provided by Joint Venture Consultant">


                                                    {!! $errors->first('jv_staff_months', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Narrative Description of
                                                    Project<span class="required" aria-required="true"> * </span></label>


                                                <div class="col-md-6">
                                                    <textarea name="project_description"
                                                              placeholder="Detailed Description of the Project"
                                                              id="summary-ckeditor" rows="3"></textarea>


                                                    {!! $errors->first('project_description', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Project Cost in (Tk.)<span class="required" aria-required="true"> * </span></label>
                                                <div class="col-md-6">
                                                    <input name="project_cost" value="" type="text" class="form-control"
                                                           placeholder="Input Project Cost">


                                                    {!! $errors->first('project_cost', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">
                                                    Start Date</label>
                                                <div class="col-md-6">
                                                    <input autocomplete="off" name="start_date" type="text" value="<?php echo date('m/Y'); ?>"
                                                           class="date-picker_1" id=""/>


                                                    {!! $errors->first('start_date', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">
                                                    Completion Date</label>
                                                <div class="col-md-6">
                                                    <input autocomplete="off" name="end_date" type="text" value="<?php echo date('m/Y'); ?>"
                                                           class="date-picker_1" id=""/>


                                                    {!! $errors->first('start_date', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Description of Actual Services by
                                                    Staff<span class="required" aria-required="true"> * </span></label>
                                                <div class="col-md-6">

                                                    <textarea class="form-control" name="service_render" id="ssd"
                                                              rows="3"
                                                              placeholder="Detailed Description of Actual Services Provided by our staff"></textarea>

                                                    {!! $errors->first('service_render', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="input_fields_wrapp">
                                                <div class="fild">

                                                    <div class="form-group">

                                                        <label class="col-md-3 control-label">Project Tag</label>
                                                        <div class="col-md-6" id="field">

                                                            <input name="project_tag[]" value="" type="text"
                                                                   class="form-control" placeholder="Project Tag">


                                                            {!! $errors->first('project_tag', '<small class="text-danger">:message</small>') !!}
                                                        </div>
                                                        <div class="col-md-3">
                                                            <button style="background-color: green; color: white;"
                                                                    id="b1" class="btn add-more" type="button">+
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Remarks</label>
                                                <div class="col-md-6">
                                                    <input name="remarks" value="" type="text" class="form-control"
                                                           placeholder="Enter Remarks">


                                                    {!! $errors->first('remarks', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>


                                            <div class="separator">Assigning Professional Staff</div>

                                            <div class="input_fields_wrap">
                                                <div class="row">
                                                    <label class="control-label col-md-3"></label>
                                                    <div class="form-group">
                                                        <div class="col-md-6">
                                                            <label for="">Staff Name</label>
                                                            <select required="" id="single" name="staff_id[]"
                                                                    class="form-control select2">
                                                                <option value=" ">Select Staff</option>

                                                                @foreach($staff as $item)
                                                                    <option value="{{$item->id}}">{{$item->staff_name}}
                                                                        --- {{$item->position}}</option>

                                                                @endforeach

                                                            </select>
                                                        </div>
                                                    </div>

                                                    <label class="control-label col-md-3"></label>
                                                    <div class="form-group">
                                                        <div class="col-md-6">
                                                            <label for="">Position</label>
                                                            <input name="assign_position[]" value="" type="text"
                                                                   class="form-control"
                                                                   placeholder="Input Position for this Project">
                                                        </div>
                                                    </div>

                                                    <label class="control-label col-md-3"></label>
                                                    <div class="form-group">
                                                        <div class="col-md-6">
                                                            <label for="">Duties</label>
                                                            <textarea class="form-control" name="duties[]" id="ssd"
                                                                      rows="3" placeholder="Enter Duty of the Staff"
                                                                      ></textarea>
                                                        </div>
                                                    </div>
                                                    <center>
                                                        <button style="background-color:green;"
                                                                class="add_field_button btn btn-info active">Add More
                                                            Staff
                                                        </button>
                                                    </center>

                                                </div>
                                            </div>

                                            <hr>

                                            <label class="control-label col-md-3"></label>
                                            <div class="form-group">
                                                <div class="col-md-6">
                                                    <label for="">Select File</label>

                                                    <input type="file" multiple name="project_file[]">

                                                </div>
                                            </div>

                                            <div class="form-actions top">
                                                <div class="row">
                                                    <center>
                                                        <button type="submit" class="btn green">Submit</button>
                                                    </center>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    </div>
    <!-- END CONTENT BODY -->
@endsection