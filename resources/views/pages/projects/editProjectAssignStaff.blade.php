@extends('layouts.app')

@section('title', 'Add Client')


@section('js')

<script>
     $(document).ready(function () {
            var max_fields = 100; //maximum input boxes allowed
            var wrapper = $(".input_fields_wrap"); //Fields wrapper
            var add_button = $(".add_field_button"); //Add button ID
            var x = 1; //initlal text box count
            $(add_button).click(function (e) { //on add input button click
                e.preventDefault();
                if (x < max_fields) { //max input box allowed
                    x++; //text box increment
                    $(wrapper).append('<div class="row"> <label class="control-label col-md-3"></label> <div class="form-group"> <div class="col-md-6"> <label for="">Staff Name</label> <select id="single" name="staff_id[]" class="form-control select2"> <option>Select Staff </option> @foreach($staff as $item) <option value="{{$item->id}}">{{$item->staff_name}} --- {{$item->position}}</option> @endforeach </select> </div> </div> <label class="control-label col-md-3"></label> <div class="form-group"> <div class="col-md-6"> <label for="">Position</label> <input name="assign_position[]" value="" type="text" class="form-control" placeholder="Input Position for this Project"> </div> </div> <label class="control-label col-md-3"></label> <div class="form-group"> <div class="col-md-6"> <label for="">Duties</label> <textarea class="form-control" name="duties[]" id="ssd" rows="3" placeholder="Enter " required=""></textarea> </div> </div> <div style=" margin-bottom: 10px;cursor:pointer;background-color:red;  margin-left: 50%;" class="remove_field btn btn-info"><i class="fa fa-close"></i></div></div>'); //add input box
                }
            });
            $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
            })
        });


</script>


@endsection


@section('css')

<style>
    #ssd {
        resize: none;
    }
</style>

@endsection


@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('pages.include.beginPageHeader')


    <!-- END PAGE HEADER-->

    <div class="row">

        <div class="col-md-12">
            <div class="tabbable-line boxless tabbable-reversed">


                <div class="tab-content">
                    <div class="tab-pane active" id="tab_0">

                        {{-- <div class="portlet box blue-hoki">--}}
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                

                            </div>
                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->

                                <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-body">
                                        <div class="input_fields_wrap">
                                                <div class="row">
                                                    <label class="control-label col-md-3"></label>
                                                    <div class="form-group">
                                                        <div class="col-md-6">
                                                            <label for="">Staff Name</label>
                                                            <select required="" id="single" name="staff_id[]"
                                                                    class="form-control select2">
                                                                <option>Select Staff</option>

                                                                @foreach($staff as $item)
                                                                    @php
                                                    if ($stafftData->projects_id == $item->id)
                                                    {
                                                    $selected = 'selected';
                                                    }
                                                    else
                                                    {
                                                    $selected = '';
                                                    }
                                                    echo '<option value="' . $item->id . '"' . $selected . '>' . ' ' .
                                                        $item->staff_name . '</option>';

                                                    @endphp

                                                                @endforeach

                                                            </select>
                                                        </div>
                                                    </div>

                                                    <label class="control-label col-md-3"></label>
                                                    <div class="form-group">
                                                        <div class="col-md-6">
                                                            <label for="">Position</label>
                                                            <input name="assign_position[]" value="" type="text" class="form-control"
                                                           placeholder="Input Position for this Project">
                                                        </div>
                                                    </div>

                                                    <label class="control-label col-md-3"></label>
                                                    <div class="form-group">
                                                        <div class="col-md-6">
                                                            <label for="">Duties</label>
                                                            <textarea class="form-control" name="duties[]" id="ssd"
                                                                      rows="3" placeholder="Enter "
                                                                      required=""></textarea>
                                                        </div>
                                                    </div>
                                                    <center>
                                                        <button style="background-color:green; margin-bottom: 10px;"
                                                                class="add_field_button btn btn-info active">Add More
                                                            Staff
                                                        </button>
                                                    </center>

                                                </div>
                                            </div>
                                       
                                    <div class="form-actions top">
                                        <div class="row">
                                            <div class="col-md-offset-4 col-md-8">
                                                <button type="submit" class="btn green">Submit</button>

                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <!-- END FORM-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>

@endsection