@extends('layouts.app')

@section('title', 'Project List')


@section('js')

<script>
       $(document).ready(function() {
    $('#example').DataTable( {
        "paging":   false,
        "info":     false,
        dom: 'Bfrtip',
        buttons: [
            'print',
            'pdf'
        ]
    } );
} );

</script>


@endsection



@section('css')

<style>
    .fa-file-pdf {
        color: red;
        background-color: white;
    }


     .portlet.box .dataTables_wrapper .dt-buttons {
            margin-top: 0px;
            margin-bottom: 20px;
        }

        .dataTables_wrapper .dt-buttons {
            float: left;
        }

        div.dataTables_wrapper div.dataTables_paginate {
            /* margin: 0; */
            white-space: nowrap;
            /* text-align: right; */
            float: right !important;
        }

        .input-group-sm > .input-group-btn > select.btn, .input-group-sm > select.form-control, .input-group-sm > select.input-group-addon, select.input-sm {
            height: 31px;
            line-height: 30px;
        }
</style>

@endsection

@php
$extention_name = pathinfo($pdata->n_file, PATHINFO_EXTENSION)
@endphp


@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('pages.include.beginPageHeader')


    <!-- END PAGE HEADER-->

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">

                    <div class="dt-buttons" style="margin-top: 5px;">
                        <a class="dt-button buttons-print btn default" tabindex="0" aria-controls="sample_2" href="{{route('addProject')}}"><span> <i class="fa fa-plus"></i>&nbsp; Add Project</span>

                        </a>
                    </div>

                </div>


                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="example">
                        <thead>
                            <tr>
                                <th style="text-align: center;"> Name and Location</th>
                                <th style="text-align: center;"> Description</th>
                                <th style="text-align: center;"> Cost</th>
                                <th style="text-align: center;"> Start Date</th>
                                <th style="text-align: center;"> End Date</th>
                                <th style="text-align: center;"> Service Render</th>
                                <th style="text-align: center;"> Remarks</th>
                                <th style="text-align: center;"> Assigned Staffs</th>
                                <th style="text-align: center;"> Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            <tr>

                                <td>{{$pdata->project_name}}</td>

                                <td style="width: 20%;">{!! $pdata->project_description !!}</td>
                                <td>{{$pdata->project_cost}}</td>
                                <td>{{ date('F-Y', strtotime($pdata->start_date)) }}</td>
                                <td>{{ date('F-Y', strtotime($pdata->end_date)) }}</td>
                               
                              

                                <td>{{$pdata->service_render}}</td>
                                <td>{{$pdata->remarks}}</td>
                                <td>{{$pdata->assign_staffs}}</td>


                                <td style="width: 15%; text-align:center">
                                    @if($extention_name == 'png' || $extention_name == 'jpg' || $extention_name == 'jpeg')
                                    <a href="{{'/'.$pdata->n_file}}" download="{{'/'.$pdata->n_file}}"> <button type="button" class="btn btn-primary"><i class="fa fa-picture-o" aria-hidden="true"></i>
</button></a>

                                    @elseif($extention_name == 'docx' || $extention_name == 'doc')

                                    <a href="{{'/'.$pdata->n_file}}" download="{{'/'.$pdata->n_file}}"> <button type="button" class="btn btn-primary"><i class="far fa-file-word"></i></button></a>

                                    @elseif($extention_name == 'pdf')
                                    <a href="{{'/'.$pdata->n_file}}" download="{{'/'.$pdata->n_file}}"> <button type="button" class="btn btn-primary"><i class="fa fa-file-pdf-o"></i></button></a>
                                    @endif

                                    @can('edit-project')   
                                    <a class="btn btn-primary" href="{{route('editProject',$pdata->id)}}"><i class="fa fa-edit"></i></a>
                                    @endcan

                                    @can('delete-project') 
                                    <a class="btn btn-danger" href="{{route('deleteProject',$pdata->id)}}" onclick="return confirm('Are You Sure?')"><i class="fa fa-trash"></i></a>
                                    @endcan
                                </td>
                            </tr>
                        </tbody>
                    </table>

                     @php
                    $extention_name = pathinfo($pdata->n_file, PATHINFO_EXTENSION)
                    @endphp

                       @if($extention_name == 'png' || $extention_name == 'jpg' || $extention_name == 'jpeg')
                    <div>
                        <center>
                            <div class="form-group">
                                <label class=""></label>
                                <img src="{{ '/'.$pdata->n_file}}" height="250px" width="300px">
                            </div>
                        </center>
                    </div>
                    @elseif($extention_name == 'docx' || $extention_name == 'doc' || $extention_name == 'pdf')
                    <div>
                        <center>
                            <div class="form-group">
                                <label class=""></label>

                                <iframe src="{{'/'.$pdata->n_file}}" frameborder="0" style="width:100%;min-height:640px;"></iframe>

                            </div>
                        </center>
                    </div>

                    @else

                    @endif


                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

</div>
<!-- END CONTENT BODY -->
@endsection




















