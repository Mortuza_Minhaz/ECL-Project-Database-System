<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>

<head>
    <title>Project Details</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        @media print {
            .noprint {
                visibility: hidden;
            }
        }

        .center {
            margin-left: auto;
            margin-right: auto;
        }

        .GFG { 
            text-decoration: none; 
            color: blue;
            font-size: 20px;


        }

        table,
        tr,
        td {
            border: 1px solid black;
            border-collapse: collapse;
            padding: 2px
        }

        p {
            margin: 5px 7px 7px 7px;
        }
    </style>

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <table class="center" style="width: 1000px; border:0">
                    <tbody>
                        <tr class="border_bottom noprint" style="border:0; ">
                            <td style="text-align:left; border:0">
                                <p style="left:0px">
                                    
                                    <div>
                                        @can('project-list')
                                            <form action="{{route('project')}}">
                                            <i style="color: #00aaaa;" class="fa fa-arrow-circle-left noprint"></i> <input style="color: #00aaaa;" class="noprint" type="submit" value="Project List">      
                                        </form>
                                        @endcan


                                    </div>
                                    
                                </p>
                                <p style="left:0px"></p>
                                <p style="left:0px"></p>
                                <p style="left:0px"></p>

                            </td>

                            <td style="text-align:right; border:0;" class="border_bottom">
                                </p>
                                <p style="left:0px">
                                    <div>

                                         <form action="{{route('editProject',$pdata->id)}}">
                                            @can('edit-project')
                                            <input style="color: #00aaaa;" class="noprint" type="submit" value="Edit"> @endcan
                                            <span>
                                                <form>
                                            <input style="color: #00aaaa;" class="noprint" type="button" value="Print" href="" onClick="window.print()">
                                        </form>
                                       
                                        </span>
                                        </form>
                                    </div>
                                </p>
                                <p style="left:0px"> </p>
                                <p style="left:0px"></p>

                            </td>
                        </tr>

                    </tbody>
                </table>
                <table>
                    <p style="margin: 0in; font-size: 16px; font-family: 'Times New Roman',serif; text-align: center;">
                        <strong><span style="font-size: 15px; font-family: 'Arial',sans-serif;">Form 5A2 &nbsp; &nbsp;Consultant&rsquo;s Organization and Experience</span></strong>
                    </p>
                    <p style="margin: 0in; font-size: 16px; font-family: 'Times New Roman',serif; text-align: center;">
                        <strong><span style="font-size: 15px; font-family: 'Arial',sans-serif;">Consultant&rsquo;s Experience</span></strong>
                    </p>
                    <p style="margin: 0in; font-size: 16px; font-family: 'Times New Roman',serif; text-align: center;">
                        <strong><span style="font-size: 15px; font-family: 'Arial',sans-serif;">Major Works Undertaken that best Illustrates Qualifications</span></strong>
                    </p>
                </table>
                <table class="center table" cellspacing="0" cellpadding="0" border="1" style="width: 1000px;">
                    <tbody>
                        <tr>
                            <td colspan="2" width="307">
                                <p><strong>Assignment Name: {{$pdata->project_name}}</strong></p>

                            </td>
                            <td width="336">
                                <p><strong>Country:</strong> {{$pdata->country}}.</p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" width="307">
                                <p><strong>Assignment Location within country: </strong>{{$pdata->in_location}}</p>

                            </td>
                            <td width="336">
                                <p>Duration of assignment
                                    (months): 
                                    @if( Carbon\Carbon::parse($pdata->start_date)->diffInMonths($pdata->end_date, false) == 0)
                                
                                    @else
                                    {{ Carbon\Carbon::parse($pdata->start_date)->diffInMonths($pdata->end_date, false)}}
                                    Months.
                                    @endif
                                </p>
                            </td>
                        </tr>
                        <tr>

                            <td colspan="2" width="307">
                                 
                                <p><strong>Name of Client</strong><strong>:@if(isset($clientName))</strong>{{$clientName->client_name}}</p>@endif
                                
                            </td>
                            <td rowspan="2" width="336">
                                <p>Professional staff provided by our Organization:</p>
                                <p>No of Staff: {{$project_involve_staff_id_array_count}}</p>
                                <p>No. of Staff-Months: {{$pdata->staff_months}}</p>
                            </td>
                        </tr>
                        <tr>
                            <td width="156">
                                <p><strong>Start Date:</strong></p>
                                <p>(Month/Year)</p>
                                <p>{{ date('F-Y', strtotime($pdata->start_date)) }}</p>
                            </td>
                            <td width="151">
                                <p><strong>Completion Date:</strong></p>
                                <p>(Month/Year)</p>
                                <p>{{ date('F-Y', strtotime($pdata->end_date)) }}</p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" width="307">
                                <p>Name of Joint Venture Consultant, if any:

                                    @if(isset($project_involve_lead_partners)) 


                                    {{$project_involve_lead_partners->lead_consultant}},
                                    @endif

                                    @if($project_involve_partners)
                                    @forelse($project_involve_partners as $item)
                                    @if($loop->last)
                                    {{$item->partner_company_name}}

                                    @else
                                    {{$item->partner_company_name.','}}
                                    @endif
                                    @empty
                                    N/A
                                    @endforelse

                                    @endif
                                </p>
                            </td>
                            <td width="336">
                                <p>No of Staff-Months of Professional Staff provided by Joint Venture
                                    Consultants: {{$pdata->jv_staff_months}}</p>
                            </td>
                        </tr>

                        @if($project_involve_staff_position->count() > 0)
                        <tr>
                            <td colspan="3" width="1100">
                                <table class="table" style="border:none">
                                    <tbody style="border:none">
                                        <tr style="border:none">
                                            <td style="border:none" colspan="2" width="623">
                                                <p><b>Assigned Professional Staff: </b></p>
                                            </td>
                                        </tr>
                                        <?php $i = 0; ?>
                                        @foreach($project_involve_staff_position as $value)

                                        <tr style="border:none">
                                            <td style="border:none" width="300">
                                                <p>{{$value->staff_name}}</p>


                                            </td>
                                            <td style="border:none" width="432">
                                                <p>{{$value->assign_position}}</p>
                                            </td>
                                        </tr>
                                        @endforeach


                                        <tr style="border:none">
                                            <td style="border:none" colspan="2" width="623">
                                                <p>And other supporting Technical staffs in each site</p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        @else
                        @endif
                        <tr>
                            <td colspan="3" width="643">
                                <p><strong><u>Detailed Narrative Description of Project:</u></strong></p>
                                <p>{!! Str::limit($pdata->project_description, 5000, ' .....') !!}</p>
                                <p><strong>(The Project Cost is Taka</strong><strong>: 76.30 Crore.)</strong></p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" width="643">
                                <p><strong><u>Detailed Description of Actual Services Provided by our
                                            staff:</u></strong> {{$pdata->service_render}}</p>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <table class="center" cellspacing="0" cellpadding="0" border="1" style="width: 1000px; margin-top:5px;">
                    <tbody>
                        <tr>
                            <td width="99">
                                <p>Firm&rsquo;s Name</p>
                            </td>
                            <td width="544">
                                <p>
                                    <strong>
                                        @if(isset($project_involve_lead_partners))
                                        A Joint Venture of {{$project_involve_lead_partners->lead_consultant}},
                                        @endif

                                        @if(isset($project_involve_partners))
                                        @foreach($project_involve_partners as $item)
                                        @if($loop->last)
                                        and {{$item->partner_company_name}}

                                        @else
                                        {{$item->partner_company_name.','}}
                                        @endif
                                        @endforeach
                                        @endif
                                    </strong>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td width="99">
                                <p>Authorized</p>
                                <p>Signature:</p>
                            </td>
                            <td width="544">
                                <p>&nbsp;</p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @can('projectfile-download')
    @if($project_multiple_files->count() > 0)
    <center><i class="icon-docs"></i><a class="noprint GFG" style="margin-top: 20px; color: #00aaaa;" target="_blank" href="{{route('projectFile',$pdata->id)}}">Project Files</a></center>
    @else
   
    @endif
    @endcan


    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
</body>

</html>