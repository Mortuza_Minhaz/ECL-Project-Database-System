@extends('layouts.app')

@section('title', 'Project Edit Form')


@php

    $project_tag = explode(',',$projectData->project_tag);

@endphp

@section('js')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


    <script>


        $(document).ready(function () {
            var max_fields = 100; //maximum input boxes allowed
            var wrapper = $(".input_fields_wrap"); //Fields wrapper
            var add_button = $(".add_field_button"); //Add button ID
            var x = 1; //initlal text box count
            $(add_button).click(function (e) { //on add input button click
                e.preventDefault();
                if (x < max_fields) { //max input box allowed
                    x++; //text box increment
                    $(wrapper).append('<div class="row"> <label class="control-label col-md-3"></label> <div class="form-group"> <div class="col-md-6"> <label for="">Staff Name</label> <select id="single" name="staff_id[]" class="form-control select2"> <option value=" ">Select Staff</option> @foreach($staff as $item) <option value="{{$item->id}}">{{$item->staff_name}} --- {{$item->position}}</option> @endforeach </select> </div> </div> <label class="control-label col-md-3"></label> <div class="form-group"> <div class="col-md-6"> <label for="">Position</label> <input name="assign_position[]" value="" type="text" class="form-control" placeholder="Input Position for this Project"> </div> </div> <label class="control-label col-md-3"></label> <div class="form-group"> <div class="col-md-6"> <label for="">Duties</label> <textarea class="form-control" name="duties[]" id="ssd" rows="3" placeholder="Enter Duty of the Staff"></textarea> </div> </div> <div style="cursor:pointer;background-color:red;  margin-left: 50%;" class="remove_field btn btn-info"><i class="fa fa-close"></i></div></div>'); //add input box
                }
            });
            $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
            })
        });


        $(document).ready(function () {


            var max_fields = 100;
            var wrapper = $(".input_fields_wrapp");
            var x = 1;

            if (1 == 1) {
                var data_json = <?php echo json_encode($project_tag, JSON_HEX_TAG); ?>;
                var theRemovedElement = data_json.shift(); // theRemovedElement == 1

                // set select options
                var $el = $(data_json);

                /* console.log($el);*/

                $el.empty(); // remove old options
                $.each($el, function (key, value) {
                    $("#field").append('<div class="fild"> <input name="project_tag[]" value="' + value + '" type="text" class="form-control" style="margin-top:10px;" placeholder="Project Tag"> <div style="cursor:pointer;background-color:red;  margin-left: 50%;" class="remove_field btn btn-info"><i class="fa fa-close"></i></div></div>');
                });


            }


            $(".add-more").click(function (e) {
                e.preventDefault();
                if (x < max_fields) {
                    x++;
                    $("#field").append('<div class="fild"> <input name="project_tag[]" value="" type="text" class="form-control" style="margin-top:10px;" placeholder="Project Tag"> <div style="cursor:pointer;background-color:red;  margin-left: 50%;" class="remove_field btn btn-info"><i class="fa fa-close"></i></div></div>');
                }
            });

            $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
            })
        });


        $(document).ready(function () {
            var max_fields = 100; //maximum input boxes allowed
            var wrapper3 = $(".input_fields_director"); //Fields wrapper
            var add_director = $(".project_director"); //Add button ID


            var x = 1; //initlal text box count
            $(add_director).click(function (e) { //on add input button click
                var r = confirm("Press a button!\nEither OK or Cancel.");
                if (r == true) {
                    e.preventDefault();
                    if (x < max_fields) { //max input box allowed
                        x++; //text box increment
                        $(wrapper3).append('<div class="row">' +
                            ' <p style="text-align: center; font-size: 25px;">Project Director</p>' +
                            ' <div class="form-group"> <label class="col-md-3 control-label">Designation</label>' +
                            ' <div class="col-md-6">' +
                            ' <input name="project_director[designation][]" value="" type="text" class="form-control" placeholder="Enter Designation"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!} </div> </div> <div class="form-group"> <label class="col-md-3 control-label">Name</label> <div class="col-md-6"> <input name="project_director[name][]" value="" type="text" class="form-control" placeholder="Enter Name"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!} </div> </div> <div class="form-group"> <label class="col-md-3 control-label">Phone</label> <div class="col-md-6"> <input name="project_director[phone][]" value="" type="text" class="form-control" placeholder="Enter Phone Number"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!} </div> </div> <div class="form-group"> <label class="col-md-3 control-label">Email</label> <div class="col-md-6"> <input name="project_director[email][]" value="" type="text" class="form-control" placeholder="Enter Email"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!} </div> </div> <div style="cursor:pointer;background-color:red;  margin-left: 50%; margin-bottom:10px;" class="remove_field3 btn btn-info"><i class="fa fa-close"></i></div></div>');
                    }
                } else {
                    return false;
                }


            });

            $(wrapper3).on("click", ".remove_field3", function (e) { //user click on remove text
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
            })
        });

        $(document)
            .ready(function () {
                var max_fields = 100; //maximum input boxes allowed
                var wrapper2 = $(".input_fields_department"); //Fields wrapper
                var add_department = $(".head_of_the_department"); //Add button ID
                var x = 1; //initlal text box count
                $(add_department).click(function (e) { //on add input button click

                    var r = confirm("Press a button!\nEither OK or Cancel.");
                    if (r == true) {
                        e.preventDefault();
                        if (x < max_fields) { //max input box allowed
                            x++; //text box increment
                            $(wrapper2).append('<div class="row"> <p style="text-align: center; font-size: 25px;">Head of the Department</p> <div class="form-group"> <label class="col-md-3 control-label">Designation</label> <div class="col-md-6"> <input name="head_of_the_department[designation][]" value="" type="text" class="form-control" placeholder="Enter Designation"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!} </div> </div> <div class="form-group"> <label class="col-md-3 control-label">Name</label> <div class="col-md-6"> <input name="head_of_the_department[name][]" value="" type="text" class="form-control" placeholder="Enter Name"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!} </div> </div> <div class="form-group"> <label class="col-md-3 control-label">Phone</label> <div class="col-md-6"> <input name="head_of_the_department[phone][]" value="" type="text" class="form-control" placeholder="Enter Phone Number"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!} </div> </div> <div class="form-group"> <label class="col-md-3 control-label">Email</label> <div class="col-md-6"> <input name="head_of_the_department[email][]" value="" type="text" class="form-control" placeholder="Enter Email"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!} </div> </div> <div style="cursor:pointer;background-color:red;  margin-left: 50%; margin-bottom:10px;" class="remove_field2 btn btn-info"><i class="fa fa-close"></i></div></div>');
                        }
                    } else {
                        return false;
                    }
                });

                $(wrapper2).on("click", ".remove_field2", function (e) { //user click on remove text
                    e.preventDefault();
                    $(this).parent('div').remove();
                    x--;
                })
            });


        $(document).ready(function () {
            var max_fields = 100; //maximum input boxes allowed
            var wrapper4 = $(".input_fields_lobby"); //Fields wrapper
            var add_button_lobby = $(".add_field_lobby"); //Add button ID
            var x = 1; //initlal text box count
            $(add_button_lobby).click(function (e) { //on add input button click
                e.preventDefault();

                var r = confirm("Press a button!\nEither OK or Cancel.");
                if (r == true) {
                    if (x < max_fields) { //max input box allowed
                        x++; //text box increment
                        $(wrapper4).append('<div class="row"> <p style="text-align: center; font-size: 25px;">Lobby</p> <div class="form-group"> <label class="col-md-3 control-label">Designation</label> <div class="col-md-6"> <input name="lobby[designation][]" value="" type="text" class="form-control" placeholder="Enter Designation"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!} </div> </div> <div class="form-group"> <label class="col-md-3 control-label">Name</label> <div class="col-md-6"> <input name="lobby[name][]" value="" type="text" class="form-control" placeholder="Enter Name"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!} </div> </div> <div class="form-group"> <label class="col-md-3 control-label">Phone</label> <div class="col-md-6"> <input name="lobby[phone][]" value="" type="text" class="form-control" placeholder="Enter Phone Number"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!} </div> </div> <div class="form-group"> <label class="col-md-3 control-label">Email</label> <div class="col-md-6"> <input name="lobby[email][]" value="" type="text" class="form-control" placeholder="Enter Email"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!} </div> </div> <div style="cursor:pointer;background-color:red;  margin-left: 50%;" class="remove_field4 btn btn-info"><i class="fa fa-close"></i></div></div>'); //add input box
                    }

                } else {
                    return false;
                }


            });
            $(wrapper4).on("click", ".remove_field4", function (e) { //user click on remove text
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
            })
        });


        $(document).ready(function () {
            var max_fields = 100; //maximum input boxes allowed
            var wrapper5 = $(".input_fields_media"); //Fields wrapper
            var add_media = $(".add_field_media"); //Add button ID
            var x = 1; //initlal text box count
            $(add_media).click(function (e) { //on add input button click
                e.preventDefault();
                var r = confirm("Press a button!\nEither OK or Cancel.");
                if (r == true) {
                    if (x < max_fields) { //max input box allowed
                        x++; //text box increment
                        $(wrapper5).append('<div class="row"> <p style="text-align: center; font-size: 25px;">Media</p> <div class="form-group"> <label class="col-md-3 control-label">Name</label> <div class="col-md-6"> <input name="media[name][]" value="" type="text" class="form-control" placeholder="Enter Name"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!} </div> </div> <div class="form-group"> <label class="col-md-3 control-label">Phone</label> <div class="col-md-6"> <input name="media[phone][]" value="" type="text" class="form-control" placeholder="Enter Phone Number"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!} </div> </div> <div class="form-group"> <label class="col-md-3 control-label">Email</label> <div class="col-md-6"> <input name="media[email][]" value="" type="text" class="form-control" placeholder="Enter Email"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!} </div> </div> <div style="cursor:pointer;background-color:red;  margin-left: 50%;" class="remove_field5 btn btn-info"><i class="fa fa-close"></i></div></div>'); //add input box
                    }
                } else {
                    return false;
                }
            });
            $(wrapper5).on("click", ".remove_field5", function (e) { //user click on remove text
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
            })
        });


        function readURL(input) {
            if (input.files && input.files[0]) {

                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.image-upload-wrap').hide();

                    $('.file-upload-image').attr('src', e.target.result);
                    $('.file-upload-content').show();

                    $('.image-title').html(input.files[0].name);
                };

                reader.readAsDataURL(input.files[0]);

            } else {
                removeUpload();
            }
        }

        function removeUpload() {
            $('.file-upload-input').replaceWith($('.file-upload-input').clone());
            $('.file-upload-content').hide();
            $('.image-upload-wrap').show();
        }

        $('.image-upload-wrap').bind('dragover', function () {
            $('.image-upload-wrap').addClass('image-dropping');
        });
        $('.image-upload-wrap').bind('dragleave', function () {
            $('.image-upload-wrap').removeClass('image-dropping');
        });

        $(function () {
            // Remove button click
            $(document).on(
                'click',
                '[data-role="dynamic-fields"] > .form-inline [data-role="remove"]',
                function (e) {
                    e.preventDefault();
                    $(this).closest('.form-inline').remove();
                }
            );
            // Add button click
            $(document).on(
                'click',
                '[data-role="dynamic-fields"] > .form-inline [data-role="add"]',
                function (e) {
                    e.preventDefault();
                    var container = $(this).closest('[data-role="dynamic-fields"]');
                    new_field_group = container.children().filter('.form-inline:first-child').clone();
                    new_field_group.find('input').each(function () {
                        $(this).val('name="assign_staffs[]"');
                    });
                    container.append(new_field_group);

                }
            );
        });

        $(function () {
            $('.date-picker_1').datepicker(
                {
                    dateFormat: "mm/yy",
                    changeMonth: true,
                    changeYear: true,
                    yearRange: "-100:+100",
                    showButtonPanel: true,
                    onClose: function (dateText, inst) {


                        function isDonePressed() {
                            return ($('#ui-datepicker-div').html().indexOf('ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all ui-state-hover') > -1);
                        }

                        if (isDonePressed()) {
                            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                            $(this).datepicker('setDate', new Date(year, month, 1)).trigger('change');

                            $('.date-picker').focusout()//Added to remove focus from datepicker input box on selecting date
                        }
                    },
                    beforeShow: function (input, inst) {

                        inst.dpDiv.addClass('month_year_datepicker')

                        if ((datestr = $(this).val()).length > 0) {
                            year = datestr.substring(datestr.length - 4, datestr.length);
                            month = datestr.substring(0, 2);
                            $(this).datepicker('option', 'defaultDate', new Date(year, month - 1, 1));
                            $(this).datepicker('setDate', new Date(year, month - 1, 1));
                            $(".ui-datepicker-calendar").hide();
                        }
                    }
                })
        });

    </script>

@endsection


@section('css')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <style>

        .ui-datepicker-calendar {
            display: none;
        }

        hr {
            border-top: 1px solid black;
        }

        .separator {
            display: flex;
            align-items: center;
            text-align: center;
            padding-bottom: 20px;

        }

        .separator::before,
        .separator::after {
            content: '';
            flex: 1;
            border-bottom: 1px solid #000;

        }

        .separator::before {
            margin-right: .25em;

        }

        .separator::after {
            margin-left: .25em;
        }

        #ssd {
            resize: none;

        }

        .file-upload {
            background-color: #ffffff;
            width: 600px;
            margin: 0 auto;
            padding: 20px;
        }

        .file-upload-btn {
            width: 100%;
            margin: 0;
            color: #fff;
            background: #1FB264;
            border: none;
            padding: 10px;
            border-radius: 4px;
            border-bottom: 4px solid #15824B;
            transition: all .2s ease;
            outline: none;
            text-transform: uppercase;
            font-weight: 700;
        }

        .file-upload-btn:hover {
            background: #1AA059;
            color: #ffffff;
            transition: all .2s ease;
            cursor: pointer;
        }

        .file-upload-btn:active {
            border: 0;
            transition: all .2s ease;
        }

        .file-upload-content {
            display: none;
            text-align: center;
        }

        .file-upload-input {
            position: absolute;
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
            outline: none;
            opacity: 0;
            cursor: pointer;
        }

        .image-upload-wrap {
            margin-top: 20px;
            border: 4px dashed #1FB264;
            position: relative;
        }

        .image-dropping,
        .image-upload-wrap:hover {
            background-color: #1FB264;
            border: 4px dashed #ffffff;
        }

        .image-title-wrap {
            padding: 0 15px 15px 15px;
            color: #222;
        }

        .drag-text {
            text-align: center;
        }

        .drag-text h3 {
            font-weight: 100;
            text-transform: uppercase;
            color: #15824B;
            padding: 60px 0;
        }

        .file-upload-image {
            max-height: 200px;
            max-width: 200px;
            margin: auto;
            padding: 20px;
        }

        .remove-image {
            width: 200px;
            margin: 0;
            color: #fff;
            background: #cd4535;
            border: none;
            padding: 10px;
            border-radius: 4px;
            border-bottom: 4px solid #b02818;
            transition: all .2s ease;
            outline: none;
            text-transform: uppercase;
            font-weight: 700;
        }

        .remove-image:hover {
            background: #c13b2a;
            color: #ffffff;
            transition: all .2s ease;
            cursor: pointer;
        }

        .remove-image:active {
            border: 0;
            transition: all .2s ease;
        }

        [data-role="dynamic-fields"] > .form-inline + .form-inline {
            margin-top: 0.5em;
        }

        [data-role="dynamic-fields"] > .form-inline [data-role="add"] {
            display: none;
        }

        [data-role="dynamic-fields"] > .form-inline:last-child [data-role="add"] {
            display: inline-block;
        }

        [data-role="dynamic-fields"] > .form-inline:last-child [data-role="remove"] {
            display: none;
        }

    </style>

@endsection




@section('content')
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->


    @include('pages.include.beginPageHeader')


    <!-- END PAGE HEADER-->


        <div class="row">

            <div class="col-md-12">
                <div class="tabbable-line boxless tabbable-reversed">

                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_0">

                            {{-- <div class="portlet box blue-hoki">--}}
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-list"></i>Project Edit Form
                                    </div>
                                    
                                    <div class="pull-right">
                                        @can('project-report')
                                <a style="background-color: transparent; border: none; margin-top: 5px;" class="btn btn-primary" href="{{route('showProject',$projectData->id)}}"><i class="fa fa-info-circle"></i> Details</a>@endcan
                                        @can('project-list')
                                <a style="background-color: transparent; border: none; margin-top: 5px;" class="btn btn-primary" href="{{route('project')}}"><i class="fa fa-list-alt"></i> Project List</a>@endcan

                                </div>
                                
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->

                                    <form action="{{ route('updateProject', $projectData->id) }}" method="post"
                                          class="form-horizontal"
                                          enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Assignment/Project Name</label>
                                                <div class="col-md-6">

                                                    <textarea class="form-control" name="project_name" id="ssd" rows="3"
                                                              placeholder="Enter Assignment/Project Name">{{$projectData->project_name}}</textarea>

                                                    {!! $errors->first('project_name', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Country</label>
                                                <div class="col-md-6">
                                                    <select id="country" name="country" class="form-control">

                                                     @foreach($contryList as $item)

                                                            @php
                                                                if($projectData->country == $item->name)
                                                                {
                                                                $selected = 'selected';
                                                                }
                                                                else
                                                                {
                                                                $selected = '';
                                                                }
                                                                echo '<option value="' . $item->name . '"' . $selected . '>' . ' ' .
                                                                    $item->name . '</option>';

                                                            @endphp

                                                        @endforeach

                                                    </select>
                                                    {!! $errors->first('country', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Location within Country</label>
                                                <div class="col-md-6">
                                                    <input name="in_location" value="{{$projectData->in_location}}"
                                                           type="text" class="form-control"
                                                           placeholder="Assignment Location within Country">


                                                    {!! $errors->first('in_location', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="separator">Client Information</div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Client Name</label>
                                                <div class="col-md-6">
                                                    <select id="single" name="clients_id" class="form-control select2">
                                                        <option value=" ">Select Clients</option>

                                                        @foreach($clients as $item)
                                                            <option value="{{$item->id}}" {{ $item->id == $project_involve_clients_info->clients_id ? 'selected' : '' }} >{{$item->client_name}}</option>

                                                        @endforeach

                                                    </select>
                                                    {!! $errors->first('client_name', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Consulting Fee</label>
                                                <div class="col-md-6">
                                                    <input name="consulting_fee"
                                                           value="{{$project_involve_clients_info->consulting_fee}}"
                                                           type="text"
                                                           class="form-control"
                                                           placeholder="Input Consulting Fee">


                                                    {!! $errors->first('consulting_fee', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Lobbing Expenses</label>
                                                <div class="col-md-6">
                                                    <input name="lobbing_expenses"
                                                           value="{{$project_involve_clients_info->lobbing_expenses}}"
                                                           type="text"
                                                           class="form-control"
                                                           placeholder="Input Lobbing Expenses">


                                                    {!! $errors->first('lobbing_expenses', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="input_fields_director">
                                                <div class="row">
                                                    {{-- <p style="text-align: center; font-size: 25px;">Project Director</p>--}}
                                                    {{--    <div class="form-group">
                                                            <label class="col-md-3 control-label">Designation</label>
                                                            <div class="col-md-6">
                                                                <input name="project_director[designation][]" value="" name
                                                                       type="text" class="form-control"
                                                                       placeholder="Enter Designation">


                                                                {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Name</label>
                                                            <div class="col-md-6">
                                                                <input name="project_director[name][]" value="" type="text"
                                                                       class="form-control" placeholder="Enter Name">


                                                                {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Phone</label>
                                                            <div class="col-md-6">
                                                                <input name="project_director[phone][]" value="" type="text"
                                                                       class="form-control" placeholder="Enter phone Number">


                                                                {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Email</label>
                                                            <div class="col-md-6">
                                                                <input name="project_director[email][]" value="" type="text"
                                                                       class="form-control" placeholder="Enter Email">


                                                                {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                                            </div>
                                                        </div>
    --}}
                                                    <br>
                                                    <center>
                                                        <button style="background-color:green; margin-bottom: 15px;"
                                                                class="project_director btn btn-info active">Add More
                                                            Project Director
                                                        </button>
                                                    </center>

                                                </div>


                                                @php
                                                    $contact_person = json_decode($project_involve_clients_info->project_director, true);


                                                      $project_director_size = count($contact_person['designation'], COUNT_RECURSIVE);
                                                @endphp
                                                @for ($i = 0; $i < $project_director_size; $i++)
                                                    <div class="row">
                                                        <p style="text-align: center; font-size: 25px;">Project
                                                            Director</p>
                                                        <div class="form-group"><label class="col-md-3 control-label">Designation</label>
                                                            <div class="col-md-6">
                                                                <input name="project_director[designation][]"
                                                                       value="{{$contact_person['designation'][$i]}}"
                                                                       type="text" class="form-control"
                                                                       placeholder="Enter Designation"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                                            </div>
                                                        </div>
                                                        <div class="form-group"><label
                                                                    class="col-md-3 control-label">Name</label>
                                                            <div class="col-md-6"><input name="project_director[name][]"
                                                                                         value="{{$contact_person['name'][$i]}}"
                                                                                         type="text"
                                                                                         class="form-control"
                                                                                         placeholder="Enter Name"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                                            </div>
                                                        </div>
                                                        <div class="form-group"><label
                                                                    class="col-md-3 control-label">Phone</label>
                                                            <div class="col-md-6"><input
                                                                        name="project_director[phone][]"
                                                                        value="{{$contact_person['phone'][$i]}}"
                                                                        type="text" class="form-control"
                                                                        placeholder="Enter Phone Number"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                                            </div>
                                                        </div>
                                                        <div class="form-group"><label
                                                                    class="col-md-3 control-label">Email</label>
                                                            <div class="col-md-6"><input
                                                                        name="project_director[email][]"
                                                                        value="{{$contact_person['email'][$i]}}"
                                                                        type="text" class="form-control"
                                                                        placeholder="Enter Email"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                                            </div>
                                                        </div>
                                                        <div style="cursor:pointer;background-color:red;  margin-left: 50%; margin-bottom:10px;"
                                                             class="remove_field3 btn btn-info"><i
                                                                    class="fa fa-close"></i></div>
                                                    </div>

                                                @endfor


                                            </div>

                                            <div class="input_fields_department">


                                                <br>
                                                <center>
                                                    <button style="background-color:green; margin-bottom: 15px;"
                                                            class="head_of_the_department btn btn-info active">Add More Head
                                                        of the Department
                                                        
                                                    </button>
                                                </center>

                                                @php
                                                    $head_of_the_department = json_decode($project_involve_clients_info->head_of_the_department, true);


                                                      $head_of_the_department_size = count($head_of_the_department['designation'], COUNT_RECURSIVE);
                                                @endphp
                                                @for ($i = 0; $i < $head_of_the_department_size; $i++)

                                                    <div class="row">
                                                        <p style="text-align: center; font-size: 25px;">Head of the
                                                            Department</p>
                                                        <div class="form-group">

                                                            <label class="col-md-3 control-label">Designation</label>
                                                            <div class="col-md-6">
                                                                <input name="head_of_the_department[designation][]"
                                                                       value="{{$head_of_the_department['designation'][$i]}}"
                                                                       type="text" class="form-control"
                                                                       placeholder="Enter Designation">
                                                                {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Name</label>
                                                            <div class="col-md-6"><input
                                                                        name="head_of_the_department[name][]"
                                                                        value="{{$head_of_the_department['name'][$i]}}"
                                                                        type="text" class="form-control"
                                                                        placeholder="Enter Name">

                                                                {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                                            </div>
                                                        </div>
                                                        <div class="form-group"><label
                                                                    class="col-md-3 control-label">Phone</label>
                                                            <div class="col-md-6"><input
                                                                        name="head_of_the_department[phone][]"
                                                                        value="{{$head_of_the_department['phone'][$i]}}"
                                                                        type="text" class="form-control"
                                                                        placeholder="Enter Phone Number"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                                            </div>
                                                        </div>
                                                        <div class="form-group"><label
                                                                    class="col-md-3 control-label">Email</label>
                                                            <div class="col-md-6"><input
                                                                        name="head_of_the_department[email][]"
                                                                        value="{{$head_of_the_department['email'][$i]}}"
                                                                        type="text" class="form-control"
                                                                        placeholder="Enter Email"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                                            </div>
                                                        </div>
                                                        <div style="cursor:pointer;background-color:red;  margin-left: 50%; margin-bottom:10px;"
                                                             class="remove_field2 btn btn-info"><i
                                                                    class="fa fa-close"></i>
                                                        </div>
                                                    </div>
                                                @endfor

                                            </div>

                                            <div class="input_fields_lobby">

                                                <br>
                                                <center>
                                                    <button style="background-color:green; margin-bottom: 15px;"
                                                            class="add_field_lobby btn btn-info active">Add More Lobby
                                                    </button>
                                                </center>

                                                @php
                                                    $lobby = json_decode($project_involve_clients_info->lobby, true);


                                                      $lobby_size = count($lobby['designation'], COUNT_RECURSIVE);
                                                @endphp
                                                @for ($i = 0; $i < $lobby_size; $i++)

                                                    <div class="row">
                                                        <p style="text-align: center; font-size: 25px;">Lobby</p>
                                                        <div class="form-group"><label class="col-md-3 control-label">Designation</label>
                                                            <div class="col-md-6">
                                                                <input name="lobby[designation][]"
                                                                       value="{{$lobby['designation'][$i]}}"
                                                                       type="text"
                                                                       class="form-control"
                                                                       placeholder="Enter Designation"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                                            </div>
                                                        </div>
                                                        <div class="form-group"><label
                                                                    class="col-md-3 control-label">Name</label>
                                                            <div class="col-md-6"><input name="lobby[name][]"
                                                                                         value="{{$lobby['name'][$i]}}"
                                                                                         type="text"
                                                                                         class="form-control"
                                                                                         placeholder="Enter Name"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                                            </div>
                                                        </div>
                                                        <div class="form-group"><label
                                                                    class="col-md-3 control-label">Phone</label>
                                                            <div class="col-md-6"><input name="lobby[phone][]"
                                                                                         value="{{$lobby['phone'][$i]}}"
                                                                                         type="text"
                                                                                         class="form-control"
                                                                                         placeholder="Enter Phone Number"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                                            </div>
                                                        </div>
                                                        <div class="form-group"><label
                                                                    class="col-md-3 control-label">Email</label>
                                                            <div class="col-md-6"><input name="lobby[email][]"
                                                                                         value="{{$lobby['email'][$i]}}"
                                                                                         type="text"
                                                                                         class="form-control"
                                                                                         placeholder="Enter Email"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                                            </div>
                                                        </div>
                                                        <div style="cursor:pointer;background-color:red;  margin-left: 50%;"
                                                             class="remove_field4 btn btn-info"><i
                                                                    class="fa fa-close"></i>
                                                        </div>
                                                    </div>
                                                @endfor


                                            </div>

                                            <div class="input_fields_media">

                                                <br>
                                                <br>
                                                <center>
                                                    <button style="background-color:green; margin-bottom: 15px;"
                                                            class="add_field_media btn btn-info active">Add More Media
                                                    </button>
                                                </center>

                                                @php
                                                    $media = json_decode($project_involve_clients_info->media, true);




                                                      $media_size = count($media['name'], COUNT_RECURSIVE);
                                                @endphp
                                                @for ($i = 0; $i < $media_size; $i++)

                                                    <div class="row"><p style="text-align: center; font-size: 25px;">
                                                            Media</p>
                                                        <div class="form-group"><label
                                                                    class="col-md-3 control-label">Name</label>
                                                            <div class="col-md-6"><input name="media[name][]"
                                                                                         value="{{$media['name'][$i]}}"
                                                                                         type="text"
                                                                                         class="form-control"
                                                                                         placeholder="Enter Designation"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                                            </div>
                                                        </div>
                                                        <div class="form-group"><label
                                                                    class="col-md-3 control-label">Phone</label>
                                                            <div class="col-md-6"><input name="media[phone][]"
                                                                                         value="{{$media['phone'][$i]}}"
                                                                                         type="text"
                                                                                         class="form-control"
                                                                                         placeholder="Enter Phone Number"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                                            </div>
                                                        </div>
                                                        <div class="form-group"><label
                                                                    class="col-md-3 control-label">Email</label>
                                                            <div class="col-md-6"><input name="media[email][]"
                                                                                         value="{{$media['email'][$i]}}"
                                                                                         type="text"
                                                                                         class="form-control"
                                                                                         placeholder="Enter Email"> {!! $errors->first('', '<small class="text-danger">:message</small>') !!}
                                                            </div>
                                                        </div>
                                                        <div style="cursor:pointer;background-color:red;  margin-left: 50%;"
                                                             class="remove_field5 btn btn-info"><i
                                                                    class="fa fa-close"></i>
                                                        </div>
                                                    </div>

                                                @endfor

                                            </div>

                                            <hr>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">No of Staff-Months</label>
                                                <div class="col-md-6">
                                                    <input name="staff_months" value="{{$projectData->staff_months}}"
                                                           type="text" class="form-control"
                                                           placeholder="No of Staff-Months">


                                                    {!! $errors->first('staff_months', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Name of Partner,
                                                    if any</label>
                                                <div class="col-md-6">


                                                    <div class="input-group select2-bootstrap-append">
                                                        <select name="jv_consultant[]" id="multi-append"
                                                                class="form-control select2" multiple>
                                                            <option></option>


                                                            @foreach($partners as $item)

                                                                <option value="{{$item->id}}" {{in_array($item->id, explode(',', $projectData->jv_consultant)) ? 'selected':''}}>{{$item->partner_company_name}}</option>
                                                                {{--<option value="{{$item->id}}">{{$item->partner_company_name}}</option>--}}

                                                            @endforeach
                                                        </select>

                                                        <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button"
                                                            data-select2-open="multi-append">
                                                        <i class="fas fa-arrow-down"></i>
                                                    </button>
                                                </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Lead Partner</label>
                                                <div class="col-md-6">
                                                    <select id="single" name="jv_lead_consultant"
                                                            class="form-control select2">
                                                        <option>Select Lead Partner</option>

                                                        @foreach($partners as $item)
                                                            <option value="{{$item->id}}" {{ $item->id == $projectData->jv_lead_consultant ? 'selected' : '' }} >{{$item->partner_company_name}}</option>

                                                        @endforeach

                                                    </select>
                                                    {!! $errors->first('jv_lead_consultant', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">No of Staff-Months of Joint
                                                    Venture Consultant</label>
                                                <div class="col-md-6">
                                                    <input name="jv_staff_months"
                                                           value="{{$projectData->jv_staff_months}}" type="text"
                                                           class="form-control"
                                                           placeholder="No of Staff-Months of Professional Staff Provided by Joint Venture Consultant">


                                                    {!! $errors->first('jv_staff_months', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Narrative Description of
                                                    Project </label>


                                                <div class="col-md-6">
                                                    <textarea name="project_description"
                                                              placeholder="Detailed Description of the Project"
                                                              id="summary-ckeditor"
                                                              rows="3">{{$projectData->project_description}}</textarea>


                                                    {!! $errors->first('project_description', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Project Cost in (Tk.)</label>
                                                <div class="col-md-6">
                                                    <input name="project_cost" value="{{$projectData->project_cost}}"
                                                           type="text" class="form-control"
                                                           placeholder="Input Project Cost">


                                                    {!! $errors->first('project_cost', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="col-md-3 control-label">
                                                    Start Date</label>
                                                <div class="col-md-6">
                                                    <input autocomplete="off" name="start_date"
                                                           value="{{ date('m/Y', strtotime($projectData->start_date)) }}"
                                                           type="text" class="date-picker_1" id=""/>


                                                    {!! $errors->first('start_date', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">
                                                    Completion Date</label>
                                                <div class="col-md-6">
                                                    <input autocomplete="off" name="end_date"
                                                           value="{{ date('m/Y', strtotime($projectData->end_date)) }}"
                                                           type="text" class="date-picker_1" id=""/>


                                                    {!! $errors->first('start_date', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Description of Actual Services by
                                                    Staff</label>
                                                <div class="col-md-6">

                                                    <textarea class="form-control" name="service_render" id="ssd"
                                                              rows="3"
                                                              placeholder="Detailed Description of Actual Services Provided by our staff">{{$projectData->service_render}}</textarea>

                                                    {!! $errors->first('service_render', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="input_fields_wrapp">
                                                <div class="fild">

                                                    <div class="form-group">

                                                        <label class="col-md-3 control-label">Project Tag</label>
                                                        <div class="col-md-6" id="field">

                                                            <input name="project_tag[]"

                                                                   value="{{$project_tag[0]}}" type="text"
                                                                   class="form-control" placeholder="Project Tag">


                                                            {!! $errors->first('project_tag', '<small class="text-danger">:message</small>') !!}
                                                        </div>
                                                        <div class="col-md-3">
                                                            <button style="background-color: green; color: white;"
                                                                    id="b1" class="btn add-more" type="button">+
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Remarks</label>
                                                <div class="col-md-6">
                                                    <input name="remarks" value="{{$projectData->remarks}}" type="text"
                                                           class="form-control"
                                                           placeholder="Enter Remarks">


                                                    {!! $errors->first('remarks', '<small class="text-danger">:message</small>') !!}
                                                </div>
                                            </div>

                                            <div class="separator">Assigning Professional Staff</div>

                                            <div class="input_fields_wrap">


                                                <center>
                                                    <button style="background-color:green;"
                                                            class="add_field_button btn btn-info active">Add More
                                                        Staff
                                                    </button>
                                                </center>

                                                @foreach($project_involve_staff_info as $item)
                                                    <div class="row"><label class="control-label col-md-3"></label>
                                                        <div class="form-group">
                                                            <div class="col-md-6"><label for="">Staff Name</label>
                                                                <select
                                                                        id="single" name="staff_id[]"
                                                                        class="form-control select2">
                                                                    <option value=" ">Select Staff
                                                                    </option> @foreach($staff as $vale)


                                                                        <option value="{{$vale->id}}" {{ $vale->id == $item->staff_id ? 'selected' : '' }} >{{$vale->staff_name}} --- {{$vale->position}}</option>
                                                                    @endforeach
                                                                </select></div>
                                                        </div>
                                                        <label class="control-label col-md-3"></label>
                                                        <div class="form-group">
                                                            <div class="col-md-6"><label for="">Position</label> <input
                                                                        name="assign_position[]"
                                                                        value="{{$item->assign_position}}" type="text"
                                                                        class="form-control"
                                                                        placeholder="Input Position for this Project">
                                                            </div>
                                                        </div>
                                                        <label class="control-label col-md-3"></label>
                                                        <div class="form-group">
                                                            <div class="col-md-6"><label for="">Duties</label> <textarea
                                                                        class="form-control" name="duties[]" id="ssd"
                                                                        rows="3" placeholder="Enter Duty of the Staff"
                                                                        >{{$item->duties}}</textarea></div>
                                                        </div>
                                                        <div style="cursor:pointer;background-color:red;  margin-left: 50%;"
                                                             class="remove_field btn btn-info"><i
                                                                    class="fa fa-close"></i>
                                                        </div>
                                                    </div>

                                                @endforeach

                                            </div>

                                            <hr>


                                            <label class="control-label col-md-3"></label>
                                            <div class="form-group">
                                                <div class="col-md-6">
                                                    <label for="">Select Single or Multi File</label>

                                                    <input type="file" multiple name="project_file[]">

                                                </div>
                                            </div>
                                            <hr>
                                            <label class="control-label col-md-3"></label>
                                            <div class="form-group">
                                                <div class="col-md-6">

                                                    <ul>
                                                        @foreach($project_files_info as $item)
                                                            @php
                                                                $path_parts = pathinfo($item->path);
                                                            @endphp

                                                            {{--   <a href="{{route('ProjectFileDelete',['projectId'=>$item->projects_id,'fileId'=>$item->id])}}" title="Edit Category">
                                                                   <button class="btn btn-primary btn-sm">
                                                                       <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button>
                                                               </a>--}}



                                                            <li>
                                                                {{substr($path_parts['basename'], 11) }} &nbsp;
                                                                <a target="_blank" onclick="return confirm('Are You Sure To Delete This File?')" href="{{route('ProjectFileDelete',['projectId'=>$item->projects_id,'fileId'=>$item->id])}}"
                                                                        title="Delete File">

                                                                        <i style="color: red" class="fa fa-trash " aria-hidden="true">Delete</i>

                                                                </a>
                                                               <hr>
                                                            </li>




                                                            {{--  <a style="font-size: 20px; width: 30%;"
                                                                 href="{{route('ProjectFileDelete',['projectId'=>$item->projects_id,'fileId'=>$item->id])}}">
                                                                  <i class="fa fa-file-image-o"
                                                                     aria-hidden="true"></i>{{substr($path_parts['basename'], 11) }}
                                                              </a> &nbsp;

                                                              <hr>--}}
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>


                                            {{--  @foreach($project_files_info as $item)
                                                  <a href="">
                                                      <img height="80" width="80" src="{{$item->path}}">
                                                  </a>
                                              @endforeach--}}

                                            <div class="form-actions top">
                                                <div class="row">
                                                    <center>
                                                        <button type="submit" class="btn green">Submit</button>
                                                    </center>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    </div>
    <!-- END CONTENT BODY -->
@endsection