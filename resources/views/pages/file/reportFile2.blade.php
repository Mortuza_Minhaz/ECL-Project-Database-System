<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>

<head>
    <title>client</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        /*  
    Side Navigation Menu V2, RWD
    ===================
    Author: https://github.com/pablorgarcia
 */

@charset "UTF-8";
@import url(https://fonts.googleapis.com/css?family=Open+Sans:300,400,700);

body {
  font-family: 'Open Sans', sans-serif;
  font-weight: 300;
  line-height: 1.42em;
  color:#A7A1AE;
  background-color:#1F2739;
}

h1 {
  font-size:3em; 
  font-weight: 300;
  line-height:1em;
  text-align: center;
  color: #4DC3FA;
}

h2 {
  font-size:1em; 
  font-weight: 300;
  text-align: center;
  display: block;
  line-height:1em;
  padding-bottom: 2em;
  color: #FB667A;
}

h2 a {
  font-weight: 700;
  text-transform: uppercase;
  color: #FB667A;
  text-decoration: none;
}

.blue { color: #185875; }
.yellow { color: #FFF842; }

.container th h1 {
      font-weight: bold;
      font-size: 1em;
  text-align: left;
  color: #185875;
}

.container td {
      font-weight: normal;
      font-size: 1em;
  -webkit-box-shadow: 0 2px 2px -2px #0E1119;
       -moz-box-shadow: 0 2px 2px -2px #0E1119;
            box-shadow: 0 2px 2px -2px #0E1119;
}

.container {
      text-align: left;
      overflow: hidden;
      width: 80%;
      margin: 0 auto;
  display: table;
  padding: 0 0 8em 0;
}

.container td, .container th {
      padding-bottom: 2%;
      padding-top: 2%;
  padding-left:2%;  
}

/* Background-color of the odd rows */
.container tr:nth-child(odd) {
      background-color: #323C50;
}

/* Background-color of the even rows */
.container tr:nth-child(even) {
      background-color: #2C3446;
}

.container th {
      background-color: #1F2739;
}

.container td:first-child { color: #FB667A; }

.container tr:hover {
   background-color: #464A52;
-webkit-box-shadow: 0 6px 6px -6px #0E1119;
       -moz-box-shadow: 0 6px 6px -6px #0E1119;
            box-shadow: 0 6px 6px -6px #0E1119;
}

.container td:hover {
  background-color: #FFF842;
  color: #403E10;
  font-weight: bold;
  
  box-shadow: #7F7C21 -1px 1px, #7F7C21 -2px 2px, #7F7C21 -3px 3px, #7F7C21 -4px 4px, #7F7C21 -5px 5px, #7F7C21 -6px 6px;
  transform: translate3d(6px, -6px, 0);
  
  transition-delay: 0s;
      transition-duration: 0.4s;
      transition-property: all;
  transition-timing-function: line;
}

@media (max-width: 800px) {
.container td:nth-child(4),
.container th:nth-child(4) { display: none; }
}
    </style>

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><span class="blue"></span>File List<span class="blue"></span> <span class="yellow">..</pan></h1>
<h2>.. <a href="https://github.com/pablorgarcia" target="_blank">..</a></h2>

<table class="container">
    <thead>
        <tr>
            <th style="text-align: center;"> File name</th>
                                <th> File Description</th>
                                <th> Start Date</th>
                                <th> Expiration Date</th>
                                <th> Document</th>
                                <th> Action</th>
        </tr>
    </thead>
    <tbody>
    <tr>

                                <td style="width: 5%;">{{$fileData->file_name}}</td>
                                <td>{{$fileData->description}}</td>
                                <td style="width: 20%;">{{ date('F-Y', strtotime($fileData->start_date)) }}</td>
                                <td>{{ date('F-Y', strtotime($fileData->exp_date)) }}</td>

                                <td>
                                    @foreach($notification_multiple_files as $key => $item)

                                    @php
                                    $path_parts = pathinfo($item->path);
                                    @endphp

                                     <a style="font-size: 20px;" href="{{ ($item->path) }}" download="{{ substr($path_parts['basename'],11) }}">
                                        <i class="fa fa-trash" aria-hidden="true"> {{substr($path_parts['basename'], 11) }}</i></a> &nbsp;
                                    <br><br>
                             
                                    @endforeach

                                </td>

                                <td>

                                    <div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="col-md-10 justify-content-center">
               

                <div class="card" style="width: auto;">
                    <div class="card-header">
                  
                 
                  </div>
                  
                     <div id="accordion">
                      <div class="card">
                        <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapse" aria-expanded="true" aria-controls="collapse">
                          <h6 class="mb-0" style="cursor: pointer; color: #e67e22">
                            
                            file list
                            
                          </h6>
                        </div>

                        <div id="collapse" class="collapse hide" aria-labelledby="headingOne" data-parent="#collapse">
                          <div class="card-body">
                            <div class="company-reg">Registration No: </div> 
                           
                          </div>

                            
                        </div>
                      </div>
                    </div>                 
                  
                   
                </div>
 
               
            </div>
        </div>
    </div>


</div>


                                </td>
                            </tr>
    </tbody>
</table>
               

            </div>
        </div>
    </div>

    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
</body>

</html>