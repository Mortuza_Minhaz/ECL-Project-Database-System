@extends('layouts.app')

@section('title', 'File Edit Form')


@section('js')

<script>

</script>


@endsection


@section('css')

<style>


</style>

@endsection


@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('pages.include.beginPageHeader')


    <!-- END PAGE HEADER-->

    <div class="row">

        <div class="col-md-6 col-md-offset-4">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('updateFile',$file->id)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <div class="col-md-6">
                                <label for="">File Name</label>

                                <input name="file_name" value="{{$file->file_name}}" type="text" class="form-control" placeholder="Enter File Name">
                            </div>

                        </div>

                        <div class="form-group">
                            <div class="col-md-6">
                                <label for="">File Description</label>

                                <input name="description" value="{{$file->description}}" type="text" class="form-control" placeholder="Enter File Description">
                            </div>

                        </div>

                        <div class="form-group">

                            <div class="col-md-6">
                                <label for="">File Start Date</label>
                                <input type="text" class="date-picker form-control" id="" name="start_date" value="{{$file->start_date}}" data-date-format="yyyy-mm-dd" placeholder="Birth Date: yyyy-mm-dd">
                            </div>


                        </div>

                        <div class="form-group">

                            <div class="col-md-6">
                                <label for="">File Expiration Date</label>
                                <input type="text" class="date-picker form-control" id="" name="exp_date" value="{{$file->exp_date}}" data-date-format="yyyy-mm-dd" placeholder="Birth Date: yyyy-mm-dd">
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6">
                                <label for="">Select Singlr or Multiple File</label>
                                <input type="file" multiple name="multi_files[]">
                            </div>
                        </div>
                        <br><br><br><br><br><br><br><br><br>


                        <hr>
                                            <!-- <label class="control-label col-md-3"></label> -->
                                            <div class="form-group">
                                                <div class="col-md-6">

                                                    <ul>

                                                        @foreach($notification_multiple_files  as $item)
                                                            @php
                                                                $path_parts = pathinfo($item->path);
                                                            @endphp


                                                            <li>
                                                                {{substr($path_parts['basename'], 11) }} &nbsp;
                                                                <a target="_blank"
                                                                   onclick="return confirm('Are You Sure To Delete This Category?')"
                                                                   href="{{route('notificationFileDelete',['multifileId'=>$item->file_id,'fileId'=>$item->id])}}"
                                                                   title="Delete Faile">

                                                                    <i style="color: red" class="fa fa-trash "
                                                                       aria-hidden="true">Delete</i>

                                                                </a>
                                                                <hr>
                                                            </li>

                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>

                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Upload</button>
                            <a href="{{route('viewfile')}}" class="btn btn-success">Back</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

</div>

@endsection