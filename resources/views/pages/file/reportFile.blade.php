@extends('layouts.app')

@section('title', 'File List')


@section('js')

<script>
    $(document).ready(function() {
        $('#example').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'print',
                'pdf'
            ]
        });
    });
</script>


@endsection


@section('css')

<style>
    #GFG { 
            text-decoration: none; 
            color: black;
        }
    .portlet.box .dataTables_wrapper .dt-buttons {
        margin-top: 0px;
        margin-bottom: 20px;
    }

    .dataTables_wrapper .dt-buttons {
        float: left;
    }

    div.dataTables_wrapper div.dataTables_paginate {
        /* margin: 0; */
        white-space: nowrap;
        /* text-align: right; */
        float: right !important;
    }

    .input-group-sm>.input-group-btn>select.btn,
    .input-group-sm>select.form-control,
    .input-group-sm>select.input-group-addon,
    select.input-sm {
        height: 31px;
        line-height: 30px;
    }
</style>

@endsection


@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('pages.include.beginPageHeader')


    <!-- END PAGE HEADER-->

<!--     <div class="row">
        <div class="col-md-12">
            
            <div class="portlet box green">
                <div class="portlet-title">

                    <div class="dt-buttons" style="margin-top: 5px;">

                        <a style="color: black; border: none; background-color: #ecf0f1;" class="dt-button buttons-print btn default" tabindex="0" aria-controls="sample_2" href="{{route('createfile')}}"><span> <i class="fa fa-plus"></i>&nbsp; Add File</span>
                        </a>
                    </div>
                </div>

                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="example">
                        <thead>
                            <tr>

                                <th style="text-align: center;"> File name</th>
                                <th style="text-align: center;"> File Description</th>
                                <th style="text-align: center;"> Start Date</th>
                                <th style="text-align: center;"> Expiration Date</th>
                                <th style="text-align: center;"> Document</th>
                                <th style="text-align: center;"> Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            <tr>

                                <td style="width: 3%;">{{$fileData->file_name}}</td>
                                <td>{{$fileData->description}}</td>
                                <td style="width: 20%;">{{ date('F-Y', strtotime($fileData->start_date)) }}</td>
                                <td>{{ date('F-Y', strtotime($fileData->exp_date)) }}</td>

                                <td>
                                    @foreach($notification_multiple_files as $key => $item)

                                    @php
                                    $path_parts = pathinfo($item->path);
                                    @endphp

                                    <a style="font-size: 20px;" href="{{ ($item->path) }}" download="{{ substr($path_parts['basename'],11) }}">
                                        <i class="fa fa-trash" aria-hidden="true"> {{substr($path_parts['basename'], 11) }}</i></a> &nbsp;
                                    <br><br>

                                    @endforeach

                                </td>
                                <td style="width: 20%; text-align:center">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
           
        </div>
    </div> -->

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="dt-buttons" style="margin-top: 5px;">
                        @can('create-multiplefile')
                        <a style="color: black; border: none; background-color: #ecf0f1;" class="dt-button buttons-print btn default" tabindex="0" aria-controls="sample_2" href="{{route('createfile')}}"><span> <i class="fa fa-plus"></i>&nbsp; Add File</span>
                        </a>
                        @endcan
                        @can('multiplefile-list')                 
                        <a style="color: black; border: none; background-color: #ecf0f1;" class="dt-button buttons-print btn default" tabindex="0" aria-controls="sample_2" href="{{route('viewfile')}}"><span> <i class="fa fa-list"></i>&nbsp;File List</span>
                        </a>                   
                    @endcan
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_3" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="all" style="text-align: center;"> File name</th>
                                <th style="text-align: center;"> File Description</th>
                                <th style="text-align: center;"> Start Date</th>
                                <th style="text-align: center;"> Expiration Date</th>
                                <th class="none" style="text-align: center;"> Document</th>
                                <!-- <th style="text-align: center;"> Action</th> -->

                                
                            </tr>
                        </thead>
                        <tbody>

                            <tr>
                                <td>{{$fileData->file_name}}</td>
                                <td>{{$fileData->description}}</td>
                                <td>{{ date('F-Y', strtotime($fileData->start_date)) }}</td>
                                <td>{{ date('F-Y', strtotime($fileData->exp_date)) }}</td>
                                <td>
                                    @if($notification_multiple_files->count() > 0)

                                    @foreach($notification_multiple_files as $item)

                                    @php
                                    $path_parts = pathinfo($item->path); 
                                    $extention_name = pathinfo($item->path, PATHINFO_EXTENSION)
                                    @endphp
                                    <br><br>
                                    @if($extention_name == 'png' || $extention_name == 'jpg' || $extention_name == 'jpeg')
                                    <a id = "GFG" style="font-size: 20px; width: 30%;" href="{{ ($item->path) }}" download="{{ substr($path_parts['basename'],11) }}">
                                        <i style="color: #15aabf;" class="far fa-file-image fa-lg"></i> {{substr($path_parts['basename'], 11) }}</a> &nbsp;

                                    <br>
                                     @elseif($extention_name == 'docx' || $extention_name == 'doc')
                                     <a id = "GFG" style="font-size: 20px; width: 30%;" href="{{ ($item->path) }}" download="{{ substr($path_parts['basename'],11) }}">
                                        <i style="color: #2980b9;" class="fas fa-file-word fa-lg" aria-hidden="true"></i> {{substr($path_parts['basename'], 11) }}</a> &nbsp;
                                    <br>
                                    @elseif($extention_name == 'pdf')
                                    <a id = "GFG" style="font-size: 20px; width: 30%;" href="{{ ($item->path) }}" download="{{ substr($path_parts['basename'],11) }}">
                                        <i style="color: red" class="fa fa-file-pdf-o fa-lg" aria-hidden="true"></i> {{substr($path_parts['basename'], 11) }}</a> &nbsp;
                                    <br>
                                    @elseif($extention_name == 'zip' || $extention_name == 'rar')
                                    <a id = "GFG" style="font-size: 20px; width: 30%;" href="{{ ($item->path) }}" download="{{ substr($path_parts['basename'],11) }}">
                                        <i style="color: #e67e22;" class="fa fa-file-zip-o fa-lg" aria-hidden="true"></i> {{substr($path_parts['basename'], 11) }}</a> &nbsp;
                                    <br>
                                    @elseif($extention_name == 'mp4' || $extention_name == 'WEBM' || $extention_name == 'flv' || $extention_name == 'AVI' || $extention_name == 'WMV' || $extention_name == 'MPEG')
                                    <a id = "GFG" style="font-size: 20px; width: 30%;" href="{{ ($item->path) }}" download="{{ substr($path_parts['basename'],11) }}">
                                        <i style="color: #15aabf;" class="far fa-file-video fa-lg"></i> {{substr($path_parts['basename'], 11) }}</a> &nbsp;
                                    <br>
                                    @else
                                    <a id = "GFG" style="font-size: 20px; width: 30%;" href="{{ ($item->path) }}" download="{{ substr($path_parts['basename'],11) }}">
                                        <i style="color: #4c6ef5;" class="fas fa-file-alt fa-lg"></i> {{substr($path_parts['basename'], 11) }}</a> &nbsp;
                                    <br>

                                    @endif
                                    @endforeach
                                    @else
                                    There is no file
                                    @endif 

                                </td>
                                <!-- <td>...</td> -->

                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

</div>
<!-- END CONTENT BODY -->
@endsection

