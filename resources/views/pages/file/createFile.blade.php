@extends('layouts.app')

@section('title', 'Add File')


@section('js')

<script>

</script>


@endsection


@section('css')

<style>


</style>

@endsection


@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('pages.include.beginPageHeader')


    <!-- END PAGE HEADER-->

    <div class="row">

        <div class="col-md-6 col-md-offset-4">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('uploadfile')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <div class="col-md-6">
                                <label for="">File Name</label>

                                <input name="file_name" value="" type="text" class="form-control" placeholder="Enter File Name">
                            </div>

                        </div>

                        <div class="form-group">
                            <div class="col-md-6">
                                <label for="">File Description</label>

                                <input name="description" value="" type="text" class="form-control" placeholder="Enter File Description">
                            </div>

                        </div>

                        <div class="form-group">

                            <div class="col-md-6">
                                <label for="">File Start Date</label>
                                <input type="text" class="date-picker form-control" id="" name="start_date" value="<?php echo date('Y-m-d'); ?>" data-date-format="yyyy-mm-dd" placeholder="Birth Date: yyyy-mm-dd">
                            </div>


                        </div>

                        <div class="form-group">

                            <div class="col-md-6">
                                <label for="">File Expiration Date</label>
                                <input type="text" class="date-picker form-control" id="" name="exp_date" value="<?php echo date('Y-m-d'); ?>" data-date-format="yyyy-mm-dd" placeholder="Birth Date: yyyy-mm-dd">
                            </div>


                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <label for="">Select Singlr or Multiple File</label>
                                <input type="file" multiple name="multi_files[]">
                            </div>
                        </div>
                        <br><br><br><br><br><br><br><br><br>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Upload</button>
                            <a href="{{route('viewfile')}}" class="btn btn-success">Back</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

</div>

@endsection