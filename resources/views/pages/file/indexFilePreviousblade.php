@extends('layouts.app')

@section('title', 'File List')


@section('js')

<script>
    $(document).ready(function() {
        $('#example').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'print',
                'pdf'
            ]
        });
    });
</script>


@endsection


@section('css')

<style>
    .portlet.box .dataTables_wrapper .dt-buttons {
        margin-top: 0px;
        margin-bottom: 20px;
    }

    .dataTables_wrapper .dt-buttons {
        float: left;
    }

    div.dataTables_wrapper div.dataTables_paginate {
        /* margin: 0; */
        white-space: nowrap;
        /* text-align: right; */
        float: right !important;
    }

    .input-group-sm>.input-group-btn>select.btn,
    .input-group-sm>select.form-control,
    .input-group-sm>select.input-group-addon,
    select.input-sm {
        height: 31px;
        line-height: 30px;
    }
</style>

@endsection


@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('pages.include.beginPageHeader')


    <!-- END PAGE HEADER-->

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    @can('create-multiplefile')
                    <div class="dt-buttons" style="margin-top: 5px;">

                        <a style="color: black; border: none; background-color: #ecf0f1;" class="dt-button buttons-print btn default" tabindex="0" aria-controls="sample_2" href="{{route('createfile')}}"><span> <i class="fa fa-plus"></i>&nbsp; Add File</span>
                        </a>
                    </div>
                    @endcan
                </div>

                <div class="portlet-body">
                    <div class="table-responsive"> 
                    <table class="table table-striped table-bordered table-hover" id="">
                        <thead>
                            <tr>
                                <th style="text-align: center;"> Sl No.</th>
                                <th style="text-align: center;"> File name</th>
                                <th style="text-align: center;"> File Description</th>
                                <th style="text-align: center;"> Start Date</th>
                                <th style="text-align: center;"> Expiration Date</th>
                                <th style="text-align: center;"> Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($files as $item)
                            <tr>
                                <td style="width: 3%;">{{$loop->iteration}}</td>
                                <td style="width: 15%;">{{$item->file_name}}</td>
                                <td>{{$item->description}}</td>
                                <td style="width: 20%;">{{ date('d-M-Y', strtotime($item->start_date)) }}</td>
                                <td>{{ date('d-M-Y', strtotime($item->exp_date)) }}</td> 

                                <td style="width: 20%; text-align:center">
                                    
                                    @can('multiplefile-download')
                                    <a style="background-color: #2980b9; border: none;" class="btn btn-primary" href="{{route('showFile',$item->id)}}"><i style="color: white;" class="icon-docs"></i></a> 
                                    @endcan
                                    
                                    <a style="background-color: #1abc9c; border: none;" class="btn btn-primary" href="{{route('editFile',$item->id)}}"><i class="fa fa-edit"></i></a>
                                   
                                    <br><br>
                                    @can('multiplefile-delete')
                                    <a class="btn btn-danger" href="{{route('deleteFile',$item->id)}}" onclick="return confirm('Are You Sure?')"><i class="fa fa-trash"></i></a>
                                    @endcan
                                </td>
                            </tr>
                            @endforeach


                        </tbody>
                    </table>
                </div>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

</div>
<!-- END CONTENT BODY -->
@endsection