@extends('layouts.app')

@section('title', 'Client List')


@section('js')

<script>
    $(document).ready(function() {
        $('#example').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'print',
                'pdf'
            ]
        });
    });
</script>


@endsection



@section('css')

<style>
    .fa-file-pdf {
        color: red;
        background-color: white;
    }


    .portlet.box .dataTables_wrapper .dt-buttons {
        margin-top: 0px;
        margin-bottom: 20px;
    }

    .dataTables_wrapper .dt-buttons {
        float: left;
    }

    div.dataTables_wrapper div.dataTables_paginate {
        /* margin: 0; */
        white-space: nowrap;
        /* text-align: right; */
        float: right !important;
    }

    .input-group-sm>.input-group-btn>select.btn,
    .input-group-sm>select.form-control,
    .input-group-sm>select.input-group-addon,
    select.input-sm {
        height: 31px;
        line-height: 30px;
    }
</style>

@endsection


@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('pages.include.beginPageHeader')


    <!-- END PAGE HEADER-->

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    @can('create-client')
                    <div class="dt-buttons" style="margin-top: 5px;">
                        <a class="dt-button buttons-print btn default" tabindex="0" aria-controls="sample_2" href="{{route('addClient')}}"><span> <i class="fa fa-plus"></i>&nbsp; Add Client</span>

                        </a>
                    </div>
                    @endcan
                </div>


                <div class="portlet-body">
                    <div class="table-responsive"> 
                    <table class="table table-striped table-bordered table-hover" id="example">
                        <thead>
                            <tr>
                                <th style="text-align: center;"> Name of the client</th>
                                <th style="text-align: center;"> Client Adddress</th>
                                <th style="text-align: center;"> Concerning Ministry</th>
                                <!--   <th style="text-align: center;"> Project Name</th> -->
                                <th style="text-align: center;"> Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($clientList as $item)
                            <tr>

                                <td>{{$item->client_name}}</td>
                                <td>{{$item->client_address}}</td>
                                <td>{{$item->concerning_ministry}}</td>
                                <!--  <td>{!! Str::limit($item->project_name, 80, ' .....') !!}</td> -->

                                <td>
                                <div class="btn-group">
                                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-left" role="menu">
                                        <li>@can('client-report')
                                            <a style="border: none;" href="{{route('showClient',$item->id)}}">
                                                <i style="" class="fa fa-eye"></i> Details</a>@endcan
                                        </li>
                                        <li>
                                            @can('edit-client')
                                            <a style="border: none;" href="{{route('editClient',$item->id)}}"><i class="fa fa-edit"></i> Edit</a>@endcan
                                        </li>
                                        <li>
                                            @can('delete-client')
                                            <a href="{{route('deleteClient',$item->id)}}" onclick="return confirm('Are You Sure?')"><i class="fa fa-trash"></i> Delete</a>@endcan
                                        </li>

                                    </ul>
                                </div>
                            </td>

                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

</div>
<!-- END CONTENT BODY -->
@endsection