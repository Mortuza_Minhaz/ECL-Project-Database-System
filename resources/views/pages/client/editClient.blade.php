@extends('layouts.app')

@section('title', 'Client Edit Form')


@section('js')

<script>

</script>


@endsection


@section('css')

<style>
    #ssd {
        resize: none;
    }
</style>

@endsection


@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('pages.include.beginPageHeader')


    <!-- END PAGE HEADER-->

    <div class="row">

        <div class="col-md-12">
            <div class="tabbable-line boxless tabbable-reversed">


                <div class="tab-content">
                    <div class="tab-pane active" id="tab_0">

                        {{-- <div class="portlet box blue-hoki">--}}
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-list"></i>Client Edit Form
                                </div>
                                
                                <div class="pull-right">
                                    @can('client-report')
                                <a style="background-color: transparent; border: none; margin-top: 5px;" class="btn btn-primary" href="{{route('showClient',$client->id)}}"><i class="fa fa-info-circle"></i> Client Details</a>@endcan
                                @can('client-list')
                                <a style="background-color: transparent; border: none; margin-top: 5px;" class="btn btn-primary" href="{{route('client')}}"><i class="fa fa-list-alt"></i> Client List</a>@endcan
                                </div>

                            </div>
                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->

                                <form action="{{route('updateClient',$client->id)}}" method="post" class="form-horizontal" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-body">

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Name of the Client</label>
                                            <div class="col-md-6">
                                                <input name="client_name" value="{{$client->client_name}}" type="text" class="form-control" placeholder="Enter Client Name">


                                                {!! $errors->first('product_name', '<small class="text-danger">:message</small>') !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Client Address</label>
                                            <div class="col-md-6">

                                                <textarea class="form-control" name="client_address" id="ssd" rows="3" placeholder="Enter Client Address">{{$client->client_address}}</textarea>

                                                {!! $errors->first('product_name', '<small class="text-danger">:message</small>') !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Concerning Ministry<span class="required" aria-required="true">  </span></label>
                                            <div class="col-md-6">
                                                <input name="concerning_ministry" value="{{$client->concerning_ministry}}" type="text" class="form-control" placeholder="">


                                                {!! $errors->first('concerning_ministry', '<small class="text-danger">:message</small>') !!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-actions top">
                                        <div class="row">
                                            <div class="col-md-offset-4 col-md-8">
                                                <button type="submit" class="btn green">Submit</button>

                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <!-- END FORM-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>

@endsection