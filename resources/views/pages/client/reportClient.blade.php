<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>

<head>
    <title>client Details</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        tr.border_bottom td {
            border-bottom: 1px solid black !important;
        }
        @media print {
            .noprint {
                visibility: hidden;
            }
        }
        .center {
            margin-left: auto;
            margin-right: auto;
        }

        table,
        tr,
        td {
            border: 1px solid black;
            border-collapse: collapse;
            padding: 2px
        }

        p {
            margin: 5px 7px 7px 7px;
        }
    </style>

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-12">

                  <table class="center" style="width: 1050px; border:0">
                    <tbody>
                        <tr class="border_bottom noprint" style="border:0; ">
                            <td style="text-align:left; border:0">
                                <p style="left:0px">
                                    @can('client-list')
                                    <div>
                                            <form action="{{route('client')}}">
                                            <i style="color: #00aaaa;" class="fa fa-arrow-circle-left noprint"></i> <input style="color: #00aaaa;" class="noprint" type="submit" value="Client List">                                                           
                                        </form>

                                    </div>
                                    @endcan
                                </p>
                                <p style="left:0px"></p>
                                <p style="left:0px"></p>
                                <p style="left:0px"></p>

                            </td>

                            <td style="text-align:right; border:0;" class="border_bottom">
                                </p>
                                <p style="left:0px">
                                    <div>
                                        @can('edit-client')
                                         <form action="{{route('editClient',$clientData->id)}}">
                                            <input style="color: #00aaaa;" class="noprint" type="submit" value="Edit">@endcan
                                            <span>
                                                <form>
                                            <input style="color: #00aaaa;" class="noprint" type="button" value="Print" href="" onClick="window.print()">
                                        </form>
                                       
                                        </span>
                                        </form>
                                    </div>
                                </p>
                                <p style="left:0px"> </p>
                                <p style="left:0px"></p>

                            </td>
                        </tr>

                    </tbody>
                </table>


                <p style="text-align: center;  margin: 15px 15px;"><strong> Client Information</strong></p>
                <table class="center table" cellspacing="0" cellpadding="0" border="1" style="width: 1050px;">
                    <tbody>

                        <tr>
                            <td width="319">
                                <p>Name of the client:</p>

                            </td>
                            <td width="319">
                                <p>{{ $clientData->client_name}}</p>
                            </td>
                        </tr>
                        <tr>
                            <td width="319">
                                <p>Address:</p>

                            </td>
                            <td width="319">
                                <p>{{ $clientData->client_address}}</p>
                            </td>
                        </tr>
                        <tr>
                            <td width="319">
                                <p>Concerning Ministry:</p>

                            </td>
                            <td width="319">
                                <p>{{ $clientData->concerning_ministry}}</p>
                            </td>
                        </tr>
                    </tbody>
                </table>




                @foreach($clientProjectName as $key=> $item)
                @if($key==0)
                <p style="text-align: center; margin: 15px 15px;"><strong>Project Information Associated with {{$clientData->client_name}}</strong></p>
                @else
                <p style="text-align: center; margin: 15px 15px;"><strong>Project Information Associated with {{$clientData->client_name}} ({{$key+1}})</strong></p>
                @endif

                <table class="center table" cellspacing="0" cellpadding="0" border="1" style="width: 1050px;">
                    <tbody>
                        <tr>
                            <td width="319">
                                <p>Name of the Project</p>
                            </td>
                            <td width="319">
                                <p>{{ $item->project_name}}</p>


                            </td>
                        </tr>
                        <tr>
                            <td width="319">
                                <p>Site location</p>
                            </td>
                            <td width="319">
                                <p>{{ $item->in_location}}</p>
                            </td>
                        </tr>
                        <tr>
                            <td width="319">
                                <p>Description of assignment</p>
                            </td>
                            <td width="319">
                                <p>{!! $item->project_description !!} </p>
                            </td>
                        </tr>
                        <tr>
                            <td width="319">
                                <p>Duration</p>
                            </td>
                            <td width="319">
                                <p>{{ date('F-Y', strtotime($item->start_date)) }} to {{ date('F-Y', strtotime($item->end_date)) }}</p>
                            </td>
                        </tr>
                        <tr>
                            <td width="319">
                                <p>Consulting Fee</p>
                            </td>
                            <td width="319">
                                <p>{{ $item->consulting_fee}}</p>
                            </td>
                        </tr>
                        <tr>
                            <td width="319">
                                <p>Lobbying Expense</p>
                            </td>
                            <td width="319">
                                <p>{{ $item->lobbing_expenses}}</p>
                            </td>
                        </tr>

                        @php
                        $project_director = json_decode($item->project_director, true);

                        $project_director_size = count($project_director['designation'], COUNT_RECURSIVE);

                        @endphp
                        @for ($i = 0; $i < $project_director_size; $i++) <tr>
                            @if($i == 0)
                            <td width="319">
                                <center>
                                    <p style="color: #00aaaa">Project Director</p>
                                </center>

                            </td>
                            @else
                            <td width="319">
                                <center>
                                    <p style="color: #00aaaa">Project Director ({{$i+1}})</p>
                                </center>

                            </td>
                            @endif
                            <td width="319">

                                <p>Designation: {{$project_director['designation'][$i]}} </p>
                                <p>Name: {{$project_director['name'][$i]}}</p>
                                <p>Phone: {{$project_director['phone'][$i]}}</p>
                                <p>Email: {{$project_director['email'][$i]}}</p>
                            </td>
                            </tr>
                            @endfor

                            @php
                            $head_of_the_department = json_decode($item->head_of_the_department, true);
                            $head_of_the_department_size = count($head_of_the_department['designation'], COUNT_RECURSIVE);

                            @endphp
                            @for ($i = 0; $i < $head_of_the_department_size; $i++) <tr>
                                @if($i==0)
                                <td width="319">
                                    <center>
                                        <p style="color: burlywood">Head of the Department</p>
                                    </center>

                                </td>
                                @else
                                <td width="319">
                                    <center>
                                        <p style="color: burlywood">Head of the Department ({{$i+1}})</p>
                                    </center>

                                </td>
                                @endif
                                <td width="319">

                                    <p>Designation: {{$head_of_the_department['designation'][$i]}} </p>
                                    <p>Name: {{$head_of_the_department['name'][$i]}}</p>
                                    <p>Phone: {{$head_of_the_department['phone'][$i]}}</p>
                                    <p>Email: {{$head_of_the_department['email'][$i]}}</p>
                                </td>
                                </tr>
                                @endfor

                                @php
                                $lobby = json_decode($item->lobby, true);
                                $lobby_size = count($lobby['designation'], COUNT_RECURSIVE);

                                @endphp
                                @for ($i = 0; $i < $lobby_size; $i++) <tr>
                                    @if($i==0)
                                    <td width="319">
                                        <center>
                                            <p style="color: mediumseagreen">Lobby</p>
                                        </center>

                                    </td>
                                    @else
                                    <td width="319">
                                        <center>
                                            <p style="color: mediumseagreen">Lobby ({{$i+1}})</p>
                                        </center>

                                    </td>
                                    @endif
                                    <td width="319">

                                        <p>Designation: {{$lobby['designation'][$i]}} </p>
                                        <p>Name: {{$lobby['name'][$i]}}</p>
                                        <p>Phone: {{$lobby['phone'][$i]}}</p>
                                        <p>Email: {{$lobby['email'][$i]}}</p>
                                    </td>
                                    </tr>
                                    @endfor

                                    @php
                                    $media = json_decode($item->media, true);
                                    $media_size = count($media['name'], COUNT_RECURSIVE);

                                    @endphp
                                    @for ($i = 0; $i < $media_size; $i++) <tr>
                                        @if($i == 0)
                                        <td width="319">
                                            <center>
                                                <p style="color: #0b2e13">Media</p>
                                            </center>

                                        </td>
                                        @else
                                        <td width="319">
                                            <center>
                                                <p style="color: #0b2e13">Media ({{$i+1}})</p>
                                            </center>

                                        </td>
                                        @endif
                                        <td width="319">


                                            <p>Name: {{$media['name'][$i]}}</p>
                                            <p>Phone: {{$media['phone'][$i]}}</p>
                                            <p>Email: {{$media['email'][$i]}}</p>
                                        </td>
                                        </tr>
                                        @endfor

                    </tbody>
                </table>
                @endforeach


            </div>
        </div>
    </div>


    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
</body>

</html>