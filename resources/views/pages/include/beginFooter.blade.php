<div class="page-footer">
    <div class="page-footer-inner"> <?php echo date("Y");?> &copy; Developed By
        <a target="_blank" href="http://amarbebsha.com/">Amar Bebsha Limited</a> &nbsp;
    </div>
    <div class="scroll-to-top" style="margin-bottom: 15px;">
    	<img src="/admin/assets/layouts/layout/img/scrollTop.png" alt="" width="40" height="40" />
    </div>
</div>