<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            <!-- END SIDEBAR TOGGLER BUTTON -->


            <li class="nav-item {{Request::is('home*') ? 'active open' : ''}} ">
                <a href="{{route('home')}}" class="nav-link  nav-toggle">
                    <i class="icon-basket"></i>
                    <span class="title">Dashboard</span>
                    <span class="selected"></span>                 
                </a>
            
            </li>

            @can('manage-project')
            <li class="nav-item {{ Request::is('project*') ? 'active open' : '' }} ">
               <li class="nav-item {{ Route::currentRouteName() == 'dashboard' ? 'active open' : '' }} ">
                <a href="javascript:;" class="nav-link  nav-toggle">
                    <i class="fa fa-building"></i>
                    <span class="title">Manage Project</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    @can('project-list') 
                    <li class="nav-item {{ Request::is('project') ? 'active open' : '' }}">
                        <a href="{{Route('project')}} " class="nav-link ">
                          
                            <span class="title">Project List</span>
                        </a>
                    </li>
                    @endcan

                    @can('create-project')
                    <li class="nav-item {{ Request::is('addProject') ? 'active open' : '' }}">
                        <a href="{{Route('addProject')}}" class="nav-link ">
                        
                            <span class="title">Add Project</span>
                        </a>
                    </li>
                    @endcan
                  <!--   <li class="nav-item {{ Request::is('order/list/company') ? 'active open' : '' }}">
                        <a href="" class="nav-link ">
                            <i class="icon-user"></i>
                            <span class="title">Company Order List</span>
                        </a>
                    </li>
                    <li class="nav-item {{ Request::is('order/list/bill-To-Dealer') ? 'active open' : '' }}">
                        <a href=" " class="nav-link ">
                            <i class="icon-user"></i>
                            <span class="title">Refill/Package Dealer Bill</span>
                        </a>
                    </li> -->
                </ul>
            </li>
            @endcan

            @can('manage-cv')
            <li class="nav-item {{ Request::is('cvstaff*') ? 'active open' : '' }} ">
                {{--<li class="nav-item {{ Route::currentRouteName() == 'dashboard' ? 'active open' : '' }} ">--}}
                <a href="javascript:;" class="nav-link  nav-toggle">
                    <i class="icon-user"></i><i class="fas fa-paste"></i>

                    <span class="title">Manage Staff CV </span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    @can('cv-list')
                    <li class="nav-item {{ Request::is('cvList') ? 'active open' : '' }}">
                        <a href="{{Route('staffcv')}} " class="nav-link ">
                        
                            <span class="title">List of CV</span>
                        </a>
                    </li>
                    @endcan

                    @can('cv-create')  
                    <li class="nav-item {{ Request::is('staffCV/create') ? 'active open' : '' }} ">
                        <a href=" {{Route('addStaffcv')}}" class="nav-link active">
                           
                            <span class="title">Create CV</span>
                        </a>
                    </li>
                    @endcan

                    @can('cv-category') 
                    <li class="nav-item {{ Request::is('CV/category') ? 'active open' : '' }} ">
                        <a href=" {{Route('category')}}" class="nav-link active">
                            
                            <span class="title">CV Category</span>
                        </a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endcan

            @can('manage-client')
            <li class="nav-item {{ Request::is('brand*') ? 'active open' : '' }} ">
                {{--<li class="nav-item {{ Route::currentRouteName() == 'dashboard' ? 'active open' : '' }} ">--}}
                <a href="javascript:;" class="nav-link  nav-toggle">
                    <i class="fas fa-user-friends"></i>
                    <span class="title">Manage Client </span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                @can('create-client') 
                    <li class="nav-item {{ Request::is('brand/create') ? 'active open' : '' }}">
                        <a href="{{Route('addClient')}}" class="nav-link ">
                           
                            <span class="title">Add Client</span>
                        </a>
                    </li>
                @endcan
                @can('client-list')
                    <li class="nav-item {{ Request::is('brand') ? 'active open' : '' }} ">
                        <a href="{{Route('client')}}" class="nav-link active">
                           
                            <span class="title">Client List</span>
                        </a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endcan

            @can('manage-partner')
              <li class="nav-item {{ Request::is('brand*') ? 'active open' : '' }} ">
                {{--<li class="nav-item {{ Route::currentRouteName() == 'dashboard' ? 'active open' : '' }} ">--}}
                <a href="javascript:;" class="nav-link  nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">Manage Partner</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu"> 
                    @can('create-partner')
                    <li class="nav-item {{ Request::is('brand/create') ? 'active open' : '' }}">
                        <a href="{{Route('addPartner')}}" class="nav-link ">
                           
                            <span class="title">Add Partner</span>
                        </a>
                    </li>
                    @endcan
                    @can('partner-list')
                    <li class="nav-item {{ Request::is('brand') ? 'active open' : '' }} ">
                        <a href="{{Route('partner')}}" class="nav-link active">
                            
                            <span class="title">Partner List</span>
                        </a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endcan

            @can('manage-file')
            <li class="nav-item {{Request::is('advertisement*') ? 'active open' : ''}} ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-docs"></i>
                    <span class="title">Manage File</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    @can('create-multiplefile')
                    <li class="nav-item {{ Request::is('advertisement/create') ? 'active open' : '' }} ">
                        <a href="{{Route('createfile')}} " class="nav-link ">
                            
                            <span class="title">Add File</span>
                        </a>
                    </li>
                    @endcan
                    @can('multiplefile-list')
                    <li class="nav-item {{ Request::is('advertisement/list') ? 'active open' : '' }} ">
                        <a href="{{Route('viewfile')}} " class="nav-link ">
                           
                            <span class="title">File List</span>
                        </a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endcan
            <!-- <li class="nav-item {{Request::is('faq*') ? 'active open' : ''}} ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-user"></i>
                    <span class="title">Manage FAQ</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item {{Request::is('create_faq') ? 'active open' : ''}} ">
                        <a href=" " class="nav-link ">
                            <i class="icon-user"></i>
                            <span class="title">FAQ Add</span>
                        </a>

                    </li>

                    <li class="nav-item {{Request::is('faq') ? 'active open' : ''}} ">

                        <a href=" " class="nav-link ">
                            <i class="icon-user"></i>
                            <span class="title">faq List</span>
                        </a>


                    </li>
                </ul>
            </li>

            <li class="nav-item {{Request::is('banner*') ? 'active open' : ''}} ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-user"></i>
                    <span class="title">Manage Banner</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item {{Request::is('create_banner') ? 'active open' : ''}} ">
                        <a href=" " class="nav-link ">
                            <i class="icon-user"></i>
                            <span class="title">Banner Add</span>
                        </a>

                    </li>

                    <li class="nav-item {{Request::is('banner') ? 'active open' : ''}} ">

                        <a href=" " class="nav-link ">
                            <i class="icon-user"></i>
                            <span class="title">Banner List</span>
                        </a>


                    </li>
                </ul>
            </li> -->
            @can('manage-user') 
            <li class="nav-item {{Request::is('users*') ? 'active open' : ''}} ">
                <a href="javascript:;" class="nav-link nav-toggle">
                  <i class="fas fa-user"></i>
                    <span class="title">Manage Users</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">


                    <li class="nav-item {{Request::is('users/create') ? 'active open' : ''}} ">
                        <a href="{{route('users.create')}}" class="nav-link ">
                          
                            <span class="title">Create New User</span>
                        </a>
                    </li>


                    <li class="nav-item {{Request::is('users') ? 'active open' : ''}} ">
                        <a href="{{route('users.index')}}" class="nav-link ">
                           
                            <span class="title">Users List</span>
                        </a>
                    </li>

                  <!--   <li class="nav-item {{Request::is('users/dealer') ? 'active open' : ''}} ">
                        <a href=" " class="nav-link ">
                            <i class="icon-clock"></i>
                            <span class="title">Dealer List</span>
                        </a>
                    </li> -->
                </ul>
            </li>
            @endcan


             @can('manage-role') 
            <li class="nav-item {{Request::is('roles*') ? 'active open' : ''}}  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fas fa-user-plus"></i>
                    <span class="title">Manage Role</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">

                    @can('role-create')
                    <li class="nav-item {{Request::is('roles/create') ? 'active open' : ''}}  ">
                        <a href="{{ route('roles.create') }}" class="nav-link ">
                           
                            <span class="title">Create New Role</span>
                        </a>
                    </li>
                    @endcan

                    @can('role-list')
                    <li class="nav-item {{Request::is('roles') ? 'active open' : ''}} ">
                        <a href="{{ route('roles.index') }}" class="nav-link ">
                           
                            <span class="title">Role List</span>
                        </a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endcan
         <!--    <li class="nav-item {{Request::is('notification*') ? 'active open' : ''}} ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-user"></i>
                    <span class="title">Manage Notification</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item {{Request::is('create_notification') ? 'active open' : ''}} ">
                        <a href=" " class="nav-link ">
                            <i class="icon-user"></i>
                            <span class="title">Notification Add</span>
                        </a>

                    </li>

                    <li class="nav-item {{Request::is('notification') ? 'active open' : ''}} ">

                        <a href=" " class="nav-link ">
                            <i class="icon-user"></i>
                            <span class="title">Notification List</span>
                        </a>


                    </li>
                </ul>
            </li>


            <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-docs"></i>
                    <span class="title">Apps</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href=" " class="nav-link ">
                            <i class="icon-clock"></i>
                            <span class="title">test Table</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href=" " class="nav-link ">
                            <i class="icon-clock"></i>
                            <span class="title">test 1</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href=" " class="nav-link ">
                            <i class="icon-check"></i>
                            <span class="title">table</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href=" " class="nav-link ">
                            <i class="icon-envelope"></i>
                            <span class="title">ajax post</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href=" " class="nav-link ">
                            <i class="icon-calendar"></i>
                            <span class="title">ajax Page View</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href=" " class="nav-link ">
                            <i class="icon-notebook"></i>
                            <span class="title">Dependent Dropdown View</span>
                        </a>
                    </li>
                </ul>
            </li> -->




        </ul>
        <!-- END SIDEBAR MENU -->
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>