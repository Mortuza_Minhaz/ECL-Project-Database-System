<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
           'role-list',
           'role-create',
           'role-edit',
           'role-delete',
           'manage-role',
           'manage-user',
           'project-list',
           'create-project',
           'edit-project',
           'delete-project',
           'project-report',
           'manage-project',
           'projectfile-download',
           'cv-list',
           'cv-create',
           'cv-edit',
           'cv-delete',
           'cv-report',
           'manage-cv',
           'cvfile-download',
           'cv-category',
           'create-category',
           'edit-category',
           'delete-category',
           'client-list',
           'create-client',
           'edit-client',
           'delete-client',
           'client-report',
           'manage-client',
           'partner-list',
           'create-partner',
           'edit-partner',
           'delete-partner',
           'partner-report',
           'manage-partner',
           'partnerfile-download',
           'multiplefile-list',
           'create-multiplefile',
           'multiplefile-edit',
           'multiplefile-delete',
           'manage-file',
           'multiplefile-download'          

        ];
   
        foreach ($permissions as $permission) {
             Permission::create(['name' => $permission]);
        }
    }
}
