<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectInvolveStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_involve_staff', function (Blueprint $table) {
            $table->id();
            $table->integer('projects_id');
            $table->integer('staff_id')->nullable();
            $table->string('assign_position')->nullable();
            $table->string('duties')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_involve_staff');
    }
}
