<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectInvolveClientsInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_involve_clients_info', function (Blueprint $table) {
            $table->id();
            $table->integer('projects_id');
            $table->integer('clients_id')->nullable();
            $table->string('consulting_fee')->nullable();
            $table->string('lobbing_expenses')->nullable();
            $table->json('project_director')->nullable();
            $table->json('head_of_the_department')->nullable();
            $table->json('lobby')->nullable();
            $table->json('media')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_involve_clients_info');
    }
}
