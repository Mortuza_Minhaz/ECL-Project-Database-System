<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partners', function (Blueprint $table) {
            $table->id();
            $table->string('partner_company_name');
            $table->string('partner_home_country');
            $table->string('partner_address')->nullable();
            $table->string('partner_phone')->nullable();
            $table->string('partner_mail')->nullable();
            $table->json('contact_person')->nullable();
            $table->json('jv_bidding')->nullable();
            $table->json('jv_project')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partners');
    }
}
