<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Staff_Category extends Model
{
    use HasFactory;

    protected $guarded = [];


    protected $table = 'staff_categories'; 

    public function cv_staff()
    {
    	return $this->hasMany('App\CvStaff');
    }
}
