<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function partner_specialization() {

        return $this->hasMany('App\Models\Specialization', 'partner_id', 'id');
    }
}