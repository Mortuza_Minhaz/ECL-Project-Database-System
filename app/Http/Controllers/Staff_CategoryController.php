<?php

namespace App\Http\Controllers;

use App\Models\CvStaff;
use App\Models\Staff_Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;
use DateTime;

class Staff_CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $basenameUrl = basename(URL::current());
        $categoryList = Staff_Category::orderBy('id', 'DESC')->get();

        return view('pages/staff_cv/indexCvCategory', compact('basenameUrl', 'categoryList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // date_default_timezone_set('Asia/dhaka'); 
        // $datea = new DateTime($request->input('birth_d'));

        // $dateb = new DateTime(date("Y-m-d H:i:s")); 

        // $interval = $datea->diff($dateb);

        // $diffInYears   = $interval->y;


        // echo '<pre>';
        // print_r($diffInYears);
        // exit();

        $requestData = $request->all();

        if (Staff_Category::create($requestData)) {
            return redirect()->route('category')->with('success', 'Successfully Add');
        }
        return back()->with('failed', 'Something went wrong. Try again');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $basenameUrl = basename(URL::current());
        $cvCategorydata = DB::table('staff_categories')->where('id', $id)->first();

        return view('pages/staff_cv/editcategory', compact('cvCategorydata', 'basenameUrl'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Staff_Category::find($id);


        $data->category_name = $request->category_name;

        // $data = array();
        // $data['category_name'] = $request->category_name;

        // $Categorydata = DB::table('staff_categories')->where('id', $id)->update($data);

        $data->save();

        return redirect()->route('category')
            ->with('success', 'Updated! The Data Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Staff_Category::find($id);

        $data->delete();

        return redirect()->route('category')
            ->with('success', 'The Data Deleted Successfully');
    }
}
