<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $basenameUrl = basename(URL::current());

        $clientList = Client::all();

        // $clientList = Client::orderBy('id', 'DESC')->get();

        //  $clientList = DB::table('clients')
        //          ->select(
        //              'clients.*',
        //              'projects.project_name'
        //          )
        //          ->join('projects', 'projects.id', '=', 'clients.projects_id')
        //          ->orderBy('id', 'DESC')
        //          ->get();

        return view('pages/client/indexClient', compact('basenameUrl', 'clientList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $basenameUrl = basename(URL::current());

        $clients = Client::all();

        return view('pages/client/createClient', compact('basenameUrl', 'clients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'client_name' => 'required',
            'client_address' => 'required',

        ]);
        $requestData = $request->all();

        if (Client::create($requestData)) {
            return redirect('clientList')->with('success', 'Successfully Add');
        }
        return back()->with('failed', 'Something went wrong. Try again');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Client $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client, $id)
    {
        $basenameUrl = basename(URL::current());
        $clientData = DB::table('clients')->where('id', $id)->first();

        $clientProjectName = DB::table('projects')->select(
            'project_name',
            'in_location',
            'project_description',
            'projects.start_date',
            'projects.end_date',
            'project_involve_clients_info.*'
        )
            ->join('project_involve_clients_info', 'project_involve_clients_info.projects_id', '=', 'projects.id')
            ->where('client_name', $id)->get();

        



        return view('pages/client/reportClient', compact('clientData', 'basenameUrl', 'clientProjectName'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Client $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        $basenameUrl = basename(URL::current());

        return view('pages/client/editClient', compact('client', 'basenameUrl'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Client $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        $requestData = $request->all();
        if($client->update($requestData)){

        return redirect()->to('clientList')->with('success', 'Successfully Updated');
    }
        return back()->with('failed', 'Something went wrong. Try again');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Client $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {

        $client->delete();

        return redirect()->route('client')
            ->with('success', 'The Data Deleted Successfully');
    }
}
