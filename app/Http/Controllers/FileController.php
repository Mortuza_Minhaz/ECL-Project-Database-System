<?php

namespace App\Http\Controllers;

use App\Models\File;

use App\Models\notificationMultifiles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class FileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $basenameUrl = basename(URL::current());
        $futureDate = File::whereDate('exp_date', '>=', Carbon::now())
        ->orderBy('exp_date', 'asc')
        ->get();



        $pastDate = File::whereDate('exp_date', '<=', Carbon::now())
        ->orderBy('exp_date', 'desc')
        ->get();

        $files = $futureDate->merge($pastDate);

        //     $fileData = DB::table('files')
        //     ->select('files.*', 'path')
        //     ->join('notification_multi_files', 'file_id', '=', 'files.id')
        //     ->where('id', $id)
        //     ->first();

        // echo '<pre>';
        // print_r($fileData);
        // exit();


        return view('pages/file/indexFile', compact('basenameUrl', 'files'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $basenameUrl = basename(URL::current());

        return view('pages/file/createFile', compact('basenameUrl'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // echo '<pre>';
        // print_r($request->all());
        // exit();

        $requestDataFile['file_name'] = $request->get('file_name');
        $requestDataFile['description'] = $request->get('description');
        $requestDataFile['start_date'] = $request->get('start_date');
        $requestDataFile['exp_date'] = $request->get('exp_date');

        $file_info = File::create($requestDataFile);

        if ($file_info->id) {

            $insertArr = array();
            if ($files = $request->file('multi_files')) {
                foreach ($files as $image) {


                    $folder_name = 'Multiple_Files';

                    $images = $this->_uploadImage($image, $folder_name);


                    $insertArr[] = ['file_id' => $file_info->id, 'path' => $images, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()];
                }
            }
            DB::table('notification_multi_files')->insert($insertArr);
        }


        return redirect()->route('viewfile')->with('success', 'Successfully Added');
    }

    private function _uploadImage($image, $folder_name)
    {
        $name = time() . '-' . $image->getClientOriginalName();
        $image->move('admin/upload/' . $folder_name, $name);
        return '/admin/upload/' . $folder_name . '/' . $name;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\File  $file
     * @return \Illuminate\Http\Response
     */
    public function show(File $file, $id)
    {

        $basenameUrl = basename(URL::current());
        $fileData = DB::table('files')->where('id', $id)->first();



        $notification_multiple_files = DB::table('notification_multi_files')
            ->select('path')
            ->where('file_id', $id)
            ->get();

           
   
        return view('pages/file/reportFile', compact('basenameUrl', 'fileData','notification_multiple_files'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\File  $file
     * @return \Illuminate\Http\Response
     */
    public function edit(File $file)
    {
        $basenameUrl = basename(URL::current());

        $notification_multiple_files = DB::table('notification_multi_files')
            ->select('path','file_id','id')

            ->where('file_id', $file->id)
            ->get();

        return view('pages/file/editFile', compact('basenameUrl','file','notification_multiple_files'));
    }

    public function notificationFileDelete($multifileId, $fileId)
    {
        
        $multipleFile = notificationMultiFiles::where('id', $fileId)->where('file_id', $multifileId)->first();    

 $old_image = public_path($multipleFile->path);

            if (is_file($old_image)) {

                unlink($old_image);
            }


        $multipleFile->delete();

        return \Redirect::back()->with('success', 'Successfully Delete');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\File  $file
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, File $file)
    {
        // echo "<'pre'>";
        // print_r($request->all());
        // exit();

       // $requestData = $request->all();
        $requestData  = $request->except(['multi_files']);

        $file->update($requestData);

        $file_id = $file->id;

        if ($request->hasFile('multi_files')) {

        $insertArr = array();
            if ($files = $request->file('multi_files')) {
                foreach ($files as $image) {

                    $folder_name = 'Multiple_Files';
                    $images = $this->_uploadImage($image, $folder_name);

                    $insertArr[] = ['file_id' => $file_id, 'path' => $images, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()];
                }
            }
            DB::table('notification_multi_files')->insert($insertArr);
        }

        return redirect()->route('viewfile')->with('success', 'Successfully Updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\File  $file
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $file = File::findOrFail($id);

    $fileGroups = File::where('id', $file->id)->get();
    foreach ($fileGroups as $fileGroup) {
        DB::table('notification_multi_files')->where('file_id', $fileGroup->id)->delete();
    }

    File::where('id', $file->id)->delete();

    return redirect()->route('viewfile')->with('success', 'Successfully Deleted');
    }
}


