<?php

namespace App\Http\Controllers;

use App\Models\Partner;
use App\Models\PartnerMultipleFile;
use App\Models\Specialization;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use File;

class PartnerController extends Controller
{

    public function index()
    {
        $basenameUrl = basename(URL::current());


        /*partner_specialization*/

        $partnerList = Partner::orderBy('id', 'DESC')->get();


        /*
                foreach ($partnerList as $item){
                    foreach ($item->partner_specialization as $value){
                        echo '<pre>';
                        echo($value->sector);
                    }

                }*/


        return view('pages/partner/indexPartner', compact('basenameUrl', 'partnerList'));
    }


    public function create()
    {
        $basenameUrl = basename(URL::current());

        $partners = Partner::all();

        return view('pages/partner/createPartner', compact('basenameUrl', 'partners'));
    }


    public function store(Request $request)
    {

        // echo '<pre>';
        //             print_r($request->all());
        //             exit();
        // $requestData = $request->all();

        request()->validate([
            'partner_company_name' => 'required',
            'partner_home_country' => 'required',

        ]);


        $requestDataPartner['partner_company_name'] = $request->get('partner_company_name');
        $requestDataPartner['partner_home_country'] = $request->get('partner_home_country');
        $requestDataPartner['partner_address'] = implode('*', $request->partner_address);
        $requestDataPartner['partner_phone'] = implode(',', $request->partner_phone);
        $requestDataPartner['partner_mail'] = implode(',', $request->partner_mail);
        $requestDataPartner['contact_person'] = json_encode($request->contact_person, JSON_PRETTY_PRINT);
        $requestDataPartner['jv_bidding'] = json_encode($request->jv_bidding, JSON_PRETTY_PRINT);
        $requestDataPartner['jv_project'] = json_encode($request->jv_project, JSON_PRETTY_PRINT);

        $requestDataPartnerInsertId = Partner::create($requestDataPartner);


        if ($requestDataPartnerInsertId->id) {

            $partner_id = $requestDataPartnerInsertId->id;

            foreach ($request['group-b'] as $item) {

                $requestDataSpecialization['partner_id'] = $partner_id;
                $requestDataSpecialization['sector'] = $item['sector'];
                $requestDataSpecialization['tags'] = $item['tags'];

                Specialization::create($requestDataSpecialization);
            }

            $insertArr = array();
            if ($files = $request->file('partner_files')) {
                foreach ($files as $image) {

                    $folder_name = 'partner_files';
                    $images = $this->_uploadImage($image, $folder_name);

                    $insertArr[] = ['partner_id' => $partner_id, 'file_path' => $images, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()];
                }
            }
            DB::table('partner_multiple_files')->insert($insertArr);
        }


        if ($requestDataPartnerInsertId->id) {
            return redirect('partnerList')->with('success', 'Successfully Add');
        }

        return back()->with('failed', 'Something went wrong. Try again');
    }


    private function _uploadImage($image, $folder_name)
    {
        $name = time() . '-' . $image->getClientOriginalName();
        $image->move('admin/upload/' . $folder_name, $name);
        return '/admin/upload/' . $folder_name . '/' . $name;
    }


    public function show(Partner $partner, $id)
    {
        $basenameUrl = basename(URL::current());
        $partnertData = DB::table('partners')->where('id', $id)->first();


        $projectsInfo = DB::table("projects")
            ->whereRaw("find_in_set('" . $id . "',projects.jv_consultant)")
            ->orWhere('jv_lead_consultant', $id)
            ->get();

        $specializationsInfo = DB::table('specializations')
            ->select('sector', 'tags')
            ->where('partner_id', $id)
            ->get();

        $partner_multiple_files = DB::table('partner_multiple_files')
            ->select('file_path')
            ->where('partner_id', $id)
            ->get();




        return view('pages/partner/reportPartner', compact('partnertData', 'basenameUrl', 'projectsInfo', 'specializationsInfo', 'partner_multiple_files'));
    }


    public function edit(Partner $partner)
    {
        $basenameUrl = basename(URL::current());
       /*  echo '<pre>';
          print_r($partner->id);
          exit();*/


        /* foreach ($partner->partner_specialization as $value){
             echo '<pre>';
             echo($value->sector);
         }*/

        $partner_multiple_files = DB::table('partner_multiple_files')
            ->select('file_path','partner_id','id')

            ->where('partner_id', $partner->id)
            ->get();

        /*echo '<pre>';
        print_r($partner_multiple_files);
        exit();*/


        return view('pages/partner/editPartner', compact('partner_multiple_files','partner', 'basenameUrl'));
    }


    public function update(Request $request, Partner $partner)
    {


        $Partner_id = $partner->id;


        $requestData['partner_company_name'] = $request->get('partner_company_name');
        $requestData['partner_home_country'] = $request->get('partner_home_country');
        $requestData['partner_address'] = implode('*', $request->partner_address);
        $requestData['partner_phone'] = implode(',', $request->partner_phone);
        $requestData['partner_mail'] = implode(',', $request->partner_mail);
        $requestData['contact_person'] = json_encode($request->contact_person, JSON_PRETTY_PRINT);
        $requestData['jv_bidding'] = json_encode($request->jv_bidding, JSON_PRETTY_PRINT);
        $requestData['jv_project'] = json_encode($request->jv_project, JSON_PRETTY_PRINT);
        $partner->update($requestData);

        if ($request->has('group-b')) {


            Specialization::where('partner_id', $Partner_id)->delete();

            foreach ($request['group-b'] as $item) {

                $requestDataSpecialization['partner_id'] = $Partner_id;
                $requestDataSpecialization['sector'] = $item['sector'];
                $requestDataSpecialization['tags'] = $item['tags'];

                Specialization::create($requestDataSpecialization);
            }

        }

        if ($request->hasFile('partner_files')) {

            $insertArr = array();
            if ($files = $request->file('partner_files')) {
                foreach ($files as $image) {

                    $folder_name = 'partner_files';
                    $images = $this->_uploadImage($image, $folder_name);

                    $insertArr[] = ['partner_id' => $Partner_id, 'file_path' => $images, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()];
                }
            }
            DB::table('partner_multiple_files')->insert($insertArr);
        }


        return redirect('partnerList')->with('success', 'Successfully Updated');
    }


    public function PartnerFileDelete($projectId, $fileId)
    {


        $partnertFile = PartnerMultipleFile::where('id', $fileId)->where('partner_id', $projectId)->first();



        $old_image = $partnertFile->file_path;
        if (File::exists(public_path($old_image))) {

            File::delete(public_path($old_image));
        }
        $partnertFile->delete();

        return \Redirect::back()->with('success', 'Successfully Delete');

    }

    public function destroy($id)
    {
    
    $partnerData = Partner::findOrFail($id);

    $fileGroups = Partner::where('id', $partnerData->id)->get();
    foreach ($fileGroups as $fileGroup) {
        DB::table('partner_multiple_files')->where('partner_id', $fileGroup->id)->delete();
        DB::table('specializations')->where('partner_id', $fileGroup->id)->delete();
    }

    Partner::where('id', $partnerData->id)->delete();

    return redirect()->route('partner')->with('success', 'Deleted! The Data Deleted Successfully');

    }
}


