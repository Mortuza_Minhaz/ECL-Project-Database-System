<?php
    
namespace App\Http\Controllers;
    
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\URL;
use DB;
use Hash;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use App\Utility\Utility;
    
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
         if (!Utility::permissionCheck('manage-user')) {
            return back()->with('failed', Utility::getPermissionMsg());
        }

        $basenameUrl = basename(URL::current());
        $data = User::orderBy('id','DESC')->paginate(5);
        return view('users.index',compact('data','basenameUrl'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }
    
     public function changePasswordView()
    {
        $basenameUrl = basename(URL::current());
  $user = User::find(Auth::id());

        return view('users.changePasswordView',compact('basenameUrl','user'));
       
    }

    public function changePasswordUpdate(Request $request){

        


     /*    if($request->filled('oldPassword')) {
        dd('user_id is not empty.');
    } else {
        dd('user_id is empty.');
    }
    exit();*/

      if($request->filled('oldPassword')){

      
         if (!(Hash::check($request->get('oldPassword'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("failed","Your current password does not matches with the password you provided. Please try again.");
        }
      }




 if($request->filled('oldPassword')){
        if(strcmp($request->get('oldPassword'), $request->get('newPassword')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("failed","New Password cannot be same as your current password. Please choose a different password.");
        }
    }


    //Change Password
        $user = Auth::user();
        $user->name = $request->filled('name')? $request->get('name') :$user->name;
        $user->email = $request->filled('email')? $request->get('email') :$user->email;
        $user->phone = $request->filled('phone')? $request->get('phone') :$user->phone;
        $user->password = $request->filled('newPassword')? Hash::make($request->get('newPassword')):$user->password ;
        $user->save();
        return redirect()->back()->with("success","updated successfully !");


    }
    
    public function create()
    {
        $basenameUrl = basename(URL::current());
        $roles = Role::pluck('name','name')->all();
        return view('users.create',compact('roles','basenameUrl'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);
    
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
    
        $user = User::create($input);
        $user->assignRole($request->input('roles'));
    
        return redirect()->route('users.index')
                        ->with('success','User created successfully');
    }
    
      public function changePassword____al_mamun(Request $request) {
        if (!(Hash::check($request->get('oldPassword'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("failed","Your current password does not matches with the password you provided. Please try again.");
        }

        if(strcmp($request->get('oldPassword'), $request->get('newPassword')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("failed","New Password cannot be same as your current password. Please choose a different password.");
        }

        $validatedData = $request->validate([
            'oldPassword' => 'required',
            'newPassword' => 'required|string|min:6|confirmed',
            'newPassword_confirmation' => 'required|same:newPassword'
        ]);

        //Change Password
        $user = Auth::user();
        $user->password = Hash::make($request->get('newPassword'));
        $user->save();
        return redirect()->back()->with("status","Password changed successfully !");
    }
    public function show($id)
    {


        $user = User::find($id);
        return view('users.show',compact('user'));
    }
    
    

    public function edit($id)
    {
        $basenameUrl = basename(URL::current());
        $user = User::find($id);
        $roles = Role::pluck('name','name')->all();
        $userRole = $user->roles->pluck('name','name')->all();
    
        return view('users.edit',compact('user','roles','userRole','basenameUrl'));
    }
    
    
    public function update(Request $request, $id)
    {
        

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'same:confirm-password',
            'roles' => 'required'
        ]);
    
        $input = $request->all();
        if(!empty($input['password'])){ 
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = Arr::except($input,array('password'));    
        }
    
        $user = User::find($id);
        $user->update($input);
        DB::table('model_has_roles')->where('model_id',$id)->delete();
    
        $user->assignRole($request->input('roles'));
    
        return redirect()->route('users.index')
                        ->with('success','User updated successfully');
    }


  
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('users.index')
                        ->with('success','User deleted successfully');
    }
}