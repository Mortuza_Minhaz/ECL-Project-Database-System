<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CvStaff;
use App\Models\Project;
use App\Models\Partner;
use App\Models\Client;
use App\Models\ProjectFile;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use App\Models\ProjectInvolveStaff;
use Illuminate\Support\Facades\File;
use App\Utility\Utility;


class ProjectController extends Controller
{
    public function index(Request $request)
    {
         if (!Utility::permissionCheck('project-list')) {
            return back()->with('failed', Utility::getPermissionMsg());
        }

        $basenameUrl = basename(URL::current());

        // $projectList = Project::orderBy('id', 'DESC')->get();

        $start_date = $request->input('start_date');

        $end_date = $request->input('end_date');

        if (!empty($start_date) && ($end_date)) {

            $datewiseProjectList = DB::table('projects')
                ->select('projects.*')
                ->orderBy('id', 'DESC')
                ->where('start_date', $start_date)
                ->where('end_date', $end_date)
                ->get();
        } else {
            $datewiseProjectList = DB::table('projects')
                ->select('projects.*')
                ->orderBy('id', 'DESC')
                ->get();
        }

        return view('pages/projects/indexProject', compact('basenameUrl', 'datewiseProjectList'));
    }


    public function create()
    {

        $basenameUrl = basename(URL::current());

        $staff = CvStaff::all();

        $partners = Partner::all();

        $clients = Client::all();

        return view('pages/projects/createProject', compact('basenameUrl', 'staff', 'partners', 'clients'));
    }

    public function yearMonth($date_convert)
    {

        $dateMonthArray = explode('/', $date_convert);

        $month = $dateMonthArray[0];
        $year = $dateMonthArray[1];


        return $date = Carbon::createFromDate($year, $month);
    }

    public function insert(Request $request)
    {
        request()->validate([
            'project_name' => 'required',
            'project_description' => 'required',
            'project_cost' => 'required',
            'service_render' => 'required',

        ]);

        // echo '<pre>';
        //     print_r($request->all());
        //     exit();


        $start_date = $request->input('start_date');
        $end_date = $request->input('end_date');


        $data = array();
        $data['project_name'] = $request->project_name;
        $data['country'] = $request->country;
        $data['in_location'] = $request->in_location;
        $data['client_name'] = $request->clients_id;
        $data['staff_months'] = $request->staff_months;
        $data['jv_consultant'] = $request->has('jv_consultant') ? implode(',', $request->jv_consultant) : '';
        $data['jv_lead_consultant'] = $request->jv_lead_consultant;
        $data['jv_staff_months'] = $request->jv_staff_months;
        $data['project_description'] = $request->project_description;
        $data['project_cost'] = $request->project_cost;
        $data['start_date'] = $this->yearMonth($start_date);
        $data['end_date'] = $this->yearMonth($end_date);
        $data['service_render'] = $request->service_render;
        $data['project_tag'] = implode(',', $request->project_tag);
        $data['remarks'] = $request->remarks;


        $projects_id = DB::table('projects')->insertGetId($data);

        if ($projects_id and $request->has('staff_id')) {

            #$staff_id = $request->input('staff_id');

            $size = count(collect($request)->get('staff_id'));


            for ($i = 0; $i < $size; $i++) {

                $requestData['projects_id'] = $projects_id;
                $requestData['staff_id'] = $request->get('staff_id')[$i];
                $requestData['duties'] = $request->get('duties')[$i];
                $requestData['assign_position'] = $request->get('assign_position')[$i];
                ProjectInvolveStaff::create($requestData);
            }
        }


        if ($projects_id) {

            $requestDataClient['projects_id'] = $projects_id;
            $requestDataClient['clients_id'] = $request->get('clients_id');

            # $requestDataClient['client_address'] = $request->get('client_address');
            $requestDataClient['consulting_fee'] = $request->get('consulting_fee');

            $requestDataClient['lobbing_expenses'] = $request->get('lobbing_expenses');
            $requestDataClient['project_director'] = json_encode($request->project_director, JSON_PRETTY_PRINT);

            $requestDataClient['head_of_the_department'] = json_encode($request->head_of_the_department, JSON_PRETTY_PRINT);

            $requestDataClient['lobby'] = json_encode($request->lobby, JSON_PRETTY_PRINT);

            $requestDataClient['media'] = json_encode($request->media, JSON_PRETTY_PRINT);


            $requestDataClient['created_at'] = Carbon::now();
            $requestDataClient['updated_at'] = Carbon::now();


            DB::table('project_involve_clients_info')->insert($requestDataClient);
        }


        $files = $request->file('project_file');

        if (isset($files)) {
            $insertArr = array();
            if ($files = $request->file('project_file')) {
                foreach ($files as $image) {

                    $folder_name = 'project_files';
                    $images = $this->_uploadImage($image, $folder_name);

                    $insertArr[] = ['projects_id' => $projects_id, 'path' => $images, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()];
                }
            }
            DB::table('project_files')->insert($insertArr);
        }


        return redirect()->route('project');
    }

    public function edit($id)
    {
        
        $staff = CvStaff::all();
        $partners = Partner::all();

        $clients = Client::all();

        $basenameUrl = basename(URL::current());
        $projectData = DB::table('projects')->where('id', $id)->first();

        $project_involve_staff_info = DB::table('project_involve_staff')->select('staff_id', 'assign_position', 'duties')->where('projects_id', $id)->get()->toArray();


        $project_involve_clients_info = DB::table('project_involve_clients_info')
            ->where('projects_id', $id)
            ->first();

        $project_files_info = DB::table('project_files')
            ->where('projects_id', $id)
            ->get();


        /*->toArray();*/


        $contryList = DB::table('country')->get();


        return view('pages/projects/editProject2', compact('project_files_info', 'project_involve_staff_info', 'contryList', 'projectData', 'basenameUrl', 'staff', 'partners', 'clients', 'project_involve_clients_info'));
    }

    public function ProjectFileDelete($projectId, $fileId)
    {

        $ProjectFile = ProjectFile::where('id', $fileId)->where('projects_id', $projectId)->first();

        $old_image = $ProjectFile->path;
        if (File::exists(public_path($old_image))) {

            File::delete(public_path($old_image));
        }
        $ProjectFile->delete();

        return \Redirect::back()->with('success', 'file Successfully Deleted');

    }

    public function show($id)
    {

        $basenameUrl = basename(URL::current());
        $pdata = DB::table('projects')->where('id', $id)->first();

        $clientName = DB::table('clients')->where('id', $pdata->client_name)->first();



        $project_involve_staff_id = DB::table('project_involve_staff')->select('staff_id')->where('projects_id', $id)->get();

        $project_involve_staff_id_array = array();

        foreach ($project_involve_staff_id as $value) {
            $project_involve_staff_id_array[] = $value->staff_id;
        }

        // $project_involve_staff_id_array_count = count($project_involve_staff_id_array);

          if (empty($project_involve_staff_id_array[0])) {
            $project_involve_staff_id_array_count = '';     

        } else {
            
            $project_involve_staff_id_array_count = count($project_involve_staff_id_array);
        }


        $project_involve_staff_position = DB::table('cv_staff')
            ->select('cv_staff.staff_name', 'project_involve_staff.assign_position')
            ->join('project_involve_staff', 'staff_id', '=', 'cv_staff.id')
            ->where('project_involve_staff.projects_id', $id)
            ->whereIn('cv_staff.id', $project_involve_staff_id_array)
            ->get();

        $jv_consultant = explode(',', $pdata->jv_consultant);

        $project_involve_partners = DB::table('partners')
            ->select('partner_company_name')
            ->whereIn('id', $jv_consultant)
            ->get();

        $project_involve_lead_partners = DB::table('partners')
            ->select('partner_company_name as lead_consultant')
            ->where('id', $pdata->jv_lead_consultant)
            ->first();

        $contryList = DB::table('country')->get();

        $project_multiple_files = DB::table('project_files')
            ->select('path')
            ->where('projects_id', $id)
            ->get();

        // echo "<pre>";
        // print_r($project_involve_staff_id_array);
        // exit();


        return view('pages/projects/reportProject', compact('contryList', 'pdata', 'basenameUrl', 'project_involve_staff_position', 'project_involve_partners', 'project_involve_staff_id_array_count', 'project_involve_lead_partners','project_multiple_files','clientName'));
    }

    public function projectFile($id)
    {
        $basenameUrl = basename(URL::current());
        $project_multiple_files = DB::table('project_files')
            ->select('path')
            ->where('projects_id', $id)
            ->get();

         $pdata = DB::table('projects')->where('id', $id)->first();

         return view('pages/projects/projectFile', compact('basenameUrl', 'project_multiple_files','pdata'));
    }

    public function update(Request $request, $id)
    {


        /* $size = count(collect($request)->get('staff_id'));

         echo '<pre>';
         print_r( $size = count(collect($request)->get('staff_id')));
         exit();*/

        $start_date_test = $request->input('start_date');
        $end_date_test = $request->input('end_date');

        $singleProject = Project::where('id', $id)->get()->first();

        $data = array();
        $data['project_name'] = $request->project_name;
        $data['country'] = $request->country;
        $data['in_location'] = $request->in_location;
        $data['client_name'] = $request->clients_id;
        $data['staff_months'] = $request->staff_months;
        $data['jv_consultant'] = $request->has('jv_consultant') ? implode(',', $request->jv_consultant) : '';
        $data['jv_lead_consultant'] = $request->jv_lead_consultant;
        $data['jv_staff_months'] = $request->jv_staff_months;
        $data['project_description'] = $request->project_description;
        $data['project_cost'] = $request->project_cost;
        $data['start_date'] = $this->yearMonth($start_date_test);
        $data['end_date'] = $this->yearMonth($end_date_test);
        $data['service_render'] = $request->service_render;
        $data['project_tag'] = implode(',', $request->project_tag);
        $data['remarks'] = $request->remarks;


        if ($id and $request->has('staff_id')) {

            ProjectInvolveStaff::where('projects_id', $id)->delete();

            $size = count(collect($request)->get('staff_id'));


            for ($i = 0; $i < $size; $i++) {

                $requestData['projects_id'] = $id;
                $requestData['staff_id'] = $request->get('staff_id')[$i];
                $requestData['duties'] = $request->get('duties')[$i];
                $requestData['assign_position'] = $request->get('assign_position')[$i];
                ProjectInvolveStaff::create($requestData);
            }
        }


        if ($id) {

            $requestDataClient['projects_id'] = $id;
            $requestDataClient['clients_id'] = $request->get('clients_id');

            # $requestDataClient['client_address'] = $request->get('client_address');
            $requestDataClient['consulting_fee'] = $request->get('consulting_fee');

            $requestDataClient['lobbing_expenses'] = $request->get('lobbing_expenses');
            $requestDataClient['project_director'] = json_encode($request->project_director, JSON_PRETTY_PRINT);

            $requestDataClient['head_of_the_department'] = json_encode($request->head_of_the_department, JSON_PRETTY_PRINT);

            $requestDataClient['lobby'] = json_encode($request->lobby, JSON_PRETTY_PRINT);

            $requestDataClient['media'] = json_encode($request->media, JSON_PRETTY_PRINT);


            /*  $requestDataClient['created_at'] = Carbon::now();*/
            $requestDataClient['updated_at'] = Carbon::now();

            DB::table('project_involve_clients_info')
                ->where('projects_id', $id)
                ->update($requestDataClient);
        }


        $files = $request->file('project_file');

        if (isset($files)) {
            $insertArr = array();
            if ($files = $request->file('project_file')) {
                foreach ($files as $image) {

                    $folder_name = 'project_files';
                    $images = $this->_uploadImage($image, $folder_name);

                    $insertArr[] = ['projects_id' => $id, 'path' => $images, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()];
                }
            }
            DB::table('project_files')->insert($insertArr);
        }

        if ($singleProject->update($data)) {
            return redirect()->route('project')->with('success', 'Successfully Update');
        }


    }


    public function delete($id)
    {

    $projectData = Project::findOrFail($id);

    $fileGroups = Project::where('id', $projectData->id)->get();
    foreach ($fileGroups as $fileGroup) {
        DB::table('project_involve_clients_info')->where('projects_id', $fileGroup->id)->delete();
        DB::table('project_involve_staff')->where('projects_id', $fileGroup->id)->delete();
        DB::table('project_files')->where('projects_id', $fileGroup->id)->delete();
    }

    Project::where('id', $projectData->id)->delete();

    return redirect()->route('project')->with('success', 'Deleted! The Data Deleted Successfully');

    }

    private function _uploadImage($image, $folder_name)
    {
        $name = time() . '-' . $image->getClientOriginalName();
        $image->move('admin/upload/' . $folder_name, $name);
        return '/admin/upload/' . $folder_name . '/' . $name;
    }
}

