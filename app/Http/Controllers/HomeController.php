<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\CvStaff;
use App\Models\Partner;
use App\Models\Client;
use App\Models\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

      public function changePasswordView()
    {
        $basenameUrl = basename(URL::current());
        echo "hii";
        exit();
    }
    
  
    public function index()
    {
        $basenameUrl = basename(URL::current());
        $data['project'] = Project::all()->count();
        $data['cvstaff'] = CvStaff::all()->count();
        $data['partner'] = Partner::all()->count();
        $data['client'] = Client::all()->count();
        $data['file'] = File::all()->count();
        
        return view('home', compact('basenameUrl','data'));
    }
}
