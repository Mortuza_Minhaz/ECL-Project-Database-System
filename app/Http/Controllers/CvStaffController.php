<?php

namespace App\Http\Controllers;

use App\Models\CvStaff;
use App\Models\CvStaffLanguage;
use App\Models\CvStaffFile;
use App\Models\Staff_Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;


class CvStaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $basenameUrl = basename(URL::current());

        //$cvList = CvStaff::all();

        //$cvList = CvStaff::orderBy('id', 'DESC')->get();

        $category_id = $request->input('category_id');


        $staff_categories = Staff_Category::all();


        if (!empty($category_id)) {

            $cvList = DB::table('cv_staff')
                ->select(
                    'cv_staff.*',
                    'staff_categories.category_name'
                )
                ->join('staff_categories', 'staff_categories.id', '=', 'cv_staff.category_id')
                ->Where('category_id', $category_id)
                ->orderBy('id', 'DESC')
                ->get();
        } else {
            $cvList = DB::table('cv_staff')
                ->select(
                    'cv_staff.*',
                    'staff_categories.category_name'
                )
                ->join('staff_categories', 'staff_categories.id', '=', 'cv_staff.category_id')
                ->orderBy('id', 'DESC')
                ->get();

            // $cvList = DB::select('SELECT * FROM cv_staff');
        }

        // echo '<pre>';
        // print_r($cvList);
        // exit();

        return view('pages/staff_cv/indexCv', compact('basenameUrl', 'cvList', 'staff_categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $basenameUrl = basename(URL::current());

        $staff_categories = Staff_Category::all();

        //$staff_categories = Staff_Category::where('id', 'id')->get();

        return view('pages/staff_cv/createStaffCv', compact('basenameUrl', 'staff_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'category_id' => 'required',
            'staff_name' => 'required',
            'position' => 'required',

        ]);
        // echo '<pre>';
        // print_r($request->all());
        // exit();

        $data = array();

        $data['category_id'] = $request->category_id;
        $data['position'] = $request->position;
        $data['staff_name'] = $request->staff_name;
        $data['birth_date'] = $request->birth_date;
        $data['nationality'] = $request->nationality;
        $data['membership'] = $request->membership;
        $data['qualification'] = $request->qualification;
        $data['experience'] = Carbon::createFromFormat('Y', $request->experience)->year;
        $data['training'] = $request->training;
        $data['country_w_experience'] = $request->country_w_experience;
        $data['employment_record'] = json_encode($request->employment_record, JSON_PRETTY_PRINT);


        $data['created_at'] = Carbon::now();


        $Stdata = DB::table('cv_staff')->insertGetId($data);

        if ($request->has(['group-b']) and $Stdata) {

            foreach ($request['group-b'] as $item) {


                $requestDataSpecialization['cv_staff_id'] = $Stdata;
                $requestDataSpecialization['language'] = $item['language'];
                $requestDataSpecialization['speaking'] = $item['speaking'];
                $requestDataSpecialization['reading'] = $item['reading'];
                $requestDataSpecialization['writing'] = $item['writing'];


                CvStaffLanguage::create($requestDataSpecialization);
            }
        }

        $files = $request->file('cv_file');

        if (isset($files)) {
            $insertArr = array();
            if ($files = $request->file('cv_file')) {
                foreach ($files as $image) {

                    $folder_name = 'cv_files';
                    $images = $this->_uploadImage($image, $folder_name);

                    $insertArr[] = ['cv_staff_id' => $Stdata, 'path' => $images, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()];
                }
            }
            DB::table('cv_staff_files')->insert($insertArr);
        }


        return redirect()->route('staffcv')->with('success', 'Successfully Add');


        // $requestData = $request->all();

        // if (CvStaff::create($requestData)) {
        // return redirect()->route('staffcv')->with('success', 'Successfully Add');
        // }
        // return back()->with('failed', 'Something went wrong. Try again');
    }

    private function _uploadImage($image, $folder_name)
    {
        $name = time() . '-' . $image->getClientOriginalName();
        $image->move('admin/upload/' . $folder_name, $name);
        return '/admin/upload/' . $folder_name . '/' . $name;
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\CvStaff $cvStaff
     * @return \Illuminate\Http\Response
     */
    public function show(CvStaff $cvStaff, $id)
    {

        $basenameUrl = basename(URL::current());
        $cvdata = DB::table('cv_staff')->where('id', $id)->first();


        $project_involve_staff_duties = DB::table('project_involve_staff')
            ->select(
                'project_involve_staff.projects_id',
                'projects.start_date',
                'projects.end_date',
                'clients.client_name as project_client_name',
                'projects.project_name',
                'projects.in_location',
                'projects.project_description',
                'project_involve_staff.duties as project_involve_staff_duties',
                'project_involve_staff.assign_position as project_involve_staff_assign_position'

            )
            ->join('projects', 'projects.id', '=', 'project_involve_staff.projects_id')
            ->join('clients', 'clients.id', '=', 'projects.client_name')
            ->where('project_involve_staff.staff_id', $id)
            ->get();

        $cv_staff_languages = DB::table('cv_staff_languages')->where('cv_staff_id', $id)->get();

        $staffcv_files = DB::table('cv_staff_files')
            ->select('path')
            ->where('cv_staff_id', $id)
            ->get();

        // $pdata = DB::table('projects')->where('id', $id)->first();

        // $project_involve_clients_of_staff = DB::table('clients')
        //     ->select('client_name')
        //     ->whereIn('id', $pdata->client_name)
        //     ->first();



        return view('pages/staff_cv/reportStaffCv', compact('cvdata', 'basenameUrl', 'project_involve_staff_duties', 'cv_staff_languages','staffcv_files'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\CvStaff $cvStaff
     * @return \Illuminate\Http\Response
     */
    public function edit(CvStaff $cvStaff, $id)
    {
        $basenameUrl = basename(URL::current());
        $staff_categories = Staff_Category::all();
        $staff_File = CvStaffFile::where('cv_staff_id', $id)->get();

         // $staff_File = DB::table('cv_staff_files')
         //    ->select('path','cv_staff_id','id')

         //    ->where('cv_staff_id', $cvStaff->id)
         //    ->get();



        $cvData = DB::table('cv_staff')->where('id', $id)->first();

        $cv_staff_languages = DB::table('cv_staff_languages')->where('cv_staff_id', $id)->get();

        return view('pages/staff_cv/editStaffCv2', compact('staff_File', 'basenameUrl', 'staff_categories', 'cvData', 'cv_staff_languages'));
    }


    public function cvStaffFileDelete($cvStaffId, $fileId)
    {


        $cvStaffFile = CvStaffFile::where('id', $fileId)->where('cv_staff_id', $cvStaffId)->first();


        $old_image = $cvStaffFile->path;
        if (File::exists(public_path($old_image))) {

            File::delete(public_path($old_image));
        }
        $cvStaffFile->delete();

        return \Redirect::back()->with('success', 'Successfully Delete');

    }

    public function update(Request $request, CvStaff $cvStaff, $id)
    {

        $singleProject = CvStaff::where('id', $id)->get()->first();

        $data = array();

        $data['category_id'] = $request->category_id;
        $data['position'] = $request->position;
        $data['staff_name'] = $request->staff_name;
        $data['birth_date'] = $request->birth_date;
        $data['nationality'] = $request->nationality;
        $data['membership'] = $request->membership;
        $data['qualification'] = $request->qualification;
        $data['experience'] = $request->experience;
        $data['training'] = $request->training;
        $data['employment_record'] = json_encode($request->employment_record, JSON_PRETTY_PRINT);
        $data['country_w_experience'] = $request->country_w_experience;

        $data['updated_at'] = Carbon::now();
        $singleProject->update($data);


        if ($request->has(['group-b'])) {

            CvStaffLanguage::where('cv_staff_id', $id)->delete();

            foreach ($request['group-b'] as $item) {


                $requestDataSpecialization['cv_staff_id'] = $id;
                $requestDataSpecialization['language'] = $item['language'];
                $requestDataSpecialization['speaking'] = $item['speaking'];
                $requestDataSpecialization['reading'] = $item['reading'];
                $requestDataSpecialization['writing'] = $item['writing'];

                CvStaffLanguage::create($requestDataSpecialization);
            }
        }


        $files = $request->hasFile('cv_file');

        if (isset($files)) {
            $insertArr = array();
            if ($files = $request->file('cv_file')) {
                foreach ($files as $image) {

                    $folder_name = 'cv_files';
                    $images = $this->_uploadImage($image, $folder_name);

                    $insertArr[] = ['cv_staff_id' => $id, 'path' => $images, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()];
                }
            }
            DB::table('cv_staff_files')->insert($insertArr);
        }

        return redirect()->route('staffcv')->with('success', ' CV updated successfully');

    }

    public function staffCvFile($id)
    {
        $basenameUrl = basename(URL::current());
        $staffcv_files = DB::table('cv_staff_files')
            ->select('path')
            ->where('cv_staff_id', $id)
            ->get();

         $cvData = DB::table('cv_staff')->where('id', $id)->first();

         return view('pages/staff_cv/staffCvFile', compact('basenameUrl', 'staffcv_files','cvData'));
    }


    public function destroy(CvStaff $cvStaff, $id)
    {
        
    $cvdata = CvStaff::findOrFail($id);

    $fileGroups = CvStaff::where('id', $cvdata->id)->get();
    foreach ($fileGroups as $fileGroup) {
        DB::table('cv_staff_languages')->where('cv_staff_id', $fileGroup->id)->delete();
        DB::table('cv_staff_files')->where('cv_staff_id', $fileGroup->id)->delete();
    }

    CvStaff::where('id', $cvdata->id)->delete();

    return redirect()->route('staffcv')->with('success', 'CV deleted successfully');
    }
}

