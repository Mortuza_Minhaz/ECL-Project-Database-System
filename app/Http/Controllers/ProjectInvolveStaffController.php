<?php

namespace App\Http\Controllers;

use App\Models\ProjectInvolveStaff;
use Illuminate\Http\Request;

class ProjectInvolveStaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProjectInvolveStaff  $projectInvolveStaff
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectInvolveStaff $projectInvolveStaff)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProjectInvolveStaff  $projectInvolveStaff
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectInvolveStaff $projectInvolveStaff)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProjectInvolveStaff  $projectInvolveStaff
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProjectInvolveStaff $projectInvolveStaff)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProjectInvolveStaff  $projectInvolveStaff
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectInvolveStaff $projectInvolveStaff)
    {
        //
    }
}
