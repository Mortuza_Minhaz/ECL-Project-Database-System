<?php
/**
 * Created by PhpStorm.
 * User: DataHost
 * Date: 6/26/2018
 * Time: 10:44 AM
 */

namespace App\Utility;

use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class Utility
{

    public static function permissionCheck($permission){

        if(auth()->user()->hasPermissionTo($permission))
        {
            return true;
        }

        return false;
    }

    public static function getPermissionMsg(){

        return 'Sorry ! Access Denied for Permission Issue';
    }

}