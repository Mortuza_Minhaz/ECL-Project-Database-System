-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 04, 2020 at 05:04 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project-management`
--

-- --------------------------------------------------------

--
-- Table structure for table `cv_staff`
--

CREATE TABLE `cv_staff` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `consultant_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rfp_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_name` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `staff_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birth_date` date DEFAULT NULL,
  `nationality` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `membership` varchar(600) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qualification` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `experience` date NOT NULL,
  `training` varchar(800) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_w_experience` varchar(350) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employer` varchar(350) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `duration` varchar(350) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employment_position` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `duties` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cv_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cv_staff`
--

INSERT INTO `cv_staff` (`id`, `category_id`, `consultant_name`, `rfp_no`, `client_name`, `position`, `staff_name`, `birth_date`, `nationality`, `membership`, `qualification`, `experience`, `training`, `country_w_experience`, `employer`, `duration`, `employment_position`, `duties`, `cv_file`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL, NULL, 'Senior Structural Engineer', 'Golam Mustafa', '1992-08-19', NULL, NULL, '<p>Masters of Civil Engineering,</p>\r\n\r\n<p>BUET-1997<br />\r\nB.Sc. Engg. (Civil) from University of<br />\r\nRajshahi in the year-1978<br />\r\nM.I.E.B. No: M-11417</p>', '2020-02-17', 'vv', 'vvv', 'vv', 'vv', 'vv', 'vv', 'media/271020_07_27_22.pdf', NULL, NULL),
(4, 3, NULL, NULL, NULL, 'GIS Specialist', 'Mizan', NULL, NULL, NULL, '<p><strong>M.Sc. (Geological Sciences) from<br />\r\nJahangirnagar University in the year 2003</strong><br />\r\n(2 nd class 3 rd )<br />\r\nB.Sc (Hons.- Geological Sciences) from<br />\r\nJahangirnagar University in the year 2002<br />\r\n(2 nd class 6 th )</p>', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-10-25 00:39:00', '2020-10-25 00:39:00'),
(9, 2, NULL, NULL, NULL, 'Senior Architect', 'Radha Gobinda Debnath', NULL, NULL, NULL, '<p>B. Arch from Bangladesh University of<br />\r\nEngineering and Technology (BUET) Dhaka.<br />\r\nSecond Class (upper) in 1997<br />\r\nIAB Membership no. : D-007<br />\r\nRAJUK Registration no. : A-00387</p>', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL),
(10, 7, NULL, NULL, NULL, 'Network &amp; IT Security Specialist', 'Mohammad Aminul Islam Miah', NULL, NULL, NULL, '<p>B.Sc. in Computer Science from North<br />\r\nSouth University in the year 2006</p>', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, 'media/271020_07_43_29.pdf', NULL, NULL),
(11, 2, NULL, NULL, NULL, 'Senior Architect', 'Arch. Shah Alam', NULL, NULL, NULL, '<p>B. Arch. 1971, Bangladesh University of<br />\r\nEngineering &amp;amp; Technology, Dhaka.<br />\r\nM.I.A.B. No: 019</p>', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL),
(12, 3, 'dd', 'dd', NULL, 'dd', 'dd', '2020-10-29', 'dd', 'dd', '<p>dd</p>', '2020-10-20', 'ddsss', 'dd', 'dd', 'dd', 'dd', 'dd', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_10_09_135640_create_permission_tables', 1),
(5, '2020_10_09_135732_create_products_table', 1),
(7, '2020_10_19_052834_create_projects_table', 2),
(12, '2020_10_24_165656_create_staff_categories_table', 5),
(13, '2020_10_19_054819_create_cv_staff_table', 6),
(15, '2020_10_27_041620_add_cv_file_to_cv_staff_table', 8);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 1),
(2, 'App\\Models\\User', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'role-list', 'web', '2020-10-18 03:37:56', '2020-10-18 03:37:56'),
(2, 'role-create', 'web', '2020-10-18 03:37:56', '2020-10-18 03:37:56'),
(3, 'role-edit', 'web', '2020-10-18 03:37:57', '2020-10-18 03:37:57'),
(4, 'role-delete', 'web', '2020-10-18 03:37:57', '2020-10-18 03:37:57'),
(5, 'cv-list', 'web', '2020-10-18 03:37:57', '2020-10-18 03:37:57'),
(6, 'cv-create', 'web', '2020-10-18 03:37:57', '2020-10-18 03:37:57'),
(7, 'cv-edit', 'web', '2020-10-18 03:37:57', '2020-10-18 03:37:57'),
(8, 'cv-delete', 'web', '2020-10-18 03:37:57', '2020-10-18 03:37:57'),
(9, 'project-list', 'web', '2020-10-25 04:55:12', '2020-10-25 04:55:12'),
(10, 'create-project', 'web', '2020-10-25 05:06:48', '2020-10-25 05:06:48'),
(11, 'edit-project', 'web', '2020-10-27 00:25:55', '2020-10-27 00:25:55'),
(12, 'delete-project', 'web', '2020-10-27 00:41:03', '2020-10-27 00:41:03'),
(13, 'cv-category', 'web', '2020-10-27 00:45:22', '2020-10-27 00:45:22'),
(14, 'create-category', 'web', '2020-10-27 00:51:06', '2020-10-27 00:51:06'),
(15, 'delete-category', 'web', '2020-10-27 01:53:29', '2020-10-27 01:53:29');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `project_name` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `in_location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_months` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jv_consultant` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jv_staff_months` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `project_description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_cost` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `service_render` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_tag` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firm_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `assign_staffs` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `n_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `project_name`, `country`, `in_location`, `client_name`, `staff_months`, `jv_consultant`, `jv_staff_months`, `project_description`, `project_cost`, `start_date`, `end_date`, `service_render`, `project_tag`, `remarks`, `firm_name`, `assign_staffs`, `staff_id`, `n_file`, `created_at`, `updated_at`) VALUES
(1, '1212', 'Algeria', 'dfhj', 'tuguk', NULL, 'drhdykt', NULL, '<p>12</p>', '108.00 Crore', '2020-10-28 05:03:50', '2012-11-28 05:03:50', '12', NULL, '12', '12', '12', NULL, '', NULL, NULL),
(2, 'new', 'Bangladesh', 'dhk', 'ami', NULL, 'N/A', NULL, '<p>ffff</p>', '108.00 Crore', '2012-05-28 06:57:13', '2012-12-28 06:57:13', 'dgtdh', NULL, '6', 'eng', '34', NULL, '', NULL, NULL),
(3, 'new upd 2', 'Angola', 'Khulna , 10 spot', 'ami', NULL, 'jbdjfgb', NULL, '<p>ffffhhhh</p>\r\n\r\n<p></p>\r\n\r\n<p>mmm</p>', '108.00 Crore', '2012-10-30 02:53:52', '2012-10-30 02:53:52', 'dgtdh', NULL, '6', 'eng', '34', NULL, '', NULL, NULL),
(4, 'dte', 'Bangladesh', 'ge', 'ege', NULL, 'ergye', NULL, '<p>egte</p>', 'ege', '2012-08-30 17:50:16', '2012-04-30 17:50:16', 'erhge', NULL, 'ergh', 'ergh', 'ergh', NULL, '', NULL, NULL),
(5, 'vbfd', 'Bangladesh', 'dfght', 'fht', NULL, NULL, NULL, '<p>dfhrt</p>', 'dhrjn', '2012-09-30 18:01:21', '2013-03-02 18:01:21', 'dhrth', NULL, 'fgbd', 'dht', 'dfh', NULL, '', NULL, NULL),
(6, 'hnymjn', 'Bangladesh', 'yh6u', 'ju57', NULL, '6uj56', NULL, '<p>6rij57</p>', '6uh5i', '2012-08-30 18:03:17', '2020-10-30 18:03:17', '6uj57i', NULL, 'y6u', '6u56i', '6uj56', NULL, '', NULL, NULL),
(7, 'dsfdf', 'Bangladesh', 'dffwe', 'dfwe', NULL, 'dfwe', NULL, '<p>wefdwef</p>', 'efew', '2020-10-31 21:24:13', '2020-10-31 21:24:13', 'ewfewf', NULL, 'edef', 'efeqf', 'efef', NULL, '', NULL, NULL),
(8, 'ddd', 'Bangladesh', 'ddd', 'ddd', NULL, 'ddd', NULL, '<p>ddd</p>', 'dddd', '2020-10-31 23:13:17', '2020-10-31 23:13:17', 'ddd', NULL, 'dd', 'dd', 'dd,dd,ss', NULL, '', NULL, NULL),
(9, 'ss', 'Bangladesh', 'ss', 'ss', NULL, 'ss', NULL, '<p>ss</p>', 'ss', '2020-10-31 23:13:07', '2020-10-31 23:13:07', 'ss', NULL, 'ss', 'ss', 'ss,ss,ss,ff,ff,ff', NULL, '', NULL, NULL),
(10, 'new', 'Bangladesh', 'dd', 'dd', NULL, 'dd', NULL, '<p>dd</p>', 'dd', '2020-07-31 23:12:58', '2020-11-30 23:12:58', 'dd', NULL, '12', 'hjr', 'eng,senio,gdhtd,arch,juni,vfhjgk', NULL, '', NULL, NULL),
(11, 'ss d', 'Åland Islands', 'ssd', 'ssd', 'ssd', 'dd', 'ssd', '<p>ssd</p>', 'ss', '2010-02-01 23:45:31', '2021-03-01 23:45:31', 'ss d', NULL, 'ssd', 'ssd', NULL, NULL, '', NULL, NULL),
(12, 'ff', 'Bangladesh', 'ff', 'ff', 'ff', 'ff', 'ff', '<p>fff</p>', '108.00 Crore', '2020-11-02 22:03:21', '2020-11-02 22:03:21', 'ff', 'dd,fghf', 'xx', 'dd', NULL, NULL, '', NULL, NULL),
(13, 'Establishment of two self contained 10 kilowatt F.M Radio Stations at Mymensingh & Gopalganj', 'Bangladesh', 'Mymensingh and Gopalgonj.', 'Ministry of Information, Bangladesh Betar, Dhaka', '75 man months.', 'n/a', 'n/a', '<p>The project comprised of:</p>\r\n\r\n<p>a.&nbsp;&nbsp; &nbsp; To renovate an Auditorium with acoustics facility, interior decoration &amp; all other required&nbsp;systems at Mymensingh only.</p>\r\n\r\n<p>b.&nbsp;&nbsp; &nbsp;Supply, installation, testing and commissioning of 2x150 KVA Electrical Sub-station with of incoming feeder (11Kv), 2x80 KVA Diesel Generator, 2 nos. Solar panels system, Fire alarming, fire detection and fire protection systems, 2 nos. self supported towers of 250 feet height, 2 nos. Networking system among the officers of Bangladesh Betarnos. Satellite Down linking system of Radio Programmes at (Total two (i) One at Mymensingh and (ii) One at Gopalgonj.</p>\r\n\r\n<p>c.&nbsp;&nbsp; &nbsp;Supply, installation, testing and commissioning of 4x50 Ton capacity central Air&nbsp;Conditioning Plants (Total four (i) Two at Mymensingh and (ii) Two at Gopalgonj.</p>\r\n\r\n<p>d.&nbsp;&nbsp; &nbsp;Construction of boundary wall, carpeting road &amp; Land Development and other ancillary&nbsp;works.</p>', '108.00 Crore', '2015-08-03 09:24:22', '2019-06-03 09:24:22', 'Conduct Site survey, Site Analysis  and preparation of sub-soil investigation Report, preparation of architectural design and drawings including necessary detailing thereof, preparation of structural design and drawings, preparation of electrical & mechanical design and drawings, preparation of estimated cost including schedule of items and bill of quantities, preparation of tender documents, full time supervision of works related to project components mentioned above, taking measurement, bill preparation and certify bills, preparation of as built drawings in scale fixed by the client..', 'df,as', NULL, 'Engineers Consortium Ltd.', 'fdadfad', NULL, '', NULL, NULL),
(14, 'vv', 'Bangladesh', 'vv', 'vv', 'vv', 'vv', 'vv', '<p>vv</p>', '107.00 Crore', '2020-11-03 09:15:29', '2020-11-03 09:15:29', 'vv', '1 , 2 , 3 , 4', 'tt', 'ttt', NULL, NULL, '', NULL, NULL),
(15, 'vv', 'Bangladesh', 'vv', 'vv', 'vv', NULL, 'vv', '<p>vv</p>', '107.00 Crore', '2020-11-03 09:23:15', '2020-11-03 09:23:15', 'vv', '1s , 2d , 3d , 4d , 5d', 'tt', 'ttt', NULL, NULL, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'web', '2020-10-18 03:38:13', '2020-10-18 03:38:13'),
(2, 'user', 'web', '2020-10-18 03:39:47', '2020-10-18 03:39:47');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(6, 2),
(7, 1),
(8, 1),
(9, 1),
(9, 2),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1);

-- --------------------------------------------------------

--
-- Table structure for table `staff_categories`
--

CREATE TABLE `staff_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff_categories`
--

INSERT INTO `staff_categories` (`id`, `category_name`, `created_at`, `updated_at`) VALUES
(1, 'Engineer', NULL, NULL),
(2, 'Architect', NULL, NULL),
(3, 'Geologist', '2020-10-25 04:05:28', '2020-10-25 04:05:28'),
(6, 'Not Defined', '2020-10-31 07:09:31', '2020-10-31 07:09:31'),
(7, 'ICT Specialist', '2020-10-31 07:14:30', '2020-10-31 07:14:30'),
(8, 'mm , nn , gg', NULL, NULL),
(9, 'mm , nn , gg', NULL, NULL),
(10, '1 , 2 , 3 , 4', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@gmail.com', NULL, '$2y$10$2/BrfTZSE9DcY/AUZu0lcu6kgqlEQi0p/p4MJgF.k0D2DVa9LO27a', '3QTcGbegBXDTzL1QgG8tfnW4DaEEcpcj1glzdYxn4MSngUg0LwEYkohdIaH4', '2020-10-18 03:38:13', '2020-10-18 03:38:13'),
(2, 'minhaz', 'minhaz@gmail.com', NULL, '$2y$10$2/BrfTZSE9DcY/AUZu0lcu6kgqlEQi0p/p4MJgF.k0D2DVa9LO27a', NULL, '2020-10-18 03:44:18', '2020-10-18 03:44:18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cv_staff`
--
ALTER TABLE `cv_staff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `staff_categories`
--
ALTER TABLE `staff_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cv_staff`
--
ALTER TABLE `cv_staff`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `staff_categories`
--
ALTER TABLE `staff_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
