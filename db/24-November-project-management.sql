-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 24, 2020 at 05:14 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project-management`
--

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `client_name`, `client_address`, `created_at`, `updated_at`) VALUES
(1, 'Client 2', 'Address 2', '2020-11-14 21:25:53', '2020-11-14 21:56:51'),
(2, 'Client 1', 'Address hdjd', '2020-11-14 21:56:32', '2020-11-23 22:06:13'),
(3, 'New Client', 'nnn', '2020-11-16 23:43:13', '2020-11-16 23:43:13');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, 93),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, 355),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, 213),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, 1684),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, 376),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, 244),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, 1264),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, 0),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, 1268),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, 54),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, 374),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, 297),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, 61),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, 43),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, 994),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, 1242),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, 973),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, 880),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, 1246),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, 375),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, 32),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, 501),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, 229),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, 1441),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, 975),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, 591),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, 387),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, 267),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, 0),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, 55),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, 246),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, 673),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, 359),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, 226),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, 257),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, 855),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, 237),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, 1),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, 238),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, 1345),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, 236),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, 235),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, 56),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, 86),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, 61),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, 672),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, 57),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, 269),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, 242),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, 242),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, 682),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, 506),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, 225),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, 385),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, 53),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, 357),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, 420),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, 45),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, 253),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, 1767),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, 1809),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, 593),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, 20),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, 503),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, 240),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, 291),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, 372),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, 251),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, 500),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, 298),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, 679),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, 358),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, 33),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, 594),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, 689),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, 0),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, 241),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, 220),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, 995),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, 49),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, 233),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, 350),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, 30),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, 299),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, 1473),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, 590),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, 1671),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, 502),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, 224),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, 245),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, 592),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, 509),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, 0),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, 39),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, 504),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, 852),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, 36),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, 354),
(99, 'IN', 'INDIA', 'India', 'IND', 356, 91),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, 62),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, 98),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, 964),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, 353),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, 972),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, 39),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, 1876),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, 81),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, 962),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, 7),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, 254),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, 686),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, 850),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, 82),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, 965),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, 996),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, 856),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, 371),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, 961),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, 266),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, 231),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, 218),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, 423),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, 370),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, 352),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, 853),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, 389),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, 261),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, 265),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, 60),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, 960),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, 223),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, 356),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, 692),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, 596),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, 222),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, 230),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, 269),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, 52),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, 691),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, 373),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, 377),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, 976),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, 1664),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, 212),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, 258),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, 95),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, 264),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, 674),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, 977),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, 31),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, 599),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, 687),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, 64),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, 505),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, 227),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, 234),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, 683),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, 672),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, 1670),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, 47),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, 968),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, 92),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, 680),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, 970),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, 507),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, 675),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, 595),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, 51),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, 63),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, 0),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, 48),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, 351),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, 1787),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, 974),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, 262),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, 40),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, 70),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, 250),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, 290),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, 1869),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, 1758),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, 508),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, 1784),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, 684),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, 378),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, 239),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, 966),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, 221),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, 381),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, 248),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, 232),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, 65),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, 421),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, 386),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, 677),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, 252),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, 27),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, 0),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, 34),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, 94),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, 249),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, 597),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, 47),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, 268),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, 46),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, 41),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, 963),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, 886),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, 992),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, 255),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, 66),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, 670),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, 228),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, 690),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, 676),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, 1868),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, 216),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, 90),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, 7370),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, 1649),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, 688),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, 256),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, 380),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, 971),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, 44),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, 1),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, 1),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, 598),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, 998),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, 678),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, 58),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, 84),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, 1284),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, 1340),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, 681),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, 212),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, 967),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, 260),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, 263);

-- --------------------------------------------------------

--
-- Table structure for table `cv_staff`
--

CREATE TABLE `cv_staff` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `consultant_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rfp_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_name` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `staff_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birth_date` date DEFAULT NULL,
  `nationality` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `membership` varchar(600) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qualification` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `experience` year(4) DEFAULT NULL,
  `training` varchar(800) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bengali` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `english` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `country_w_experience` varchar(350) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employment_record` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `cv_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cv_staff`
--

INSERT INTO `cv_staff` (`id`, `category_id`, `consultant_name`, `rfp_no`, `client_name`, `position`, `staff_name`, `birth_date`, `nationality`, `membership`, `qualification`, `experience`, `training`, `bengali`, `english`, `country_w_experience`, `employment_record`, `cv_file`, `created_at`, `updated_at`) VALUES
(1, 11, 'Engineers Consortium Ltd.', '33.01.0000.835.11.004.20-96, Dated: 05/07/2020', 'Project Director, Establishment of Institute of Livestock Science and Technology project in Sylhet, Lalmonirhat/Kurigram and Barishal, Krishi Khamar Sarak, Farmgate, Dhaka-1215', 'COST ESTIMATOR', 'S M KHAZA ALAUDDIN', '1965-12-17', 'Bangladeshi by birth', 'not available', '<p>Dip-in-Engg. (Civil), from Engineering and Survey Institute, Rajshahi in the year 1989</p>', 1989, 'not available', '{\n    \"speaking\": \"Mother Tongue\",\n    \"reading\": \"Excellent\",\n    \"writing\": \"Excellent\"\n}', '{\n    \"speaking\": \"Excellent\",\n    \"reading\": \"Excellent\",\n    \"writing\": \"Excellent\"\n}', 'Bangladesh', '{\r\n    \"employer\": [\r\n        \"Eng Consort\",\r\n        \"Eng Consort 2\"\r\n    ],\r\n    \"duration\": [\r\n        \"From Jan to till\",\r\n        \"From Jan to till 2\"\r\n    ],\r\n    \"employment_position\": [\r\n        \"Leader\",\r\n        \"Manager\"\r\n    ],\r\n    \"duties\": [\r\n        \"Duty 1\",\r\n        \"Duty 2\"\r\n    ]\r\n}', '', '2020-11-09 22:30:05', NULL),
(2, 2, 'Engineers Consortium Ltd.', '33.01.0000.835.11.004.20-96, Dated: 05/07/2020', 'Project Director, Establishment of Institute of Livestock Science and Technology project in Sylhet, Lalmonirhat/Kurigram and Barishal, Krishi Khamar Sarak, Farmgate, Dhaka-1215', 'Senior Architect', 'Arch. Shah Alam', '1980-02-05', 'Bangladeshi by birth', 'n/a', '<p style=\"margin:0cm\">B. Arch. 1971, Bangladesh University of Engineering &amp; Technology, Dhaka. M.I.A.B. No: 019</p>', 1971, 'n/a', '{\n    \"speaking\": \"Mother Tongue\",\n    \"reading\": \"Excellent\",\n    \"writing\": \"Excellent\"\n}', '{\n    \"speaking\": \"Good\",\n    \"reading\": \"Good\",\n    \"writing\": \"Good\"\n}', 'Bangladesh', '{\r\n    \"employer\": [\r\n        \"Eng Consort\"\r\n    ],\r\n    \"duration\": [\r\n        \"From Jan to till\"\r\n    ],\r\n    \"employment_position\": [\r\n        \"Leader\"\r\n    ],\r\n    \"duties\": [\r\n        \"Duty 1\"\r\n\r\n    ]\r\n}', '', '2020-11-09 23:02:05', NULL),
(3, 1, 'Engineers Consortium Ltd.', '33.01.0000.835.11.004.20-96, Dated: 05/07/2020', 'Establishment of Institute of Livestock Science and Technology project in Sylhet, Lalmonirhat/Kurigram and Barishal, Krishi Khamar Sarak, Farmgate, Dhaka-1215', 'Construction Engineer', 'Md Mustafizur Rahman', '1965-07-21', 'Bangladeshi by birth', NULL, '<p style=\"text-align:justify; margin:0cm\"><span style=\"font-size:12pt\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\">B.Sc. Engg. (Civil) in BUET, Dhaka- 1984</span></span></p>\r\n\r\n<p><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Tahoma&quot;,sans-serif\"><span lang=\"EN-US\" style=\"font-size:12.0pt\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\">M.I.E.B. No.- 3593</span></span></span></span></p>', 1984, NULL, '{\n    \"speaking\": \"Mother Tongue\",\n    \"reading\": \"Excellent\",\n    \"writing\": \"Excellent\"\n}', '{\n    \"speaking\": \"Medium\",\n    \"reading\": \"Medium\",\n    \"writing\": \"Medium\"\n}', 'Bangladesh', '{\r\n    \"employer\": [\r\n        \"Eng Consort\"\r\n    ],\r\n    \"duration\": [\r\n        \"From Jan to till\"\r\n    ],\r\n    \"employment_position\": [\r\n        \"Leader\"\r\n    ],\r\n    \"duties\": [\r\n        \"Duty 1\"\r\n\r\n    ]\r\n}', '', '2020-11-11 01:19:00', '2020-11-11 01:20:35'),
(4, 2, 'Engineers Consortium Ltd.', '33.01.0000.835.11.004.20-96, Dated: 05/07/2020', 'Establishment of Institute of Livestock Science and Technology project in Sylhet, Lalmonirhat/Kurigram and Barishal, Krishi Khamar Sarak, Farmgate, Dhaka-1215', 'Architect', 'Tangila Khatun', '1982-07-15', 'Bangladeshi by birth', NULL, '<p style=\"margin:0cm\"><span style=\"font-size:12pt\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\">B. Arch. from Ashanulla University, Dhaka in the year-2004</span></span></p>\r\n\r\n<p><span lang=\"EN-US\" style=\"font-size:12.0pt\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\">M.I.A.B. No: K-116</span></span></p>', 2004, NULL, '{\n    \"speaking\": \"Mother Tongue\",\n    \"reading\": \"Excellent\",\n    \"writing\": \"excllnt\"\n}', '{\n    \"speaking\": \"Good\",\n    \"reading\": \"Good\",\n    \"writing\": \"Good\"\n}', 'Bangladesh', '{\r\n    \"employer\": [\r\n        \"Eng Consort\"\r\n    ],\r\n    \"duration\": [\r\n        \"From Jan to till\"\r\n    ],\r\n    \"employment_position\": [\r\n        \"Leader\"\r\n    ],\r\n    \"duties\": [\r\n        \"Duty 1\"\r\n\r\n    ]\r\n}', '', '2020-11-11 01:27:08', NULL),
(5, 7, 'Engineers Consortium Ltd.', '33.01.0000.835.11.004.20-96, Dated: 05/07/2020', 'Establishment of Institute of Livestock Science and Technology project in Sylhet, Lalmonirhat/Kurigram and Barishal, Krishi Khamar Sarak, Farmgate, Dhaka-1215', 'Senior Software Engineer', 'Md. Rezwanur Rahman (Rasel)', '1985-07-25', 'Bangladeshi by birth', NULL, '<p><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Arial&quot;,sans-serif\">Bachelor of Science (B.Sc) in Computer Science and Information Technology (CIT) from Islamic University of Technology (IUT) in the year 2008</span></span></p>', 2008, NULL, '{\n    \"speaking\": \"Mother Tongue\",\n    \"reading\": \"Excellent\",\n    \"writing\": \"Excellent\"\n}', '{\n    \"speaking\": \"Medium\",\n    \"reading\": \"Medium\",\n    \"writing\": \"Medium\"\n}', 'Bangladesh', '{\r\n    \"employer\": [\r\n        \"Eng Consort\"\r\n    ],\r\n    \"duration\": [\r\n        \"From Jan to till\"\r\n    ],\r\n    \"employment_position\": [\r\n        \"Leader\"\r\n    ],\r\n    \"duties\": [\r\n        \"Duty 1\"\r\n\r\n    ]\r\n}', '', '2020-11-11 01:48:28', NULL),
(6, 3, 'Engineers Consortium Ltd.', '33.01.0000.835.11.004.20-96, Dated: 05/07/2020', 'Establishment of Institute of Livestock Science and Technology project in Sylhet, Lalmonirhat/Kurigram and Barishal, Krishi Khamar Sarak, Farmgate, Dhaka-1215', 'Geologist', 'Md. Arifuzzaman Raju', '1980-06-11', 'Bangladeshi by birth', NULL, '<p style=\"text-align:justify; margin:0cm\"><span style=\"font-size:12pt\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\"><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Arial&quot;,sans-serif\">M.Sc. (Geological Sciences) from Jahangirnagar University in the year 2003 (2<sup>nd</sup> class 3<sup>rd</sup>) </span></span></span></span></p>\r\n\r\n<p><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Arial&quot;,sans-serif\">B.Sc (Hons.- Geological Sciences) from Jahangirnagar University in the year 2002 (2<sup>nd</sup> class 6<sup>th</sup>)</span></span></p>', 2002, NULL, '{\n    \"speaking\": \"Mother Tongue\",\n    \"reading\": \"Excellent\",\n    \"writing\": \"Excellent\"\n}', '{\n    \"speaking\": \"Excellent\",\n    \"reading\": \"Excellent\",\n    \"writing\": \"Excellent\"\n}', 'Bangladesh', '{\r\n    \"employer\": [\r\n        \"Eng Consort\"\r\n    ],\r\n    \"duration\": [\r\n        \"From Jan to till\"\r\n    ],\r\n    \"employment_position\": [\r\n        \"Leader\"\r\n    ],\r\n    \"duties\": [\r\n        \"Duty 1\"\r\n\r\n    ]\r\n}', '', '2020-11-11 01:51:09', NULL),
(7, 2, 'Engineers Consortium Ltd.', '33.01.0000.835.11.004.20-96, Dated: 05/07/2020', 'Project Director, Establishment of Institute of Livestock Science and Technology project in Sylhet, Lalmonirhat/Kurigram and Barishal, Krishi Khamar Sarak, Farmgate, Dhaka-1215', 'Architect', 'Rezwan Sobhan', '1979-07-11', 'Bangladeshi by birth', 'IAB', '<p><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Arial&quot;,sans-serif\">Bachelor of Architect </span></span><span lang=\"EN\" style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Arial&quot;,sans-serif\">from Khulna University, Bangladesh in the year- 2001</span></span></p>', 2001, NULL, '{\n    \"speaking\": \"Mother Tongue\",\n    \"reading\": \"Excellent\",\n    \"writing\": \"Excellent\"\n}', '{\n    \"speaking\": \"Excellent\",\n    \"reading\": \"Excellent\",\n    \"writing\": \"Excellent\"\n}', 'Bangladesh', '{\r\n    \"employer\": [\r\n        \"Eng Consort\"\r\n    ],\r\n    \"duration\": [\r\n        \"From Jan to till\"\r\n    ],\r\n    \"employment_position\": [\r\n        \"Leader\"\r\n    ],\r\n    \"duties\": [\r\n        \"Duty 1\"\r\n\r\n    ]\r\n}', '', '2020-11-11 03:27:14', NULL),
(8, 12, 'Engineers Consortium Ltd.', '.....', 'Establishment of Institute of Livestock Science and Technology project in Sylhet, Lalmonirhat/Kurigram and Barishal, Krishi Khamar Sarak, Farmgate, Dhaka-1215', 'Supply Eng', 'Engr. Dewan Nuruzzaman', '1980-06-17', 'Bangladeshi by birth', NULL, '<p style=\"margin:0cm\"><span style=\"font-size:12pt\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\">Bachelor of Science of Engineering (Civil), BUET, Dhaka, in the year- 1979</span></span></p>\r\n\r\n<p style=\"margin:0cm\"><span style=\"font-size:12pt\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\">F.I.E.B-8889</span></span></p>\r\n\r\n<p style=\"text-align:justify; margin:0cm\"><span style=\"font-size:12pt\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\">M.I.E.B. No: 7116 </span></span></p>\r\n\r\n<p><span lang=\"EN-US\" style=\"font-size:12.0pt\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\">RAJUK Enlistment No. DMINB/CE-0219</span></span></p>', 1979, NULL, '{\n    \"speaking\": \"Mother Tongue\",\n    \"reading\": \"Excellent\",\n    \"writing\": \"Excellent\"\n}', '{\n    \"speaking\": \"Excellent\",\n    \"reading\": \"Excellent\",\n    \"writing\": \"Excellent\"\n}', 'Bangladesh', '{\n    \"employer\": [\n        \"Engineers Consortium\",\n        \"AEL\"\n    ],\n    \"duration\": [\n        \"From Jan to Dec\",\n        \"From September to Dec\"\n    ],\n    \"employment_position\": [\n        \"Manager\",\n        \"Team Leader\"\n    ],\n    \"duties\": [\n        \"duty as manager\",\n        \"Duty as Team Leader\"\n    ]\n}', '', '2020-11-12 00:38:13', NULL),
(10, 1, 'lang test', 'lang test', 'lang test', 'lang test', 'lang test', '2014-06-10', 'lang test', 'lang test', '<p>lang test</p>', 2015, 'lang test', 'null', 'null', 'Bangladse', '{\n    \"employer\": [\n        \"Engineers Consortium\"\n    ],\n    \"duration\": [\n        \"From Jan to Dec\"\n    ],\n    \"employment_position\": [\n        \"Manager\"\n    ],\n    \"duties\": [\n        \"Good\"\n    ]\n}', '', '2020-11-16 00:17:36', NULL),
(11, 3, 'year test', 'year test', 'year test', 'year test', 'year test', '2020-11-16', 'year test', 'year test', '<p>year test</p>', 1990, 'year test', 'null', 'null', 'year test', '{\n    \"employer\": [\n        \"year test\"\n    ],\n    \"duration\": [\n        \"year test\"\n    ],\n    \"employment_position\": [\n        \"year test\"\n    ],\n    \"duties\": [\n        \"year test\"\n    ]\n}', '', '2020-11-16 04:35:48', NULL),
(12, 2, 'test', 'test', 'test', 'test', 'test', '2020-11-18', 'test', 'test', '<p>test</p>', 2003, NULL, 'null', 'null', 'bd', '{\n    \"employer\": [\n        null\n    ],\n    \"duration\": [\n        null\n    ],\n    \"employment_position\": [\n        null\n    ],\n    \"duties\": [\n        null\n    ]\n}', '', '2020-11-18 01:58:57', NULL),
(19, 7, 'staff file test', 'staff file test', 'staff file test', 'staff file test', 'staff file test', '2015-11-19', 'staff file test', 'staff file test', '<p>staff file test</p>', 2020, 'staff file test', 'null', 'null', NULL, '{\n    \"employer\": [\n        null\n    ],\n    \"duration\": [\n        null\n    ],\n    \"employment_position\": [\n        null\n    ],\n    \"duties\": [\n        null\n    ]\n}', NULL, '2020-11-19 03:55:47', NULL),
(20, 2, 'Engineers Consortium Ltd.', 'edit test', 'edit test', 'edit test', 'edit test', '2020-11-22', 'edit test', 'edit test', '<p>edit test</p>', 2002, NULL, 'null', 'null', 'edit test', '{\n    \"employer\": [\n        \"Engineers Consortium\",\n        \"ECK\",\n        \"AEL\"\n    ],\n    \"duration\": [\n        \"From Jan to Dec\",\n        \"From Jan to Dec\",\n        \"Medium\"\n    ],\n    \"employment_position\": [\n        \"Manager\",\n        \"Edit\",\n        \"Medium\"\n    ],\n    \"duties\": [\n        \"Medium\",\n        \"Medium\",\n        \"Medium\"\n    ]\n}', NULL, '2020-11-21 22:13:03', NULL),
(22, 2, NULL, NULL, NULL, 'Senior Architect', 'S M Khaza', '2020-11-22', NULL, NULL, NULL, 2020, NULL, 'null', 'null', NULL, '{\n    \"employer\": [\n        null\n    ],\n    \"duration\": [\n        null\n    ],\n    \"employment_position\": [\n        null\n    ],\n    \"duties\": [\n        null\n    ]\n}', NULL, '2020-11-21 23:58:12', '2020-11-21 23:58:34'),
(23, 2, NULL, NULL, NULL, 'Senior Architect', 'min', '1992-08-20', NULL, NULL, '<p>test</p>', 2015, NULL, 'null', 'null', NULL, '{\n    \"employer\": [\n        null\n    ],\n    \"duration\": [\n        null\n    ],\n    \"employment_position\": [\n        null\n    ],\n    \"duties\": [\n        null\n    ]\n}', NULL, '2020-11-22 04:43:33', NULL),
(24, 1, 'aaa', 'aaa', 'aaa', 'aaa', 'aaa', '2017-01-31', 'aaa', 'aaa', '<p>aaa</p>', 2013, 'aa', '', '', 'aaa', '{\n    \"employer\": [\n        \"aa\"\n    ],\n    \"duration\": [\n        \"aa\"\n    ],\n    \"employment_position\": [\n        \"aa\"\n    ],\n    \"duties\": [\n        \"aa\"\n    ]\n}', NULL, '2020-11-22 06:57:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cv_staff_files`
--

CREATE TABLE `cv_staff_files` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cv_staff_id` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cv_staff_files`
--

INSERT INTO `cv_staff_files` (`id`, `cv_staff_id`, `path`, `created_at`, `updated_at`) VALUES
(1, 19, '/admin/upload/cv_files/1605779747-create.rar', '2020-11-19 03:55:47', '2020-11-19 03:55:47'),
(2, 19, '/admin/upload/cv_files/1605779747-cv.pdf', '2020-11-19 03:55:47', '2020-11-19 03:55:47'),
(3, 19, '/admin/upload/cv_files/1605779747-download.jpg', '2020-11-19 03:55:47', '2020-11-19 03:55:47'),
(4, 19, '/admin/upload/cv_files/1605779747-Partner Form.docx', '2020-11-19 03:55:47', '2020-11-19 03:55:47'),
(5, 19, '/admin/upload/cv_files/1605779747-SampleVideo_1280x720_1mb.flv', '2020-11-19 03:55:47', '2020-11-19 03:55:47'),
(6, 20, '/admin/upload/cv_files/1606018383-clients.sql', '2020-11-21 22:13:03', '2020-11-21 22:13:03'),
(7, 20, '/admin/upload/cv_files/1606018383-clients.zip', '2020-11-21 22:13:03', '2020-11-21 22:13:03'),
(8, 20, '/admin/upload/cv_files/1606018383-controller.txt', '2020-11-21 22:13:03', '2020-11-21 22:13:03'),
(9, 20, '/admin/upload/cv_files/1606018383-cv.pdf', '2020-11-21 22:13:03', '2020-11-21 22:13:03'),
(10, 20, '/admin/upload/cv_files/1606018383-download.jpg', '2020-11-21 22:13:03', '2020-11-21 22:13:03'),
(11, 20, '/admin/upload/cv_files/1606018383-Partner Form.docx', '2020-11-21 22:13:03', '2020-11-21 22:13:03'),
(12, 20, '/admin/upload/cv_files/1606018383-SampleVideo_1280x720_1mb.flv', '2020-11-21 22:13:03', '2020-11-21 22:13:03'),
(14, 24, '/admin/upload/cv_files/1606049834-clients.zip', '2020-11-22 06:57:14', '2020-11-22 06:57:14'),
(15, 24, '/admin/upload/cv_files/1606049834-cv.pdf', '2020-11-22 06:57:14', '2020-11-22 06:57:14'),
(16, 24, '/admin/upload/cv_files/1606049834-download.jpg', '2020-11-22 06:57:14', '2020-11-22 06:57:14'),
(17, 24, '/admin/upload/cv_files/1606049834-laravel_project-management.sql', '2020-11-22 06:57:14', '2020-11-22 06:57:14'),
(19, 24, '/admin/upload/cv_files/1606049834-SampleVideo_1280x720_1mb.flv', '2020-11-22 06:57:14', '2020-11-22 06:57:14');

-- --------------------------------------------------------

--
-- Table structure for table `cv_staff_languages`
--

CREATE TABLE `cv_staff_languages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cv_staff_id` int(11) DEFAULT NULL,
  `language` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `speaking` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reading` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `writing` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cv_staff_languages`
--

INSERT INTO `cv_staff_languages` (`id`, `cv_staff_id`, `language`, `speaking`, `reading`, `writing`, `created_at`, `updated_at`) VALUES
(1, 10, 'Bangla', 'Mother Toungue', 'Excellent', 'Excellent', '2020-11-16 00:17:36', '2020-11-16 00:17:36'),
(2, 10, 'English', 'Good', 'Good', 'Good', '2020-11-16 00:17:36', '2020-11-16 00:17:36'),
(3, 11, 'year test', 'year test', 'year test', 'year test', '2020-11-16 04:35:48', '2020-11-16 04:35:48'),
(4, 12, 'Bangla', 'Mother Toungue', 'Excellent', 'Excellent', '2020-11-18 01:58:57', '2020-11-18 01:58:57'),
(5, 12, 'English', 'Good', 'Good', 'Good', '2020-11-18 01:58:57', '2020-11-18 01:58:57'),
(6, 13, 'English11', 'Good11', 'Good', 'Good', '2020-11-18 02:03:06', '2020-11-18 02:03:06'),
(7, 16, NULL, NULL, NULL, NULL, '2020-11-19 03:52:31', '2020-11-19 03:52:31'),
(8, 17, NULL, NULL, NULL, NULL, '2020-11-19 03:53:39', '2020-11-19 03:53:39'),
(9, 18, NULL, NULL, NULL, NULL, '2020-11-19 03:55:18', '2020-11-19 03:55:18'),
(10, 19, NULL, NULL, NULL, NULL, '2020-11-19 03:55:47', '2020-11-19 03:55:47'),
(11, 20, 'Bangla', 'Mother Toungue', 'Excellent', 'Excellent', '2020-11-21 22:13:03', '2020-11-21 22:13:03'),
(12, 20, 'English', 'Good', 'Good', 'Good', '2020-11-21 22:13:03', '2020-11-21 22:13:03'),
(13, 20, 'Arabic', 'Medium', 'Medium', 'Medium', '2020-11-21 22:13:03', '2020-11-21 22:13:03'),
(14, 21, NULL, NULL, NULL, NULL, '2020-11-21 23:56:34', '2020-11-21 23:56:34'),
(15, 22, NULL, NULL, NULL, NULL, '2020-11-21 23:58:12', '2020-11-21 23:58:12'),
(16, 23, NULL, NULL, NULL, NULL, '2020-11-22 04:43:33', '2020-11-22 04:43:33'),
(17, 24, NULL, NULL, NULL, NULL, '2020-11-22 06:57:14', '2020-11-22 06:57:14');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `exp_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `file_name`, `description`, `start_date`, `exp_date`, `created_at`, `updated_at`) VALUES
(2, '2nd', 'Multi file', '2020-11-16', '2019-11-16', '2020-11-16 08:39:08', '2020-11-16 08:39:08'),
(3, 'doc', 'mult', '2020-11-17', '2021-01-12', '2020-11-17 12:18:28', '2020-11-17 12:18:28'),
(4, 'zip', 'Multi file', '2020-11-18', '2021-03-19', '2020-11-18 03:29:43', '2020-11-18 03:29:43'),
(5, 'vdeo', 'Multi file', '2020-11-18', '2021-06-24', '2020-11-18 04:06:53', '2020-11-18 04:06:53'),
(6, 'vdeo', 'Multi file', '2020-11-18', '2020-12-18', '2020-11-18 04:36:26', '2020-11-18 04:36:26'),
(7, 'Extension Test', 'Multi file', '2020-11-18', '2020-10-12', '2020-11-18 04:57:56', '2020-11-18 04:57:56'),
(8, 'date', 'Multi file', '2010-02-02', '2021-03-04', '2020-11-18 21:55:36', '2020-11-18 21:55:36');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_10_09_135640_create_permission_tables', 1),
(5, '2020_10_09_135732_create_products_table', 1),
(7, '2020_10_19_052834_create_projects_table', 2),
(12, '2020_10_24_165656_create_staff_categories_table', 5),
(13, '2020_10_19_054819_create_cv_staff_table', 6),
(15, '2020_10_27_041620_add_cv_file_to_cv_staff_table', 8),
(17, '2020_11_08_040222_create_partners_table', 10),
(18, '2020_11_08_054118_create_project_involve_staff_table', 11),
(20, '2020_11_14_121645_create_files_table', 12),
(21, '2020_11_07_184252_create_clients_table', 13),
(22, '2020_11_15_032337_create_clients_table', 14),
(23, '2020_11_15_063955_create_specializations_table', 15),
(24, '2020_11_15_064339_create_partner_multiple_files_table', 15),
(25, '2020_11_16_061107_create_cv_staff_languages_table', 16),
(26, '2020_11_16_063043_create_notification_multi_files_table', 17),
(27, '2020_11_19_091245_create_project_files_table', 18),
(28, '2020_11_19_093223_create_cv_staff_files_table', 19);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 1),
(2, 'App\\Models\\User', 2),
(4, 'App\\Models\\User', 3);

-- --------------------------------------------------------

--
-- Table structure for table `notification_multi_files`
--

CREATE TABLE `notification_multi_files` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `file_id` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notification_multi_files`
--

INSERT INTO `notification_multi_files` (`id`, `file_id`, `path`, `created_at`, `updated_at`) VALUES
(1, 1, '/admin/upload/Multiple_Files/1605515173-client.txt', '2020-11-16 02:26:13', '2020-11-16 02:26:13'),
(2, 1, '/admin/upload/Multiple_Files/1605515173-clients.sql', '2020-11-16 02:26:13', '2020-11-16 02:26:13'),
(3, 1, '/admin/upload/Multiple_Files/1605515173-cv.pdf', '2020-11-16 02:26:13', '2020-11-16 02:26:13'),
(4, 1, '/admin/upload/Multiple_Files/1605515173-cv.xps', '2020-11-16 02:26:13', '2020-11-16 02:26:13'),
(5, 1, '/admin/upload/Multiple_Files/1605515173-download.jpg', '2020-11-16 02:26:13', '2020-11-16 02:26:13'),
(6, 2, '/admin/upload/Multiple_Files/1605537548-clients.sql', '2020-11-16 08:39:08', '2020-11-16 08:39:08'),
(7, 2, '/admin/upload/Multiple_Files/1605537548-controller.txt', '2020-11-16 08:39:08', '2020-11-16 08:39:08'),
(8, 2, '/admin/upload/Multiple_Files/1605537548-create.txt', '2020-11-16 08:39:08', '2020-11-16 08:39:08'),
(9, 2, '/admin/upload/Multiple_Files/1605537548-cv.pdf', '2020-11-16 08:39:08', '2020-11-16 08:39:08'),
(10, 2, '/admin/upload/Multiple_Files/1605537548-doc.xps', '2020-11-16 08:39:08', '2020-11-16 08:39:08'),
(11, 2, '/admin/upload/Multiple_Files/1605537548-download.jpg', '2020-11-16 08:39:08', '2020-11-16 08:39:08'),
(12, 2, '/admin/upload/Multiple_Files/1605537548-project-management.sql', '2020-11-16 08:39:08', '2020-11-16 08:39:08'),
(13, 3, '/admin/upload/Multiple_Files/1605637108-EnamCoverLetter_Sg.docx', '2020-11-17 12:18:28', '2020-11-17 12:18:28'),
(14, 4, '/admin/upload/Multiple_Files/1605691783-clients.zip', '2020-11-18 03:29:43', '2020-11-18 03:29:43'),
(15, 4, '/admin/upload/Multiple_Files/1605691783-create.rar', '2020-11-18 03:29:43', '2020-11-18 03:29:43'),
(16, 6, '/admin/upload/Multiple_Files/1605695786-SampleVideo_1280x720_1mb.flv', '2020-11-18 04:36:26', '2020-11-18 04:36:26'),
(17, 7, '/admin/upload/Multiple_Files/1605697076-clients.zip', '2020-11-18 04:57:56', '2020-11-18 04:57:56'),
(18, 7, '/admin/upload/Multiple_Files/1605697076-cv.pdf', '2020-11-18 04:57:56', '2020-11-18 04:57:56'),
(19, 7, '/admin/upload/Multiple_Files/1605697076-download.jpg', '2020-11-18 04:57:56', '2020-11-18 04:57:56'),
(20, 7, '/admin/upload/Multiple_Files/1605697076-Partner Form.docx', '2020-11-18 04:57:56', '2020-11-18 04:57:56'),
(21, 7, '/admin/upload/Multiple_Files/1605697076-permissssion.txt', '2020-11-18 04:57:56', '2020-11-18 04:57:56'),
(22, 7, '/admin/upload/Multiple_Files/1605697076-project-management.sql', '2020-11-18 04:57:56', '2020-11-18 04:57:56'),
(23, 7, '/admin/upload/Multiple_Files/1605697076-SampleVideo_1280x720_1mb.flv', '2020-11-18 04:57:56', '2020-11-18 04:57:56'),
(24, 7, '/admin/upload/Multiple_Files/1605697076-PDS-Format.doc', '2020-11-18 04:57:56', '2020-11-18 04:57:56'),
(25, 8, '/admin/upload/Multiple_Files/1605758136-clients.sql', '2020-11-18 21:55:36', '2020-11-18 21:55:36'),
(26, 8, '/admin/upload/Multiple_Files/1605758136-cv.pdf', '2020-11-18 21:55:36', '2020-11-18 21:55:36'),
(27, 8, '/admin/upload/Multiple_Files/1605758136-Partner Form.docx', '2020-11-18 21:55:36', '2020-11-18 21:55:36'),
(28, 8, '/admin/upload/Multiple_Files/1605758136-PDS-Format.doc', '2020-11-18 21:55:36', '2020-11-18 21:55:36'),
(29, 8, '/admin/upload/Multiple_Files/1605758136-SampleVideo_1280x720_1mb.flv', '2020-11-18 21:55:36', '2020-11-18 21:55:36');

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE `partners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `partner_company_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sector_of_specialization` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `partner_home_country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `partner_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `partner_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `partner_mail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_person` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `jv_bidding` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `jv_project` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `partners`
--

INSERT INTO `partners` (`id`, `partner_company_name`, `sector_of_specialization`, `partner_home_country`, `partner_address`, `partner_phone`, `partner_mail`, `contact_person`, `jv_bidding`, `jv_project`, `created_at`, `updated_at`) VALUES
(1, 'Toyota ltd', NULL, 'Malaysia', 'Subang Avenue\r\nSubang Jaya', '01112011350', '1emai@gmail', '{\n    \"name\": [\n        \"Sony\"\n    ],\n    \"phone\": [\n        \"01112011350\"\n    ],\n    \"email\": [\n        \"sony@gmail\"\n    ]\n}', '{\n    \"name\": [\n        \"fhg\"\n    ],\n    \"country\": [\n        \"ghfgj\"\n    ],\n    \"procuring\": [\n        \"tjyj\"\n    ]\n}', '{\n    \"name\": [\n        \"drhdth\"\n    ],\n    \"country\": [\n        \"dh\"\n    ],\n    \"procuring\": [\n        \"rhe\"\n    ]\n}', '2020-11-22 05:13:40', '2020-11-22 05:13:40'),
(2, 'nn', NULL, 'nnn', '', '', '', '{\n    \"name\": [\n        null\n    ],\n    \"phone\": [\n        null\n    ],\n    \"email\": [\n        null\n    ]\n}', '{\n    \"name\": [\n        null\n    ],\n    \"country\": [\n        null\n    ],\n    \"procuring\": [\n        null\n    ]\n}', '{\n    \"name\": [\n        null\n    ],\n    \"country\": [\n        null\n    ],\n    \"procuring\": [\n        null\n    ]\n}', '2020-11-22 11:55:41', '2020-11-22 11:55:41');

-- --------------------------------------------------------

--
-- Table structure for table `partner_multiple_files`
--

CREATE TABLE `partner_multiple_files` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `partner_id` int(11) NOT NULL,
  `file_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `partner_multiple_files`
--

INSERT INTO `partner_multiple_files` (`id`, `partner_id`, `file_path`, `created_at`, `updated_at`) VALUES
(3, 1, '/admin/upload/partner_files/1606043320-download.jpg', '2020-11-22 05:08:40', '2020-11-22 05:08:40'),
(4, 1, '/admin/upload/partner_files/1606043320-SampleVideo_1280x720_1mb.flv', '2020-11-22 05:08:40', '2020-11-22 05:08:40'),
(6, 1, '/admin/upload/partner_files/1606043620-cv.pdf', '2020-11-22 05:13:40', '2020-11-22 05:13:40');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'role-list', 'web', '2020-10-18 03:37:56', '2020-10-18 03:37:56'),
(2, 'role-create', 'web', '2020-10-18 03:37:56', '2020-10-18 03:37:56'),
(3, 'role-edit', 'web', '2020-10-18 03:37:57', '2020-10-18 03:37:57'),
(4, 'role-delete', 'web', '2020-10-18 03:37:57', '2020-10-18 03:37:57'),
(5, 'cv-list', 'web', '2020-10-18 03:37:57', '2020-10-18 03:37:57'),
(6, 'cv-create', 'web', '2020-10-18 03:37:57', '2020-10-18 03:37:57'),
(7, 'cv-edit', 'web', '2020-10-18 03:37:57', '2020-10-18 03:37:57'),
(8, 'cv-delete', 'web', '2020-10-18 03:37:57', '2020-10-18 03:37:57'),
(9, 'project-list', 'web', '2020-10-25 04:55:12', '2020-10-25 04:55:12'),
(10, 'create-project', 'web', '2020-10-25 05:06:48', '2020-10-25 05:06:48'),
(11, 'edit-project', 'web', '2020-10-27 00:25:55', '2020-10-27 00:25:55'),
(12, 'delete-project', 'web', '2020-10-27 00:41:03', '2020-10-27 00:41:03'),
(13, 'cv-category', 'web', '2020-10-27 00:45:22', '2020-10-27 00:45:22'),
(14, 'create-category', 'web', '2020-10-27 00:51:06', '2020-10-27 00:51:06'),
(15, 'delete-category', 'web', '2020-10-27 01:53:29', '2020-10-27 01:53:29'),
(16, 'create-partner', 'web', '2020-11-10 21:24:36', '2020-11-10 21:24:36'),
(17, 'edit-partner', 'web', '2020-11-10 21:25:52', '2020-11-10 21:25:52'),
(18, 'partner-list', 'web', '2020-11-10 21:26:43', '2020-11-10 21:26:43'),
(19, 'delete-partner', 'web', '2020-11-10 21:27:07', '2020-11-10 21:27:07'),
(20, 'edit-client', 'web', '2020-11-10 22:20:54', '2020-11-10 22:20:54'),
(21, 'client-report', 'web', '2020-11-10 22:21:32', '2020-11-10 22:21:32'),
(22, 'delete-client', 'web', '2020-11-10 22:21:50', '2020-11-10 22:21:50'),
(23, 'project-report', 'web', '2020-11-10 22:24:01', '2020-11-10 22:24:01'),
(24, 'cv-form', 'web', '2020-11-10 22:30:04', '2020-11-10 22:30:04'),
(27, 'manage-user', 'web', '2020-11-23 00:08:57', '2020-11-23 00:08:57'),
(28, 'manage-role', 'web', '2020-11-23 00:12:39', '2020-11-23 00:12:39'),
(29, 'manage-cv', 'web', '2020-11-23 21:27:37', '2020-11-23 21:27:37'),
(30, 'manage-project', 'web', '2020-11-23 21:27:54', '2020-11-23 21:27:54'),
(31, 'manage-client', 'web', '2020-11-23 21:28:07', '2020-11-23 21:28:07'),
(32, 'manage-partner', 'web', '2020-11-23 21:28:18', '2020-11-23 21:28:18'),
(33, 'manage-file', 'web', '2020-11-23 21:28:28', '2020-11-23 21:28:28'),
(34, 'projectfile-download', 'web', '2020-11-23 21:29:36', '2020-11-23 21:29:36'),
(35, 'cvfile-download', 'web', '2020-11-23 21:29:49', '2020-11-23 21:29:49'),
(36, 'partnerfile-download', 'web', '2020-11-23 21:30:11', '2020-11-23 21:30:11'),
(37, 'multiplefile-download', 'web', '2020-11-23 21:30:49', '2020-11-23 21:30:49'),
(39, 'create-client', 'web', '2020-11-23 21:55:32', '2020-11-23 21:55:32'),
(40, 'client-list', 'web', '2020-11-23 22:01:22', '2020-11-23 22:01:22');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `project_name` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `in_location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_months` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jv_consultant` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jv_lead_consultant` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jv_staff_months` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `project_description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_cost` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `service_render` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_tag` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `n_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `project_name`, `country`, `in_location`, `client_name`, `staff_months`, `jv_consultant`, `jv_lead_consultant`, `jv_staff_months`, `project_description`, `project_cost`, `start_date`, `end_date`, `service_render`, `project_tag`, `remarks`, `n_file`, `created_at`, `updated_at`) VALUES
(1, 'tag test', 'Bangladesh', 'test', '2', '75 man months.', '2', '4', NULL, '<p>test</p>', '108.00 Crore', '2020-11-14 23:01:53', '2020-11-14 23:01:53', 'test', 'high, storied, eng', NULL, '', NULL, NULL),
(2, 'new test client', 'Bangladesh', 'new test client', '2', 'new test client', NULL, NULL, 'new test client', '<p>new test client</p>', 'new test client', '2020-11-14 23:07:12', '2020-11-14 23:07:12', 'new test client', '', NULL, '', NULL, NULL),
(3, 'mont test', 'BANGLADESH', 'mont test', '1', '75 man months.', '', 'Select Lead Partner', 'mont test', '<p>mont test</p>', 'mont test', '2019-01-19 03:47:51', '2020-01-19 03:47:51', 'mont test', '', NULL, '', NULL, '2020-11-19 03:47:51'),
(4, 'file test', 'BANGLADESH', NULL, '2', NULL, '', 'Select Lead Partner', NULL, '<p>file test</p>', '108.00 Crore', '2020-11-19 03:20:13', '2020-11-19 03:20:13', 'file test', '', NULL, '', NULL, '2020-11-19 03:20:13'),
(8, 'nn', 'BANGLADESH', NULL, NULL, NULL, '1', '2', NULL, '<p>nn</p>', 'nn', '2020-11-23 03:34:55', '2020-11-23 03:34:55', 'nn', '', NULL, '', NULL, '2020-11-23 03:34:56');

-- --------------------------------------------------------

--
-- Table structure for table `project_files`
--

CREATE TABLE `project_files` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `projects_id` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `project_files`
--

INSERT INTO `project_files` (`id`, `projects_id`, `path`, `created_at`, `updated_at`) VALUES
(2, 4, '/admin/upload/project_files/1605777513-controller.txt', '2020-11-19 03:18:33', '2020-11-19 03:18:33'),
(3, 4, '/admin/upload/project_files/1605777513-cv.pdf', '2020-11-19 03:18:33', '2020-11-19 03:18:33'),
(4, 4, '/admin/upload/project_files/1605777513-download.jpg', '2020-11-19 03:18:33', '2020-11-19 03:18:33'),
(6, 4, '/admin/upload/project_files/1605777513-SampleVideo_1280x720_1mb.flv', '2020-11-19 03:18:33', '2020-11-19 03:18:33');

-- --------------------------------------------------------

--
-- Table structure for table `project_involve_clients_info`
--

CREATE TABLE `project_involve_clients_info` (
  `id` int(11) NOT NULL,
  `projects_id` int(11) DEFAULT NULL,
  `clients_id` int(11) DEFAULT NULL,
  `consulting_fee` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lobbing_expenses` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `project_director` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `head_of_the_department` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `lobby` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `media` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `project_involve_clients_info`
--

INSERT INTO `project_involve_clients_info` (`id`, `projects_id`, `clients_id`, `consulting_fee`, `lobbing_expenses`, `project_director`, `head_of_the_department`, `lobby`, `media`, `created_at`, `updated_at`) VALUES
(1, 1, 2, '200 taka', '560 taka', '{\n    \"designation\": [\n        \"Project Director\",\n        null\n    ],\n    \"name\": [\n        \"A K M Tauhid\",\n        null\n    ],\n    \"phone\": [\n        \"575\",\n        null\n    ],\n    \"email\": [\n        \"mah@gmail.com\",\n        null\n    ]\n}', '{\n    \"designation\": [\n        \"Head of the Department\"\n    ],\n    \"name\": [\n        \"Engr Khaled\"\n    ],\n    \"phone\": [\n        \"567\"\n    ],\n    \"email\": [\n        \"gfd@gmail.com\"\n    ]\n}', '{\n    \"designation\": [\n        \"Name of The Lobby\"\n    ],\n    \"name\": [\n        \"Saidul\"\n    ],\n    \"phone\": [\n        \"564\"\n    ],\n    \"email\": [\n        \"dhgsiufbh@gmail.com\"\n    ]\n}', '{\n    \"name\": [\n        \"Asifur\",\n        \"Mahmud\"\n    ],\n    \"phone\": [\n        \"2354\",\n        \"4455\"\n    ],\n    \"email\": [\n        \"asdf@gmail.com\",\n        \"Saikul@gmail.com\"\n    ]\n}', '2020-11-14 23:01:53', '2020-11-14 23:01:53'),
(2, 2, 2, '200 taka', '560 taka', '{\n    \"designation\": [\n        \"new test client\"\n    ],\n    \"name\": [\n        \"new test client\"\n    ],\n    \"phone\": [\n        \"new test client\"\n    ],\n    \"email\": [\n        \"new test client\"\n    ]\n}', '{\n    \"designation\": [\n        \"new test client\"\n    ],\n    \"name\": [\n        \"new test client\"\n    ],\n    \"phone\": [\n        \"new test client\"\n    ],\n    \"email\": [\n        \"new test client\"\n    ]\n}', '{\n    \"designation\": [\n        \"new test client\"\n    ],\n    \"name\": [\n        \"new test client\"\n    ],\n    \"phone\": [\n        \"new test client\"\n    ],\n    \"email\": [\n        \"new test client\"\n    ]\n}', '{\n    \"name\": [\n        \"new test client\",\n        \"new test client\"\n    ],\n    \"phone\": [\n        \"new test client\",\n        \"new test client\"\n    ],\n    \"email\": [\n        \"new test client\",\n        \"new test client\"\n    ]\n}', '2020-11-14 23:07:12', '2020-11-14 23:07:12'),
(3, 3, 1, 'mont test', NULL, '{\n    \"designation\": [\n        \"Project Director\"\n    ],\n    \"name\": [\n        null\n    ],\n    \"phone\": [\n        null\n    ],\n    \"email\": [\n        null\n    ]\n}', '{\n    \"designation\": [\n        \"Head of the Department\"\n    ],\n    \"name\": [\n        null\n    ],\n    \"phone\": [\n        null\n    ],\n    \"email\": [\n        null\n    ]\n}', '{\n    \"designation\": [\n        \"Name of The Lobby\"\n    ],\n    \"name\": [\n        null\n    ],\n    \"phone\": [\n        null\n    ],\n    \"email\": [\n        null\n    ]\n}', '{\n    \"name\": [\n        null\n    ],\n    \"phone\": [\n        \"01112011350\"\n    ],\n    \"email\": [\n        null\n    ]\n}', '2020-11-16 09:29:20', '2020-11-19 03:47:51'),
(4, 4, 2, NULL, NULL, '{\n    \"designation\": [\n        null\n    ],\n    \"name\": [\n        null\n    ],\n    \"phone\": [\n        null\n    ],\n    \"email\": [\n        null\n    ]\n}', '{\n    \"designation\": [\n        null\n    ],\n    \"name\": [\n        null\n    ],\n    \"phone\": [\n        null\n    ],\n    \"email\": [\n        null\n    ]\n}', '{\n    \"designation\": [\n        null\n    ],\n    \"name\": [\n        null\n    ],\n    \"phone\": [\n        null\n    ],\n    \"email\": [\n        null\n    ]\n}', '{\n    \"name\": [\n        null\n    ],\n    \"phone\": [\n        null\n    ],\n    \"email\": [\n        null\n    ]\n}', '2020-11-19 03:18:33', '2020-11-19 03:20:13'),
(5, 7, NULL, NULL, NULL, '{\n    \"designation\": [\n        null\n    ],\n    \"name\": [\n        null\n    ],\n    \"phone\": [\n        null\n    ],\n    \"email\": [\n        null\n    ]\n}', '{\n    \"designation\": [\n        null\n    ],\n    \"name\": [\n        null\n    ],\n    \"phone\": [\n        null\n    ],\n    \"email\": [\n        null\n    ]\n}', '{\n    \"designation\": [\n        null\n    ],\n    \"name\": [\n        null\n    ],\n    \"phone\": [\n        null\n    ],\n    \"email\": [\n        null\n    ]\n}', '{\n    \"name\": [\n        null\n    ],\n    \"phone\": [\n        null\n    ],\n    \"email\": [\n        null\n    ]\n}', '2020-11-22 00:08:36', '2020-11-22 00:08:36'),
(6, 8, NULL, NULL, NULL, '{\n    \"designation\": [\n        null\n    ],\n    \"name\": [\n        null\n    ],\n    \"phone\": [\n        null\n    ],\n    \"email\": [\n        null\n    ]\n}', '{\n    \"designation\": [\n        null\n    ],\n    \"name\": [\n        null\n    ],\n    \"phone\": [\n        null\n    ],\n    \"email\": [\n        null\n    ]\n}', '{\n    \"designation\": [\n        null\n    ],\n    \"name\": [\n        null\n    ],\n    \"phone\": [\n        null\n    ],\n    \"email\": [\n        null\n    ]\n}', '{\n    \"name\": [\n        null\n    ],\n    \"phone\": [\n        null\n    ],\n    \"email\": [\n        null\n    ]\n}', '2020-11-22 00:11:17', '2020-11-23 03:34:55'),
(7, 9, NULL, NULL, NULL, '{\n    \"designation\": [\n        null\n    ],\n    \"name\": [\n        null\n    ],\n    \"phone\": [\n        null\n    ],\n    \"email\": [\n        null\n    ]\n}', '{\n    \"designation\": [\n        null\n    ],\n    \"name\": [\n        null\n    ],\n    \"phone\": [\n        null\n    ],\n    \"email\": [\n        null\n    ]\n}', '{\n    \"designation\": [\n        null\n    ],\n    \"name\": [\n        null\n    ],\n    \"phone\": [\n        null\n    ],\n    \"email\": [\n        null\n    ]\n}', '{\n    \"name\": [\n        null\n    ],\n    \"phone\": [\n        null\n    ],\n    \"email\": [\n        null\n    ]\n}', '2020-11-22 03:26:46', '2020-11-22 03:26:46');

-- --------------------------------------------------------

--
-- Table structure for table `project_involve_staff`
--

CREATE TABLE `project_involve_staff` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `projects_id` int(11) NOT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `assign_position` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `duties` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `project_involve_staff`
--

INSERT INTO `project_involve_staff` (`id`, `projects_id`, `staff_id`, `assign_position`, `duties`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Team Leader', 'dyty  Team Leader', '2020-11-14 12:05:25', '2020-11-14 12:05:25'),
(2, 1, 5, 'Manger', 'duty Manger', '2020-11-14 12:05:25', '2020-11-14 12:05:25'),
(3, 2, 7, 'Architect', 'duty as architect', '2020-11-14 21:59:33', '2020-11-14 21:59:33'),
(4, 2, 6, 'Geoly', 'duty as geuly', '2020-11-14 21:59:33', '2020-11-14 21:59:33'),
(6, 1, 2, 'test', 'test', '2020-11-14 23:01:53', '2020-11-14 23:01:53'),
(7, 2, 3, 'new test client', 'new test client', '2020-11-14 23:07:12', '2020-11-14 23:07:12'),
(10, 4, 2, NULL, 'nn', '2020-11-19 03:20:13', '2020-11-19 03:20:13'),
(13, 3, 3, 'test', 'test', '2020-11-19 03:47:51', '2020-11-19 03:47:51'),
(14, 3, 2, 'mont test', 'nn', '2020-11-19 03:47:51', '2020-11-19 03:47:51'),
(15, 7, NULL, NULL, 'nn', '2020-11-22 00:08:36', '2020-11-22 00:08:36'),
(32, 9, NULL, NULL, NULL, '2020-11-22 03:26:46', '2020-11-22 03:26:46'),
(33, 8, 3, 'Architect', NULL, '2020-11-23 03:34:55', '2020-11-23 03:34:55'),
(34, 8, NULL, 'Team Leader', NULL, '2020-11-23 03:34:55', '2020-11-23 03:34:55');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'web', '2020-10-18 03:38:13', '2020-10-18 03:38:13'),
(2, 'user', 'web', '2020-10-18 03:39:47', '2020-10-18 03:39:47'),
(4, 'project', 'web', '2020-11-23 03:26:55', '2020-11-23 03:26:55');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(5, 2),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(9, 4),
(10, 1),
(11, 1),
(12, 1),
(12, 4),
(13, 1),
(13, 2),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(23, 4),
(24, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(39, 1),
(40, 1);

-- --------------------------------------------------------

--
-- Table structure for table `specializations`
--

CREATE TABLE `specializations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `partner_id` int(11) NOT NULL,
  `sector` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tags` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `specializations`
--

INSERT INTO `specializations` (`id`, `partner_id`, `sector`, `tags`, `created_at`, `updated_at`) VALUES
(8, 1, 'health', 'hospital, covid,', '2020-11-22 05:16:42', '2020-11-22 05:16:42'),
(9, 1, 'transport', 'road, airport', '2020-11-22 05:16:42', '2020-11-22 05:16:42'),
(11, 3, 'health', 'hospital, covid,', '2020-11-22 11:56:56', '2020-11-22 11:56:56');

-- --------------------------------------------------------

--
-- Table structure for table `staff_categories`
--

CREATE TABLE `staff_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff_categories`
--

INSERT INTO `staff_categories` (`id`, `category_name`, `created_at`, `updated_at`) VALUES
(1, 'Construction Engineer', NULL, '2020-11-11 01:13:57'),
(2, 'Architect', NULL, NULL),
(3, 'Geologist', '2020-10-25 04:05:28', '2020-10-25 04:05:28'),
(6, 'Not Defined', '2020-10-31 07:09:31', '2020-10-31 07:09:31'),
(7, 'ICT Specialist', '2020-10-31 07:14:30', '2020-10-31 07:14:30'),
(11, 'Cost Estmator', '2020-11-09 22:26:04', '2020-11-09 22:26:04'),
(12, 'SANITARY AND WATER SUPPLY ENGINEER', '2020-11-11 03:17:09', '2020-11-11 03:17:09');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `phone`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@gmail.com', '1235875', NULL, '$2y$10$sJT.1kYoR0cw4YSzMpLO4eHZ8fG90fAvU.n3nPHdhwPVrNg6JSVYa', 'pWysvkoflwiUK0yxvClEDlH9j6RWSsSmGlUa4FQ98WCA9bh1zAxiHBf2uprK', '2020-10-18 03:38:13', '2020-11-22 23:59:26'),
(2, 'Minhaz', 'minhaz@gmail.com', '01854556929', NULL, '$2y$10$3IP/kK/fnhmZe1jW.QOIGeXB0vTgap.AdANieNfhQZd6of1kEPHwO', NULL, '2020-10-18 03:44:18', '2020-11-23 00:00:31'),
(3, 'al mamun', 'almamun214@gmail.com', '01913705269', NULL, '$2y$10$v9E32ngKw3zRLNpNypr3jOjMUnD5ETLRhL6objV1qiGsjhBQiSL/2', NULL, '2020-11-23 03:28:14', '2020-11-23 03:28:14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cv_staff`
--
ALTER TABLE `cv_staff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cv_staff_files`
--
ALTER TABLE `cv_staff_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cv_staff_languages`
--
ALTER TABLE `cv_staff_languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `notification_multi_files`
--
ALTER TABLE `notification_multi_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partner_multiple_files`
--
ALTER TABLE `partner_multiple_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_files`
--
ALTER TABLE `project_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_involve_clients_info`
--
ALTER TABLE `project_involve_clients_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_involve_staff`
--
ALTER TABLE `project_involve_staff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `specializations`
--
ALTER TABLE `specializations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff_categories`
--
ALTER TABLE `staff_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `cv_staff`
--
ALTER TABLE `cv_staff`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `cv_staff_files`
--
ALTER TABLE `cv_staff_files`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `cv_staff_languages`
--
ALTER TABLE `cv_staff_languages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `notification_multi_files`
--
ALTER TABLE `notification_multi_files`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `partner_multiple_files`
--
ALTER TABLE `partner_multiple_files`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `project_files`
--
ALTER TABLE `project_files`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `project_involve_clients_info`
--
ALTER TABLE `project_involve_clients_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `project_involve_staff`
--
ALTER TABLE `project_involve_staff`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `specializations`
--
ALTER TABLE `specializations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `staff_categories`
--
ALTER TABLE `staff_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
