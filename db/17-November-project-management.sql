-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 17, 2020 at 05:45 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project-management`
--

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `client_name`, `client_address`, `created_at`, `updated_at`) VALUES
(1, 'Client 2', 'Address 2', '2020-11-14 21:25:53', '2020-11-14 21:56:51'),
(2, 'Client 1', 'Address 1', '2020-11-14 21:56:32', '2020-11-14 21:56:32');

-- --------------------------------------------------------

--
-- Table structure for table `cv_staff`
--

CREATE TABLE `cv_staff` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `consultant_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rfp_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_name` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `staff_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birth_date` date DEFAULT NULL,
  `nationality` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `membership` varchar(600) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qualification` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `experience` year(4) DEFAULT NULL,
  `training` varchar(800) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bengali` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `english` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `country_w_experience` varchar(350) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employment_record` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `cv_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cv_staff`
--

INSERT INTO `cv_staff` (`id`, `category_id`, `consultant_name`, `rfp_no`, `client_name`, `position`, `staff_name`, `birth_date`, `nationality`, `membership`, `qualification`, `experience`, `training`, `bengali`, `english`, `country_w_experience`, `employment_record`, `cv_file`, `created_at`, `updated_at`) VALUES
(1, 11, 'Engineers Consortium Ltd.', '33.01.0000.835.11.004.20-96, Dated: 05/07/2020', 'Project Director, Establishment of Institute of Livestock Science and Technology project in Sylhet, Lalmonirhat/Kurigram and Barishal, Krishi Khamar Sarak, Farmgate, Dhaka-1215', 'COST ESTIMATOR', 'S M KHAZA ALAUDDIN', '1965-12-17', 'Bangladeshi by birth', 'not available', '<p>Dip-in-Engg. (Civil), from Engineering and Survey Institute, Rajshahi in the year 1989</p>', 1989, 'not available', '{\n    \"speaking\": \"Mother Tongue\",\n    \"reading\": \"Excellent\",\n    \"writing\": \"Excellent\"\n}', '{\n    \"speaking\": \"Excellent\",\n    \"reading\": \"Excellent\",\n    \"writing\": \"Excellent\"\n}', 'Bangladesh', '{\r\n    \"employer\": [\r\n        \"Eng Consort\",\r\n        \"Eng Consort 2\"\r\n    ],\r\n    \"duration\": [\r\n        \"From Jan to till\",\r\n        \"From Jan to till 2\"\r\n    ],\r\n    \"employment_position\": [\r\n        \"Leader\",\r\n        \"Manager\"\r\n    ],\r\n    \"duties\": [\r\n        \"Duty 1\",\r\n        \"Duty 2\"\r\n    ]\r\n}', '', '2020-11-09 22:30:05', NULL),
(2, 2, 'Engineers Consortium Ltd.', '33.01.0000.835.11.004.20-96, Dated: 05/07/2020', 'Project Director, Establishment of Institute of Livestock Science and Technology project in Sylhet, Lalmonirhat/Kurigram and Barishal, Krishi Khamar Sarak, Farmgate, Dhaka-1215', 'Senior Architect', 'Arch. Shah Alam', '1980-02-05', 'Bangladeshi by birth', 'n/a', '<p style=\"margin:0cm\">B. Arch. 1971, Bangladesh University of Engineering &amp; Technology, Dhaka. M.I.A.B. No: 019</p>', 1971, 'n/a', '{\n    \"speaking\": \"Mother Tongue\",\n    \"reading\": \"Excellent\",\n    \"writing\": \"Excellent\"\n}', '{\n    \"speaking\": \"Good\",\n    \"reading\": \"Good\",\n    \"writing\": \"Good\"\n}', 'Bangladesh', '{\r\n    \"employer\": [\r\n        \"Eng Consort\"\r\n    ],\r\n    \"duration\": [\r\n        \"From Jan to till\"\r\n    ],\r\n    \"employment_position\": [\r\n        \"Leader\"\r\n    ],\r\n    \"duties\": [\r\n        \"Duty 1\"\r\n\r\n    ]\r\n}', '', '2020-11-09 23:02:05', NULL),
(3, 1, 'Engineers Consortium Ltd.', '33.01.0000.835.11.004.20-96, Dated: 05/07/2020', 'Establishment of Institute of Livestock Science and Technology project in Sylhet, Lalmonirhat/Kurigram and Barishal, Krishi Khamar Sarak, Farmgate, Dhaka-1215', 'Construction Engineer', 'Md Mustafizur Rahman', '1965-07-21', 'Bangladeshi by birth', NULL, '<p style=\"text-align:justify; margin:0cm\"><span style=\"font-size:12pt\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\">B.Sc. Engg. (Civil) in BUET, Dhaka- 1984</span></span></p>\r\n\r\n<p><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Tahoma&quot;,sans-serif\"><span lang=\"EN-US\" style=\"font-size:12.0pt\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\">M.I.E.B. No.- 3593</span></span></span></span></p>', 1984, NULL, '{\n    \"speaking\": \"Mother Tongue\",\n    \"reading\": \"Excellent\",\n    \"writing\": \"Excellent\"\n}', '{\n    \"speaking\": \"Medium\",\n    \"reading\": \"Medium\",\n    \"writing\": \"Medium\"\n}', 'Bangladesh', '{\r\n    \"employer\": [\r\n        \"Eng Consort\"\r\n    ],\r\n    \"duration\": [\r\n        \"From Jan to till\"\r\n    ],\r\n    \"employment_position\": [\r\n        \"Leader\"\r\n    ],\r\n    \"duties\": [\r\n        \"Duty 1\"\r\n\r\n    ]\r\n}', '', '2020-11-11 01:19:00', '2020-11-11 01:20:35'),
(4, 2, 'Engineers Consortium Ltd.', '33.01.0000.835.11.004.20-96, Dated: 05/07/2020', 'Establishment of Institute of Livestock Science and Technology project in Sylhet, Lalmonirhat/Kurigram and Barishal, Krishi Khamar Sarak, Farmgate, Dhaka-1215', 'Architect', 'Tangila Khatun', '1982-07-15', 'Bangladeshi by birth', NULL, '<p style=\"margin:0cm\"><span style=\"font-size:12pt\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\">B. Arch. from Ashanulla University, Dhaka in the year-2004</span></span></p>\r\n\r\n<p><span lang=\"EN-US\" style=\"font-size:12.0pt\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\">M.I.A.B. No: K-116</span></span></p>', 2004, NULL, '{\n    \"speaking\": \"Mother Tongue\",\n    \"reading\": \"Excellent\",\n    \"writing\": \"excllnt\"\n}', '{\n    \"speaking\": \"Good\",\n    \"reading\": \"Good\",\n    \"writing\": \"Good\"\n}', 'Bangladesh', '{\r\n    \"employer\": [\r\n        \"Eng Consort\"\r\n    ],\r\n    \"duration\": [\r\n        \"From Jan to till\"\r\n    ],\r\n    \"employment_position\": [\r\n        \"Leader\"\r\n    ],\r\n    \"duties\": [\r\n        \"Duty 1\"\r\n\r\n    ]\r\n}', '', '2020-11-11 01:27:08', NULL),
(5, 7, 'Engineers Consortium Ltd.', '33.01.0000.835.11.004.20-96, Dated: 05/07/2020', 'Establishment of Institute of Livestock Science and Technology project in Sylhet, Lalmonirhat/Kurigram and Barishal, Krishi Khamar Sarak, Farmgate, Dhaka-1215', 'Senior Software Engineer', 'Md. Rezwanur Rahman (Rasel)', '1985-07-25', 'Bangladeshi by birth', NULL, '<p><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Arial&quot;,sans-serif\">Bachelor of Science (B.Sc) in Computer Science and Information Technology (CIT) from Islamic University of Technology (IUT) in the year 2008</span></span></p>', 2008, NULL, '{\n    \"speaking\": \"Mother Tongue\",\n    \"reading\": \"Excellent\",\n    \"writing\": \"Excellent\"\n}', '{\n    \"speaking\": \"Medium\",\n    \"reading\": \"Medium\",\n    \"writing\": \"Medium\"\n}', 'Bangladesh', '{\r\n    \"employer\": [\r\n        \"Eng Consort\"\r\n    ],\r\n    \"duration\": [\r\n        \"From Jan to till\"\r\n    ],\r\n    \"employment_position\": [\r\n        \"Leader\"\r\n    ],\r\n    \"duties\": [\r\n        \"Duty 1\"\r\n\r\n    ]\r\n}', '', '2020-11-11 01:48:28', NULL),
(6, 3, 'Engineers Consortium Ltd.', '33.01.0000.835.11.004.20-96, Dated: 05/07/2020', 'Establishment of Institute of Livestock Science and Technology project in Sylhet, Lalmonirhat/Kurigram and Barishal, Krishi Khamar Sarak, Farmgate, Dhaka-1215', 'Geologist', 'Md. Arifuzzaman Raju', '1980-06-11', 'Bangladeshi by birth', NULL, '<p style=\"text-align:justify; margin:0cm\"><span style=\"font-size:12pt\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\"><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Arial&quot;,sans-serif\">M.Sc. (Geological Sciences) from Jahangirnagar University in the year 2003 (2<sup>nd</sup> class 3<sup>rd</sup>) </span></span></span></span></p>\r\n\r\n<p><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Arial&quot;,sans-serif\">B.Sc (Hons.- Geological Sciences) from Jahangirnagar University in the year 2002 (2<sup>nd</sup> class 6<sup>th</sup>)</span></span></p>', 2002, NULL, '{\n    \"speaking\": \"Mother Tongue\",\n    \"reading\": \"Excellent\",\n    \"writing\": \"Excellent\"\n}', '{\n    \"speaking\": \"Excellent\",\n    \"reading\": \"Excellent\",\n    \"writing\": \"Excellent\"\n}', 'Bangladesh', '{\r\n    \"employer\": [\r\n        \"Eng Consort\"\r\n    ],\r\n    \"duration\": [\r\n        \"From Jan to till\"\r\n    ],\r\n    \"employment_position\": [\r\n        \"Leader\"\r\n    ],\r\n    \"duties\": [\r\n        \"Duty 1\"\r\n\r\n    ]\r\n}', '', '2020-11-11 01:51:09', NULL),
(7, 2, 'Engineers Consortium Ltd.', '33.01.0000.835.11.004.20-96, Dated: 05/07/2020', 'Project Director, Establishment of Institute of Livestock Science and Technology project in Sylhet, Lalmonirhat/Kurigram and Barishal, Krishi Khamar Sarak, Farmgate, Dhaka-1215', 'Architect', 'Rezwan Sobhan', '1979-07-11', 'Bangladeshi by birth', 'IAB', '<p><span lang=\"EN-US\" style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Arial&quot;,sans-serif\">Bachelor of Architect </span></span><span lang=\"EN\" style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Arial&quot;,sans-serif\">from Khulna University, Bangladesh in the year- 2001</span></span></p>', 2001, NULL, '{\n    \"speaking\": \"Mother Tongue\",\n    \"reading\": \"Excellent\",\n    \"writing\": \"Excellent\"\n}', '{\n    \"speaking\": \"Excellent\",\n    \"reading\": \"Excellent\",\n    \"writing\": \"Excellent\"\n}', 'Bangladesh', '{\r\n    \"employer\": [\r\n        \"Eng Consort\"\r\n    ],\r\n    \"duration\": [\r\n        \"From Jan to till\"\r\n    ],\r\n    \"employment_position\": [\r\n        \"Leader\"\r\n    ],\r\n    \"duties\": [\r\n        \"Duty 1\"\r\n\r\n    ]\r\n}', '', '2020-11-11 03:27:14', NULL),
(8, 12, 'Engineers Consortium Ltd.', '.....', 'Establishment of Institute of Livestock Science and Technology project in Sylhet, Lalmonirhat/Kurigram and Barishal, Krishi Khamar Sarak, Farmgate, Dhaka-1215', 'Supply Eng', 'Engr. Dewan Nuruzzaman', '1980-06-17', 'Bangladeshi by birth', NULL, '<p style=\"margin:0cm\"><span style=\"font-size:12pt\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\">Bachelor of Science of Engineering (Civil), BUET, Dhaka, in the year- 1979</span></span></p>\r\n\r\n<p style=\"margin:0cm\"><span style=\"font-size:12pt\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\">F.I.E.B-8889</span></span></p>\r\n\r\n<p style=\"text-align:justify; margin:0cm\"><span style=\"font-size:12pt\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\">M.I.E.B. No: 7116 </span></span></p>\r\n\r\n<p><span lang=\"EN-US\" style=\"font-size:12.0pt\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\">RAJUK Enlistment No. DMINB/CE-0219</span></span></p>', 1979, NULL, '{\n    \"speaking\": \"Mother Tongue\",\n    \"reading\": \"Excellent\",\n    \"writing\": \"Excellent\"\n}', '{\n    \"speaking\": \"Excellent\",\n    \"reading\": \"Excellent\",\n    \"writing\": \"Excellent\"\n}', 'Bangladesh', '{\n    \"employer\": [\n        \"Engineers Consortium\",\n        \"AEL\"\n    ],\n    \"duration\": [\n        \"From Jan to Dec\",\n        \"From September to Dec\"\n    ],\n    \"employment_position\": [\n        \"Manager\",\n        \"Team Leader\"\n    ],\n    \"duties\": [\n        \"duty as manager\",\n        \"Duty as Team Leader\"\n    ]\n}', '', '2020-11-12 00:38:13', NULL),
(10, 1, 'lang test', 'lang test', 'lang test', 'lang test', 'lang test', '2014-06-10', 'lang test', 'lang test', '<p>lang test</p>', 2015, 'lang test', 'null', 'null', 'Bangladse', '{\n    \"employer\": [\n        \"Engineers Consortium\"\n    ],\n    \"duration\": [\n        \"From Jan to Dec\"\n    ],\n    \"employment_position\": [\n        \"Manager\"\n    ],\n    \"duties\": [\n        \"Good\"\n    ]\n}', '', '2020-11-16 00:17:36', NULL),
(11, 3, 'year test', 'year test', 'year test', 'year test', 'year test', '2020-11-16', 'year test', 'year test', '<p>year test</p>', 1990, 'year test', 'null', 'null', 'year test', '{\n    \"employer\": [\n        \"year test\"\n    ],\n    \"duration\": [\n        \"year test\"\n    ],\n    \"employment_position\": [\n        \"year test\"\n    ],\n    \"duties\": [\n        \"year test\"\n    ]\n}', '', '2020-11-16 04:35:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cv_staff_languages`
--

CREATE TABLE `cv_staff_languages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cv_staff_id` int(11) NOT NULL,
  `language` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `speaking` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reading` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `writing` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cv_staff_languages`
--

INSERT INTO `cv_staff_languages` (`id`, `cv_staff_id`, `language`, `speaking`, `reading`, `writing`, `created_at`, `updated_at`) VALUES
(1, 10, 'Bangla', 'Mother Toungue', 'Excellent', 'Excellent', '2020-11-16 00:17:36', '2020-11-16 00:17:36'),
(2, 10, 'English', 'Good', 'Good', 'Good', '2020-11-16 00:17:36', '2020-11-16 00:17:36'),
(3, 11, 'year test', 'year test', 'year test', 'year test', '2020-11-16 04:35:48', '2020-11-16 04:35:48');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `exp_date` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `file_name`, `description`, `start_date`, `exp_date`, `created_at`, `updated_at`) VALUES
(1, 'First', 'Multi file', '2020-11-17 00:00:00', '2021-02-25 00:00:00', '2020-11-16 02:26:13', '2020-11-16 02:26:13'),
(2, '2nd', 'Multi file', '2020-11-16 00:00:00', '2020-11-16 00:00:00', '2020-11-16 08:39:08', '2020-11-16 08:39:08');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_10_09_135640_create_permission_tables', 1),
(5, '2020_10_09_135732_create_products_table', 1),
(7, '2020_10_19_052834_create_projects_table', 2),
(12, '2020_10_24_165656_create_staff_categories_table', 5),
(13, '2020_10_19_054819_create_cv_staff_table', 6),
(15, '2020_10_27_041620_add_cv_file_to_cv_staff_table', 8),
(17, '2020_11_08_040222_create_partners_table', 10),
(18, '2020_11_08_054118_create_project_involve_staff_table', 11),
(20, '2020_11_14_121645_create_files_table', 12),
(21, '2020_11_07_184252_create_clients_table', 13),
(22, '2020_11_15_032337_create_clients_table', 14),
(23, '2020_11_15_063955_create_specializations_table', 15),
(24, '2020_11_15_064339_create_partner_multiple_files_table', 15),
(25, '2020_11_16_061107_create_cv_staff_languages_table', 16),
(26, '2020_11_16_063043_create_notification_multi_files_table', 17);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 1),
(2, 'App\\Models\\User', 2);

-- --------------------------------------------------------

--
-- Table structure for table `notification_multi_files`
--

CREATE TABLE `notification_multi_files` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `file_id` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notification_multi_files`
--

INSERT INTO `notification_multi_files` (`id`, `file_id`, `path`, `created_at`, `updated_at`) VALUES
(1, 1, '/admin/upload/Multiple_Files/1605515173-client.txt', '2020-11-16 02:26:13', '2020-11-16 02:26:13'),
(2, 1, '/admin/upload/Multiple_Files/1605515173-clients.sql', '2020-11-16 02:26:13', '2020-11-16 02:26:13'),
(3, 1, '/admin/upload/Multiple_Files/1605515173-cv.pdf', '2020-11-16 02:26:13', '2020-11-16 02:26:13'),
(4, 1, '/admin/upload/Multiple_Files/1605515173-cv.xps', '2020-11-16 02:26:13', '2020-11-16 02:26:13'),
(5, 1, '/admin/upload/Multiple_Files/1605515173-download.jpg', '2020-11-16 02:26:13', '2020-11-16 02:26:13'),
(6, 2, '/admin/upload/Multiple_Files/1605537548-clients.sql', '2020-11-16 08:39:08', '2020-11-16 08:39:08'),
(7, 2, '/admin/upload/Multiple_Files/1605537548-controller.txt', '2020-11-16 08:39:08', '2020-11-16 08:39:08'),
(8, 2, '/admin/upload/Multiple_Files/1605537548-create.txt', '2020-11-16 08:39:08', '2020-11-16 08:39:08'),
(9, 2, '/admin/upload/Multiple_Files/1605537548-cv.pdf', '2020-11-16 08:39:08', '2020-11-16 08:39:08'),
(10, 2, '/admin/upload/Multiple_Files/1605537548-doc.xps', '2020-11-16 08:39:08', '2020-11-16 08:39:08'),
(11, 2, '/admin/upload/Multiple_Files/1605537548-download.jpg', '2020-11-16 08:39:08', '2020-11-16 08:39:08'),
(12, 2, '/admin/upload/Multiple_Files/1605537548-project-management.sql', '2020-11-16 08:39:08', '2020-11-16 08:39:08');

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE `partners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `partner_company_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sector_of_specialization` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `partner_home_country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `partner_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `partner_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `partner_mail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_person` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `jv_bidding` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `jv_project` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `partners`
--

INSERT INTO `partners` (`id`, `partner_company_name`, `sector_of_specialization`, `partner_home_country`, `partner_address`, `partner_phone`, `partner_mail`, `contact_person`, `jv_bidding`, `jv_project`, `created_at`, `updated_at`) VALUES
(1, 'Tata Projects Limited', NULL, 'India', NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-07 22:42:43', '2020-11-07 22:43:10'),
(2, 'Toyota ltd', NULL, 'UK', 'Subang Avenue\r\nSubang Jaya', '01112011350,014225896', '1emai@gmail,2emai@gmail', '{\n    \"name\": [\n        \"Sony\",\n        \"Emon\"\n    ],\n    \"phone\": [\n        \"01112011350\",\n        \"752704327832\"\n    ],\n    \"email\": [\n        \"sony@gmail\",\n        \"em@gmail\"\n    ]\n}', '{\n    \"name\": [\n        \"Jv bidd\",\n        \"Jv bidd 2\"\n    ],\n    \"country\": [\n        \"Malaysia\",\n        \"Malaysia 2\"\n    ],\n    \"procuring\": [\n        \"no\",\n        \"no 2\"\n    ]\n}', '{\n    \"name\": [\n        \"jv prj\",\n        \"jv prj\"\n    ],\n    \"country\": [\n        \"bangla\",\n        \"bangla 2\"\n    ],\n    \"procuring\": [\n        \"not\",\n        \"not 2\"\n    ]\n}', '2020-11-10 03:23:37', '2020-11-10 03:23:37'),
(3, 'AH Monem Ltd', NULL, 'Bangladesh', 'Kawran Bazar Dhaka*Gulshan 2 Dhaka - 1204', '01854785632,024788564256', 'monem@gmail.com,ah@gmail.com', '{\n    \"name\": [\n        \"Shafiqur\",\n        \"Asif\"\n    ],\n    \"phone\": [\n        \"0157863585\",\n        \"01987456324\"\n    ],\n    \"email\": [\n        \"shafiqur@yahoo.com\",\n        \"asif @yahoo.com\"\n    ]\n}', '{\n    \"name\": [\n        \"JV Bidding Project\",\n        \"JV Bidding Project 2\"\n    ],\n    \"country\": [\n        \"Bangladesh\",\n        \"BD\"\n    ],\n    \"procuring\": [\n        \"N\\/A\",\n        \"N\\/A 2\"\n    ],\n    \"document\": [\n        {},\n        {}\n    ]\n}', '{\n    \"name\": [\n        \"JV\",\n        \"JV 2\"\n    ],\n    \"country\": [\n        \"India\",\n        \"India\"\n    ],\n    \"procuring\": [\n        \"not available\",\n        \"not available\"\n    ],\n    \"document\": [\n        {},\n        {}\n    ]\n}', '2020-11-10 03:33:04', '2020-11-10 03:33:04'),
(4, 'Engineers Consortium Ltd.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `partner_multiple_files`
--

CREATE TABLE `partner_multiple_files` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `partner_id` int(11) NOT NULL,
  `file_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'role-list', 'web', '2020-10-18 03:37:56', '2020-10-18 03:37:56'),
(2, 'role-create', 'web', '2020-10-18 03:37:56', '2020-10-18 03:37:56'),
(3, 'role-edit', 'web', '2020-10-18 03:37:57', '2020-10-18 03:37:57'),
(4, 'role-delete', 'web', '2020-10-18 03:37:57', '2020-10-18 03:37:57'),
(5, 'cv-list', 'web', '2020-10-18 03:37:57', '2020-10-18 03:37:57'),
(6, 'cv-create', 'web', '2020-10-18 03:37:57', '2020-10-18 03:37:57'),
(7, 'cv-edit', 'web', '2020-10-18 03:37:57', '2020-10-18 03:37:57'),
(8, 'cv-delete', 'web', '2020-10-18 03:37:57', '2020-10-18 03:37:57'),
(9, 'project-list', 'web', '2020-10-25 04:55:12', '2020-10-25 04:55:12'),
(10, 'create-project', 'web', '2020-10-25 05:06:48', '2020-10-25 05:06:48'),
(11, 'edit-project', 'web', '2020-10-27 00:25:55', '2020-10-27 00:25:55'),
(12, 'delete-project', 'web', '2020-10-27 00:41:03', '2020-10-27 00:41:03'),
(13, 'cv-category', 'web', '2020-10-27 00:45:22', '2020-10-27 00:45:22'),
(14, 'create-category', 'web', '2020-10-27 00:51:06', '2020-10-27 00:51:06'),
(15, 'delete-category', 'web', '2020-10-27 01:53:29', '2020-10-27 01:53:29'),
(16, 'create-partner', 'web', '2020-11-10 21:24:36', '2020-11-10 21:24:36'),
(17, 'edit-partner', 'web', '2020-11-10 21:25:52', '2020-11-10 21:25:52'),
(18, 'partner-list', 'web', '2020-11-10 21:26:43', '2020-11-10 21:26:43'),
(19, 'delete-partner', 'web', '2020-11-10 21:27:07', '2020-11-10 21:27:07'),
(20, 'edit-client', 'web', '2020-11-10 22:20:54', '2020-11-10 22:20:54'),
(21, 'client-report', 'web', '2020-11-10 22:21:32', '2020-11-10 22:21:32'),
(22, 'delete-client', 'web', '2020-11-10 22:21:50', '2020-11-10 22:21:50'),
(23, 'project-report', 'web', '2020-11-10 22:24:01', '2020-11-10 22:24:01'),
(24, 'cv-form', 'web', '2020-11-10 22:30:04', '2020-11-10 22:30:04');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `project_name` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `in_location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_months` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jv_consultant` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jv_lead_consultant` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jv_staff_months` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `project_description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_cost` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `service_render` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_tag` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `n_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `project_name`, `country`, `in_location`, `client_name`, `staff_months`, `jv_consultant`, `jv_lead_consultant`, `jv_staff_months`, `project_description`, `project_cost`, `start_date`, `end_date`, `service_render`, `project_tag`, `remarks`, `n_file`, `created_at`, `updated_at`) VALUES
(1, 'test', 'Bangladesh', 'test', '2', '75 man months.', '2', '4', NULL, '<p>test</p>', '108.00 Crore', '2020-11-14 23:01:53', '2020-11-14 23:01:53', 'test', '', NULL, '', NULL, NULL),
(2, 'new test client', 'Bangladesh', 'new test client', '2', 'new test client', NULL, NULL, 'new test client', '<p>new test client</p>', 'new test client', '2020-11-14 23:07:12', '2020-11-14 23:07:12', 'new test client', '', NULL, '', NULL, NULL),
(3, 'mont test', 'Bangladesh', 'mont test', '1', '75 man months.', '', 'Select Lead Partner', 'mont test', '<p>mont test</p>', 'mont test', '2019-01-16 09:29:20', '2020-01-16 09:29:20', 'mont test', '', NULL, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `project_involve_clients_info`
--

CREATE TABLE `project_involve_clients_info` (
  `id` int(11) NOT NULL,
  `projects_id` int(11) DEFAULT NULL,
  `clients_id` int(11) DEFAULT NULL,
  `consulting_fee` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lobbing_expenses` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `project_director` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `head_of_the_department` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `lobby` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `media` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `project_involve_clients_info`
--

INSERT INTO `project_involve_clients_info` (`id`, `projects_id`, `clients_id`, `consulting_fee`, `lobbing_expenses`, `project_director`, `head_of_the_department`, `lobby`, `media`, `created_at`, `updated_at`) VALUES
(1, 1, 2, '200 taka', '560 taka', '{\n    \"designation\": [\n        \"Project Director\",\n        null\n    ],\n    \"name\": [\n        \"A K M Tauhid\",\n        null\n    ],\n    \"phone\": [\n        \"575\",\n        null\n    ],\n    \"email\": [\n        \"mah@gmail.com\",\n        null\n    ]\n}', '{\n    \"designation\": [\n        \"Head of the Department\"\n    ],\n    \"name\": [\n        \"Engr Khaled\"\n    ],\n    \"phone\": [\n        \"567\"\n    ],\n    \"email\": [\n        \"gfd@gmail.com\"\n    ]\n}', '{\n    \"designation\": [\n        \"Name of The Lobby\"\n    ],\n    \"name\": [\n        \"Saidul\"\n    ],\n    \"phone\": [\n        \"564\"\n    ],\n    \"email\": [\n        \"dhgsiufbh@gmail.com\"\n    ]\n}', '{\n    \"name\": [\n        \"Asifur\",\n        \"Mahmud\"\n    ],\n    \"phone\": [\n        \"2354\",\n        \"4455\"\n    ],\n    \"email\": [\n        \"asdf@gmail.com\",\n        \"Saikul@gmail.com\"\n    ]\n}', '2020-11-14 23:01:53', '2020-11-14 23:01:53'),
(2, 2, 2, '200 taka', '560 taka', '{\n    \"designation\": [\n        \"new test client\"\n    ],\n    \"name\": [\n        \"new test client\"\n    ],\n    \"phone\": [\n        \"new test client\"\n    ],\n    \"email\": [\n        \"new test client\"\n    ]\n}', '{\n    \"designation\": [\n        \"new test client\"\n    ],\n    \"name\": [\n        \"new test client\"\n    ],\n    \"phone\": [\n        \"new test client\"\n    ],\n    \"email\": [\n        \"new test client\"\n    ]\n}', '{\n    \"designation\": [\n        \"new test client\"\n    ],\n    \"name\": [\n        \"new test client\"\n    ],\n    \"phone\": [\n        \"new test client\"\n    ],\n    \"email\": [\n        \"new test client\"\n    ]\n}', '{\n    \"name\": [\n        \"new test client\",\n        \"new test client\"\n    ],\n    \"phone\": [\n        \"new test client\",\n        \"new test client\"\n    ],\n    \"email\": [\n        \"new test client\",\n        \"new test client\"\n    ]\n}', '2020-11-14 23:07:12', '2020-11-14 23:07:12'),
(3, 3, 1, 'mont test', NULL, '{\n    \"designation\": [\n        null\n    ],\n    \"name\": [\n        null\n    ],\n    \"phone\": [\n        null\n    ],\n    \"email\": [\n        null\n    ]\n}', '{\n    \"designation\": [\n        null\n    ],\n    \"name\": [\n        null\n    ],\n    \"phone\": [\n        null\n    ],\n    \"email\": [\n        null\n    ]\n}', '{\n    \"designation\": [\n        null\n    ],\n    \"name\": [\n        null\n    ],\n    \"phone\": [\n        null\n    ],\n    \"email\": [\n        null\n    ]\n}', '{\n    \"name\": [\n        null\n    ],\n    \"phone\": [\n        null\n    ],\n    \"email\": [\n        null\n    ]\n}', '2020-11-16 09:29:20', '2020-11-16 09:29:20');

-- --------------------------------------------------------

--
-- Table structure for table `project_involve_staff`
--

CREATE TABLE `project_involve_staff` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `projects_id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `assign_position` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `duties` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `project_involve_staff`
--

INSERT INTO `project_involve_staff` (`id`, `projects_id`, `staff_id`, `assign_position`, `duties`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Team Leader', 'dyty  Team Leader', '2020-11-14 12:05:25', '2020-11-14 12:05:25'),
(2, 1, 5, 'Manger', 'duty Manger', '2020-11-14 12:05:25', '2020-11-14 12:05:25'),
(3, 2, 7, 'Architect', 'duty as architect', '2020-11-14 21:59:33', '2020-11-14 21:59:33'),
(4, 2, 6, 'Geoly', 'duty as geuly', '2020-11-14 21:59:33', '2020-11-14 21:59:33'),
(5, 3, 3, 'test', 'test', '2020-11-14 22:57:12', '2020-11-14 22:57:12'),
(6, 1, 2, 'test', 'test', '2020-11-14 23:01:53', '2020-11-14 23:01:53'),
(7, 2, 3, 'new test client', 'new test client', '2020-11-14 23:07:12', '2020-11-14 23:07:12'),
(8, 3, 2, 'mont test', 'nn', '2020-11-16 09:29:20', '2020-11-16 09:29:20');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'web', '2020-10-18 03:38:13', '2020-10-18 03:38:13'),
(2, 'user', 'web', '2020-10-18 03:39:47', '2020-10-18 03:39:47');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(5, 2),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(9, 2),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(13, 2),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1);

-- --------------------------------------------------------

--
-- Table structure for table `specializations`
--

CREATE TABLE `specializations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `partner_id` int(11) NOT NULL,
  `sector` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tags` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `specializations`
--

INSERT INTO `specializations` (`id`, `partner_id`, `sector`, `tags`, `created_at`, `updated_at`) VALUES
(1, 2, 'sector1', 'tags 1, tags 2', NULL, NULL),
(2, 2, 'sector1', 'tags 1, tags 2', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `staff_categories`
--

CREATE TABLE `staff_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff_categories`
--

INSERT INTO `staff_categories` (`id`, `category_name`, `created_at`, `updated_at`) VALUES
(1, 'Construction Engineer', NULL, '2020-11-11 01:13:57'),
(2, 'Architect', NULL, NULL),
(3, 'Geologist', '2020-10-25 04:05:28', '2020-10-25 04:05:28'),
(6, 'Not Defined', '2020-10-31 07:09:31', '2020-10-31 07:09:31'),
(7, 'ICT Specialist', '2020-10-31 07:14:30', '2020-10-31 07:14:30'),
(11, 'Cost Estmator', '2020-11-09 22:26:04', '2020-11-09 22:26:04'),
(12, 'SANITARY AND WATER SUPPLY ENGINEER', '2020-11-11 03:17:09', '2020-11-11 03:17:09');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@gmail.com', NULL, '$2y$10$2/BrfTZSE9DcY/AUZu0lcu6kgqlEQi0p/p4MJgF.k0D2DVa9LO27a', '5dX4ywO7DmnL34sncRQ1PwSi6G3ncm1o3mqhcPqPj5PTULZJZDAnfnsQmbkM', '2020-10-18 03:38:13', '2020-10-18 03:38:13'),
(2, 'minhaz', 'minhaz@gmail.com', NULL, '$2y$10$2/BrfTZSE9DcY/AUZu0lcu6kgqlEQi0p/p4MJgF.k0D2DVa9LO27a', NULL, '2020-10-18 03:44:18', '2020-10-18 03:44:18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cv_staff`
--
ALTER TABLE `cv_staff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cv_staff_languages`
--
ALTER TABLE `cv_staff_languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `notification_multi_files`
--
ALTER TABLE `notification_multi_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partner_multiple_files`
--
ALTER TABLE `partner_multiple_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_involve_clients_info`
--
ALTER TABLE `project_involve_clients_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_involve_staff`
--
ALTER TABLE `project_involve_staff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `specializations`
--
ALTER TABLE `specializations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff_categories`
--
ALTER TABLE `staff_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cv_staff`
--
ALTER TABLE `cv_staff`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `cv_staff_languages`
--
ALTER TABLE `cv_staff_languages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `notification_multi_files`
--
ALTER TABLE `notification_multi_files`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `partner_multiple_files`
--
ALTER TABLE `partner_multiple_files`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `project_involve_clients_info`
--
ALTER TABLE `project_involve_clients_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `project_involve_staff`
--
ALTER TABLE `project_involve_staff`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `specializations`
--
ALTER TABLE `specializations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `staff_categories`
--
ALTER TABLE `staff_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
